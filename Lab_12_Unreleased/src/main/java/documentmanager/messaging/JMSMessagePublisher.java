package documentmanager.messaging;


import documentmanager.utils.JMSConstants;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.jms.*;
import javax.json.Json;
import javax.json.JsonObject;
import java.time.Instant;

@Singleton
public class JMSMessagePublisher {

    @Resource(lookup = JMSConstants.JMS_FACTORY)
    private ConnectionFactory jmsFactory;

    @Resource(lookup = JMSConstants.JMS_QUEUE)
    private Queue jmsQueue;

    public void sendMessageLogin(String code, String strMessage) {
        TextMessage message;

        try (Connection connection = jmsFactory.createConnection();
             Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
             MessageProducer producer = session.createProducer(jmsQueue)) {

            JsonObject stockInformation = Json.createObjectBuilder()
                    .add("code", code)
                    .add("message", strMessage)
                    .add("timestamp", Instant.now().toEpochMilli()).build();

            message = session.createTextMessage();
            message.setText(stockInformation.toString());

            producer.send(message);

        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
