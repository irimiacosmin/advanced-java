package documentmanager.messaging;

import lombok.*;

import javax.json.JsonObject;
import javax.persistence.*;
import java.time.Instant;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Table(name = "jms_messages", schema = "docs_manager")
public class JMSMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String code;

    @Column(nullable = false)
    private String message;

    @Column(nullable = false)
    private Instant timestamp;

    public JMSMessage(JsonObject json) {
        this.code = json.getString("code");
        this.message = json.getString("message");
        this.timestamp = Instant.ofEpochMilli(json.getJsonNumber("timestamp").longValue());
    }
}
