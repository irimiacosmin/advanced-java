package documentmanager.utils;

public class GlassfishConstants {
    /*
            asadmin create-auth-realm
                --classname com.sun.enterprise.security.auth.realm.jdbc.JDBCRealm
                --property group-table="groups":group-name-column="name":defaultuser=admin
                        :password=admin:jaas-context="docs-realm":datasource-jndi="jdbc/jndiDataSource"
                        :user-table="users":user-name-column="username":password-column="password"
                        :assign-groups="admin,guest" docs-realm
     */

    public static final String JMS_TOPIC_DOCUMENTS = "Documents";
    public static final String PERSISTENCE_UNIT_NAME = "MSPWebAppPU";
}
