package documentmanager.models;

import documentmanager.entities.PeriodsEntity;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Period {

    private Date startDate;
    private Date endDate;
    private User user;

    public PeriodsEntity toEntity() {
        PeriodsEntity periodsEntity = new PeriodsEntity();
        periodsEntity.setStartDate(new java.sql.Date(this.getStartDate().getTime()));
        periodsEntity.setEndDate(new java.sql.Date(this.getEndDate().getTime()));
        return periodsEntity;
    }
}
