package documentmanager.beans;

import documentmanager.models.Period;
import documentmanager.repositories.PeriodRepository;
import documentmanager.services.AuthService;
import lombok.Getter;
import lombok.Setter;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
@Getter
@Setter
public class PeriodBean implements Serializable {

    @EJB
    private PeriodRepository periodRepository;

    @Inject
    private AuthService authService;

    private Period period = new Period();

    public String add() {
        period.setUser(authService.getCurrentUser());
        periodRepository.addPeriod(period);

        return "pages/home?faces-redirect=true";
    }
}
