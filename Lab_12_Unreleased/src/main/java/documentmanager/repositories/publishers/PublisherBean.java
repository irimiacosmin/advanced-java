package documentmanager.repositories.publishers;

import documentmanager.models.Document;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.*;

import static documentmanager.utils.GlassfishConstants.JMS_TOPIC_DOCUMENTS;

@Stateless
public class PublisherBean {

    @Resource
    private SessionContext sc;

    @Resource(lookup = JMS_TOPIC_DOCUMENTS)
    private Topic topic;

    @Inject
    private JMSContext context;

    public void sendDocumentMessage(Document document) {
        context.createProducer().send(topic, document);
    }
}
