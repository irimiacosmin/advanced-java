package documentmanager.services.alternatives;

public interface FileService {
    void uploadDocument(byte[] rawDocument, Integer registrationNumber);

    public byte[] getDocumentByRegistrationNumber(Integer registrationNumber);
}
