package documentmanager.services.alternatives;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Default;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static documentmanager.utils.FileConstants.FILE_PATH;

@Named
@SessionScoped
@Default
public class DocumentService implements Serializable, FileService {

    public void uploadDocument(byte[] rawDocument, Integer registrationNumber) {
        System.out.println("THIS IS A LOCAL SERVICE");
        try {
            Path path = Paths.get(FILE_PATH + registrationNumber);
            Files.write(path, rawDocument);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public byte[] getDocumentByRegistrationNumber(Integer registrationNumber) {
        System.out.println("THIS IS A LOCAL SERVICE");
        try {
            Path path = Paths.get(FILE_PATH + registrationNumber);
            return Files.readAllBytes(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
