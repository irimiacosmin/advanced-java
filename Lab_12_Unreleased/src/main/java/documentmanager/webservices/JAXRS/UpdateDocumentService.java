package documentmanager.webservices.JAXRS;

import documentmanager.models.Document;
import documentmanager.repositories.DocumentsRepository;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/documents")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UpdateDocumentService {

    @EJB
    private DocumentsRepository documentsRepository;

    @PUT
    public void updateDocument(Document document) {
        documentsRepository.updateDocument(document);
    }
}