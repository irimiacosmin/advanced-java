# Java Technologies - Lab 9
#
#
#
Continue the application created in the previous lab, exposing some functionalities through various Web services.
#
#### Point 1 (2p)

- The application will offer the following **JAX-WS** service:
    * ViewDocumentService that returns a "list" of the documents there were uploaded. The parameter of the web method will be an identifier for the user. If the parameter is null, then all documents should be considered.
    * Create a simple application client that invokes the service.
    * Define a **message handler** that will act as a cache, storing the parameters of the incoming SOAP messages and the returned responses. If the documents are modified by the application, reset the cache accordingly..

    
#### Point 2 (2p)

- Create RESTful Web services using **JAX-RS** that allow the interaction with at least one JPA entity, implementing CRUD operations. For example, the application may offer the following services:
    * AddDocumentService that allows adding a new document;
    * UpdateDocumentService that allows replacing an existing document;
    * DeleteDocumentService that allows deleting an existing document from the database.
- Use JSON for representing consumed or produced data.

#
#
#
#### Bibliography
- [Lab slides](https://profs.info.uaic.ro/~acf/tj/labs/pdf/lab9.pdf)
- [Java EE Tutorial: Web Services](https://docs.oracle.com/javaee/7/tutorial/partwebsvcs.htm#BNAYK)
- [MicroProfile: Optimizing Enterprise Java for a Microservices Architecture](https://microprofile.io/)
- [Microservices for Java EE Developers](https://blog.payara.fish/microservices-for-java-ee-developers2)
- [Use JAX-WS message handlers to address cross-cutting concerns: Caching](https://buddhiraju.wordpress.com/2011/12/28/use-soap-message-handler-for-cross-cutting-concerns-caching/)