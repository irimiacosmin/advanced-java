const {URL, URLSearchParams} = require('url');
const fetch = require("node-fetch");

const SERVLET_URL = 'http://localhost:8080/MainServlet';

async function doRequest(method = 'GET', url = "/", params = [], data = {}) {
    let urlObject = new URL(url)
    urlObject.search = new URLSearchParams(params).toString();
    let fetchParams = {
        method: method,
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
    };

    if (method !== 'GET') {
        fetchParams.body = JSON.stringify(data)
    }

    let response = await fetch(urlObject, fetchParams);
    return response.json();
}

let getRandomIntBetween = (min, max) => {
    return Math.floor(Math.random() * max) + min;
}

let letters = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'y', 'z'
]

let lettersLength = letters.length;
let howManyLetters = getRandomIntBetween(2, lettersLength);

for (let runs = 0; runs < 100; runs++) {
    console.log('Run #' + runs)
    console.time('Run #' + runs);
    let letterString = ''
    for (let i = 0; i < howManyLetters; i++) {
        letterString += letters[getRandomIntBetween(0, lettersLength)]
    }
    let params = {letters: letterString}
    doRequest('POST', SERVLET_URL, params)
        .then((res) => console.timeEnd('Run #' + runs))
        .catch(console.error)
}

