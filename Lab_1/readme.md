# Java Technologies - Lab 1
#
#
#
#### Point 1 (2p)

- Create a servlet that receives a number of letters and responds with a list of valid words formed with the given letters.
- The servlet will comunicate with a server-side resource such as a file or a database, containing the list of acceptable words. This list should be large enough; you may use aspell to generate a text file containing English words, or anything similar: [WordNet](https://wordnet.princeton.edu/), [dexonline](https://dexonline.ro/), etc.
For example, if the servlet receives the leters a,a,j,v it may return the list aa, ava, java (assuming it uses an en english dictionary).
- The servlet invocation will be done using a simple HTML form. The servlet will return the response as an HTML page.
Write in the server log the following information about each request: the HTTP method used, the IP-address of the client, the user-agent, the client language(s) and the parameters of the request. (Take a look at [HttpServletRequest API](https://javaee.github.io/javaee-spec/javadocs/javax/servlet/http/HttpServletRequest.html)).

#### Point 2 (0.5p)

- Invoke the service from a desktop application (Java, Python, .NET, etc.).
In this case, the servlet must respond with a simple text containing the list of words, instead of an HTML page.

#### Point 3 (1p)

- If the dictionary is large enough, the servlet may take some time in order to create the response.
Analyze the response times, concurrency issues and resource contention, invoking the servlet repeatedly, in an asynchronous manner.
#
#
#
#### Bibliography
- [Lab slides](https://profs.info.uaic.ro/~acf/tj/labs/pdf/lab1.pdf)
- [Getting Started with Web Applications](https://docs.oracle.com/javaee/6/tutorial/doc/bnadr.html)
- [Java Servlet Technology](https://docs.oracle.com/javaee/6/tutorial/doc/bnafd.html)
- [Java Server-Side Programming: Java Servlets](http://www.ntu.edu.sg/home/ehchua/programming/java/JavaServlets.html)