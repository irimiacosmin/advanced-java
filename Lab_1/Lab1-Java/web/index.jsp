<%--
  Created by IntelliJ IDEA.
  User: CosminIulianIrimia
  Date: 10/2/2020
  Time: 5:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome</title>
    <style>
        .form-call {
            display: flex;
        }

        .form-call > * {
            margin: 10px;
        }
    </style>
</head>
<body>
    <form action="MainServlet" method="post" class="form-call">
        <div>
            <label for="letters">Letters:</label>
            <input id="letters" type="text" name="letters">
        </div>
        <div>
            <input type="submit" value="Get Words">
        </div>
    </form>
</body>
</html>
