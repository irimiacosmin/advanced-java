<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="models.OutputModel" %>
<%--
  Created by IntelliJ IDEA.
  User: CosminIulianIrimia
  Date: 10/2/2020
  Time: 5:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome</title>
</head>
<body>
    <h1>Response:</h1>
    <%
        OutputModel outputModel = (OutputModel) request.getAttribute("outputModel");
        Object result = outputModel.getResponse();
        StringBuilder toBePrinted = new StringBuilder();

        if (result instanceof List) {
            List<String> itemList = (List<String>) outputModel.getResponse();
            for (String item : itemList) {
                toBePrinted.append("\n").append("<li>").append(item).append("</li>");
            }
        } else if (result instanceof String) {
            toBePrinted.append((String) result);
        }
    %>
    <ul>
        <%=toBePrinted.toString()%>
    </ul>
</body>
</html>
