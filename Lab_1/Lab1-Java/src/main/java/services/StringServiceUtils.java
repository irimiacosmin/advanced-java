package services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringServiceUtils {

    public static List<String> splitStringIntoLowercaseChars(String word) {
        if (word == null) {
            return new ArrayList<>();
        }
        return new ArrayList<>(Arrays.asList(word.toLowerCase().split("(?!^)")));
    }
}
