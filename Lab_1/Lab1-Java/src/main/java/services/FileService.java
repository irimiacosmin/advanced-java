package services;

import models.InputModel;

import javax.servlet.ServletContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileService {
    public static String DICTIONARY_FILE_NAME = "WEB-INF/dictionary.txt";

    private static List<String> readFileSync(ServletContext context, String filename) {
        List<String> fileLines = new ArrayList<>();
        try {
            InputStream inputStream = context.getResourceAsStream(filename);
            if (inputStream == null) {
                return fileLines;
            }
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String text;
            while ((text = bufferedReader.readLine()) != null) {
                fileLines.add(text);
            }
            return fileLines;
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public static List<String> getWordsMatchingLetters(ServletContext context, InputModel inputModel) {
        List<String> letters = inputModel.getLetters();
        int lettersSize = letters.size();
        return readFileSync(context, DICTIONARY_FILE_NAME)
                .stream()
                .filter(word -> word.length() > 1 && word.length() <= lettersSize)
                .filter(word -> matchesLetters(word, letters))
                .collect(Collectors.toList());
    }

    private static boolean matchesLetters(String word, List<String> letters) {
        List<String> wordLetters = StringServiceUtils.splitStringIntoLowercaseChars(word);
        wordLetters.removeAll(letters);
        return wordLetters.size() == 0;
    }
}
