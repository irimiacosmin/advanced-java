package services;

import models.InputModel;
import models.OutputModel;

import javax.servlet.ServletContext;

public class Processor {

    public static OutputModel computeResponse (ServletContext context, InputModel inputModel) {
        if (!inputModel.isSafe()) {
            return new OutputModel("Invalid parameters");
        }
        return new OutputModel(FileService.getWordsMatchingLetters(context, inputModel));
    }
}
