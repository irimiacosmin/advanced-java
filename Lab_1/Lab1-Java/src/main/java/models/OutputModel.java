package models;

import com.google.gson.Gson;
import lombok.Getter;

import java.util.List;

@Getter

public class OutputModel {

    private final Gson gson = new Gson();
    private final Object response;

    public OutputModel(List<String> lines) {
        this.response = lines;
    }

    public OutputModel(String message) {
        this.response = message;
    }

    public String toJSON() {
        return gson.toJson(this.response);
    }
}