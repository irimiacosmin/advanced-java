package models;

import lombok.Getter;
import services.StringServiceUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Getter
public class InputModel {
    private List<String> letters;
    private Boolean safe;

    public Boolean isSafe() {
        return safe;
    }

    public InputModel(String letterString) {
        try {
            this.letters = StringServiceUtils.splitStringIntoLowercaseChars(letterString);
            this.safe = true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            this.safe = false;
        }
    }

    public InputModel(HttpServletRequest req) {
        this(req.getParameter("letters"));
    }
}