import models.InputModel;
import models.OutputModel;
import services.Processor;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "/MainServlet", urlPatterns = "/MainServlet")
public class MainServlet extends HttpServlet {

    List<String> BROWSERS = new ArrayList<String>() {{
        add("CHROME");
        add("MOZILLA");
        add("SAFARI");
        add("OPERA");
    }};

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        logRequest(request);
        InputModel inputModel = new InputModel(request);
        ServletContext context = getServletContext();
        String userAgent = request.getHeader("user-agent").toUpperCase();
        OutputModel outputModel = Processor.computeResponse(context, inputModel);
        boolean requestWasMadeFromBrowser = BROWSERS.stream().anyMatch(userAgent::contains);
        if (requestWasMadeFromBrowser) {
            handleBrowser(request, response, outputModel);
        } else {
            handleService(response, outputModel);
        }
    }

    private void logRequest(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        String params = request.getParameterMap()
                .entrySet()
                .stream()
                .map((entry) -> "[" + entry.getKey() + ":" + String.join(" ", entry.getValue()) + "]")
                .collect(Collectors.joining(", "));

        StringBuilder loggingBuilder = new StringBuilder();
        loggingBuilder.append(System.lineSeparator()).append("=").append(" New request ").append("=");
        loggingBuilder.append(System.lineSeparator()).append("Method: ").append(request.getMethod());
        loggingBuilder.append(System.lineSeparator()).append("User-agent: ").append(request.getHeader("user-agent"));
        loggingBuilder.append(System.lineSeparator()).append("IP Address: ").append(ipAddress);
        loggingBuilder.append(System.lineSeparator()).append("Language: ").append(request.getHeader("Accept-Language"));
        loggingBuilder.append(System.lineSeparator()).append("Parameters: ").append(params);
        loggingBuilder.append(System.lineSeparator()).append("=");

        System.out.println(loggingBuilder.toString());
    }

    private void handleBrowser(HttpServletRequest httpServletRequest,
                               HttpServletResponse httpServletResponse,
                               OutputModel outputModel) throws IOException {
        httpServletRequest.setAttribute("outputModel", outputModel);
        RequestDispatcher dispatcher = httpServletRequest.getRequestDispatcher("welcome.jsp");
        if (dispatcher != null) {
            try {
                dispatcher.forward(httpServletRequest, httpServletResponse);
            } catch (ServletException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleService(HttpServletResponse httpServletResponse, OutputModel outputModel) throws IOException {
        httpServletResponse.getWriter().print(outputModel.toJSON());
    }
}
