# Java Technologies - Lab 11
#
#
#
Java EE and MicroProfile Security
#
#### Point 1 (3p)

- Add security features to the application created in the previous labs, using standard mechanisms offered by Java EE for:
    - Authentication, using a JDBC Realm;
    - Controlling the access to Web resources, using Web constraints;
    - Securing EJB components and REST services.

    
#### Point 2 (2p)

- Add security features to the microservice(s) created in the previous lab:
    - Secure the MicroProfile CRUD service, using signed JWTs.
    - Invoke the microservice (from another microservice or a JS client) in order to test it.
#
#
#
#### Bibliography
- [Lab slides](https://profs.info.uaic.ro/~acf/tj/slides/security_slide_en.pdf)
- [Java EE Tutorial: Security](https://docs.oracle.com/javaee/7/tutorial/partsecurity.htm#GIJRP)
- [Java EE Security Essentials](https://dzone.com/refcardz/getting-started-java-ee?chapter=1)
- [JDBC Realm and Form Based Authentication](https://dzone.com/articles/jdbc-realm-and-form-based)
- [Introduction to JSON Web Tokens](https://jwt.io/introduction/)
- [Eclipse MicroProfile JWT Authentication API](https://docs.payara.fish/enterprise/docs/5.23.0/documentation/microprofile/jwt.html)
- [MicroProfile JSON Web Token (JWT)](https://www.tomitribe.com/blog/microprofile-json-web-token-jwt/)
- [Securing microservices with JSON Web Tokens](https://openliberty.io/guides/microprofile-jwt.html)
- [Eclipse MicroProfile - JWT RBAC Security (MP-JWT)](https://www.eclipse.org/community/eclipse_newsletter/2017/september/article2.php)
#
#
#

## Running the projects

In order to run the services, we must generate the JAR fileS:
In classpath of [Lab 11](../Lab_11) and  [Lab 11 Invoker](../Lab_11_Invoker) run `mvn install` or run the **Install** lifecycle option from the right **Maven** Menu

To start the services follow the instructions IN THE PROVIDED ORDER:
##### Lab 11

1. Create a new configurations of type **JAR Application**.
2. Select the **JAR Path** to `payara-micro-5.2020.7.jar` included in the classpath
3. Insert `--deploy target/Lab_8_1-1.0-SNAPSHOT.war --autoBindHttp --name DocumentService --contextRoot /rest` in  **Program Arguments**
4. Run the application and wait for it to start
5. The app will now expose a new endpoint: `http://localhost:8080/rest/webresources/documents/classified` that is protected by role auth using a JWT. You can try to access it but you will receive `401-Unauthorized` for the moment.

##### Lab 11 Invoker
1. Create a new configurations of type **JAR Application**.
2. Select the **JAR Path** to `payara-micro-5.2020.7.jar` included in the Lab 11 classpath
3. Insert `--deploy target/demo.war --autoBindHttp --name Invoker --contextRoot /rest` in  **Program Arguments**
4. The app will now expose a new endpoint: `http://localhost:8081/rest/data/secured/test`

The **Lab_11_Invoker** service will generate a JWT and then will invoke the **Lab_11** service's `/documents/classified` endpoint succesfully.