# Java Technologies - Lab 6
#
#
#
#### Point 1 (2p)

- Rewrite the persistence layer of the application created for the previous laboratory using a technology that implements the **JPA specifications**.
    * Define the persistence unit using a data source configured as a JDBC Resource.
    * Create the EntityManager objects using dependency injection.
    * Define the mappings using JPA-only annotations.
   * Implement the repository classes using **JPA-QL**.
    
#### Point 2 (1p)

- Meetings can be of various types, for example conference, workshop, etc, each having specific properties, such as list of speakers or required resources, etc.
- Use **inheritance** mapping in order to define this new model. Adapt the user interface accordingly.

#### Point 3 (1p)
- Add a "meeting search page". This page will allow specifying various filters: person's name, location, starting time, etc.
    * Each filter will have a checkox - if it is checked then the filter will be taken into consideration.
    * The query must be implemented using **JPA Criteria API**.
    
#### Point 4 (0.5p)
- Create a test unit for the CRUD operations of at least one entity class.

#### Notes
- It is **required** to use a JPA implementation and not just any third-party ORM library.
#
#
#
#### Bibliography
- [Lab slides](https://profs.info.uaic.ro/~acf/tj/labs/pdf/lab6.pdf)
- [Java EE Tutorial: Persistence](https://docs.oracle.com/javaee/7/tutorial/partpersist.htm#BNBPY)
- [Eclipse Link Documentation Center Technology](https://www.eclipse.org/eclipselink/documentation/)
- [Hibernate EntityManager](https://docs.jboss.org/hibernate/entitymanager/3.6/reference/en/html_single/)
- [Dynamic, typesafe queries in JPA](https://developer.ibm.com/articles/j-typesafejpa/)
- [Criteria Queries](https://docs.jboss.org/hibernate/entitymanager/3.5/reference/en/html/querycriteria.html)
- [Java Persistence Performance](http://java-persistence-performance.blogspot.com/2011/06/how-to-improve-jpa-performance-by-1825.html)