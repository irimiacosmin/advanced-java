package unit.entities;


import lab4.entities.PersonEntity;
import lab4.models.Person;
import lab4.persistance.PersonRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PersonEntityUnitTest {

    @InjectMocks
    private PersonEntity personEntity;

    @InjectMocks
    private Person person;

    @Mock
    private PersonRepository personRepository;

    @Before
    public void init() {
        personEntity = new PersonEntity(1, "Irimia Cosmin", "irimia.cosmin@gmail.com");
        person = new Person(personEntity);
    }

    @Test
    public void create() throws Exception {
        when(personRepository.add(any(Person.class))).thenReturn(person.getId());
        int result = personRepository.add(person);

        Assert.assertEquals(personEntity.getId(), person.getId());
        Assert.assertEquals(result, (int) person.getId());
    }

    @Test
    public void read() {
        when(personRepository.getById(1)).thenReturn(person);
        Person result = personRepository.getById(1);

        Assert.assertEquals(result, person);
    }

    @Test
    public void update() {
        when(personRepository.update(any(Person.class))).thenReturn(person);
        Person result = personRepository.update(person);

        Assert.assertEquals(result, person);
    }

    @Test
    public void delete() {
        when(personRepository.delete(any(Person.class))).thenReturn(Boolean.TRUE);
        boolean result = personRepository.delete(person);

        Assert.assertEquals(result, Boolean.TRUE);
    }

}
