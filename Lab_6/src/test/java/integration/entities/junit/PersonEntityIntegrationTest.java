package integration.entities.junit;

import lab4.entities.PersonEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.jupiter.api.*;
import utils.HibernateUtil;

import java.util.List;

public class PersonEntityIntegrationTest {

    private static SessionFactory sessionFactory;
    private Session session;
    private static PersonEntity personEntity;
    private static final long currentTimestamp = System.currentTimeMillis();

    @BeforeAll
    public static void setup() {
        sessionFactory = HibernateUtil.getSessionFactory();
        System.out.println("SessionFactory created");
        String email = "irimia." + currentTimestamp + ".cosmin@gmail.com";
        personEntity = new PersonEntity(null, "Irimia Cosmin Test", email);
    }

    @BeforeEach
    public void openSession() {
        session = sessionFactory.openSession();
        System.out.println("Session created");
    }

    @AfterEach
    public void closeSession() {
        if (session != null) {
            session.close();
        }
        System.out.println("Session closed\n");
    }

    @AfterAll
    public static void tearDown() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
        System.out.println("SessionFactory destroyed");
    }

    @Test
    @Order(1)
    public void create() {
        System.out.println("Running create...");

        session.beginTransaction();
        Integer id = (Integer) session.save(personEntity);
        session.getTransaction().commit();
        Assertions.assertTrue(id > 0);
        personEntity.setId(id);
    }

    @Test
    @Order(2)
    public void readBeforeUpdate() {
        System.out.println("Running read...");

        Integer id = personEntity.getId();

        PersonEntity returnedEntity = session.find(PersonEntity.class, id);

        Assertions.assertEquals(personEntity.getName(), returnedEntity.getName());
        Assertions.assertEquals(personEntity.getEmail(), returnedEntity.getEmail());
    }

    @Test
    @Order(3)
    public void update() {
        System.out.println("Running update...");

        Integer id = personEntity.getId();
        String expectedEmail = "another." + currentTimestamp + "@email.com";
        String expectedName = "Another name";
        PersonEntity newYearOldMe = new PersonEntity(id, expectedName, expectedEmail);

        session.beginTransaction();
        session.update(newYearOldMe);
        session.getTransaction().commit();

        PersonEntity updatedPerson = session.find(PersonEntity.class, personEntity.getId());

        Assertions.assertEquals(expectedName, updatedPerson.getName());
        Assertions.assertEquals(expectedEmail, updatedPerson.getEmail());
    }

    @Test
    @Order(4)
    public void readAfterUpdate() {
        System.out.println("Running read...");

        Integer id = personEntity.getId();

        PersonEntity returnedEntity = session.find(PersonEntity.class, id);

        Assertions.assertEquals("Another name", returnedEntity.getName());
        Assertions.assertEquals("another." + currentTimestamp + "@email.com", returnedEntity.getEmail());
    }

    @Test
    @Order(5)
    public void getAll() {
        System.out.println("Running testList...");

        Query<PersonEntity> query = session.createQuery("from PersonEntity", PersonEntity.class);
        List<PersonEntity> resultList = query.getResultList();

        Assertions.assertFalse(resultList.isEmpty());
    }

    @Test
    @Order(6)
    public void testDelete() {
        System.out.println("Running testDelete...");

        Integer id = personEntity.getId();
        PersonEntity personEntity = session.find(PersonEntity.class, id);

        session.beginTransaction();
        session.delete(personEntity);
        session.getTransaction().commit();

        PersonEntity deletedProduct = session.find(PersonEntity.class, id);

        Assertions.assertNull(deletedProduct);
    }
}
