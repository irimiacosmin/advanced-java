package integration.entities.spock


import lab4.entities.LocationEntity
import org.hibernate.Session
import org.hibernate.SessionFactory
import spock.lang.Specification
import spock.lang.Subject
import utils.HibernateUtil

class LocationEntitySpec extends Specification {

    @Subject
    static LocationEntity entity;

    static SessionFactory sessionFactory;
    static Session session;
    static long currentTimestamp = System.currentTimeMillis();

    def setupSpec() {
        sessionFactory = HibernateUtil.getSessionFactory();
        entity = new LocationEntity(null, "Location " + currentTimestamp);
    }

    def setup() {
        session = sessionFactory.openSession();
        System.out.println("Session created");
    }

    def cleanup() {
        if (session != null) {
            session.close();
        }
        System.out.println("Session closed\n");
    }

    def cleanupSpec() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
        System.out.println("SessionFactory destroyed");
    }

    def "create"() {
        System.out.println("Running create...");
        given:
            session.beginTransaction();
        when:
            Integer id = (Integer) session.save(entity);
            session.getTransaction().commit();
            entity.setId(id);
        then:
            id > 0
    }

    def "readBeforeUpdate"() {
        System.out.println("Running read...");
        given:
            Integer id = entity.getId();
        when:
            def returnedEntity = session.find(LocationEntity.class, id);
        then:
            returnedEntity.getName() == entity.getName()
    }

    def "update"() {
        System.out.println("Running update...");
        given:
            Integer id = entity.getId()

            def newYearOldMe = new LocationEntity(id, expectedName)
        when:
            session.beginTransaction()
            session.update(newYearOldMe)
            session.getTransaction().commit()
            def updatedEntity = session.find(LocationEntity.class, entity.getId())
        then:
            updatedEntity.getName() == expectedName
        where:
            expectedName = "Location " + currentTimestamp
    }

    def "readAfterUpdate"() {
        System.out.println("Running readAfterUpdate...");
        given:
            Integer id = entity.getId()
        when:
            def returnedEntity = session.find(LocationEntity.class, id);
        then:
            returnedEntity.getName() == "Location " + currentTimestamp
    }

    def "getAll"() {
        System.out.println("Running getAll...");
        when:
            def query = session.createQuery("from LocationEntity", LocationEntity.class);
            def resultList = query.getResultList();
        then:
            !resultList.isEmpty()
    }

    def "testDelete"() {
        System.out.println("Running testDelete...");
        given:
            Integer id = entity.getId()
            def entityToDelete = session.find(LocationEntity.class, id);
        when:
            session.beginTransaction()
            session.delete(entityToDelete)
            session.getTransaction().commit()
            def deletedEntity = session.find(LocationEntity.class, id);
        then:
            deletedEntity == null
    }
}
