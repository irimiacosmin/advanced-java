package integration.entities.spock

import lab4.entities.LocationEntity
import lab4.entities.MeetingEntity
import lab4.entities.MeetingPersonEntity
import lab4.entities.MeetingTypeEntity
import lab4.entities.PersonEntity
import lab4.entities.Topic
import org.hibernate.Session
import org.hibernate.SessionFactory
import spock.lang.Specification
import spock.lang.Subject
import utils.HibernateUtil

import java.time.LocalDateTime

class MeetingPersonEntitySpec extends Specification {

    @Subject
    static MeetingPersonEntity entity;

    static Topic topic
    static LocalDateTime startingDateTime
    static Integer durationMinutes
    static LocationEntity location
    static MeetingTypeEntity type
    static MeetingEntity meetingEntity;

    static PersonEntity personEntity;

    static SessionFactory sessionFactory;
    static Session session;
    static long currentTimestamp = System.currentTimeMillis();

    def setupSpec() {
        sessionFactory = HibernateUtil.getSessionFactory();

        sessionFactory = HibernateUtil.getSessionFactory();
        topic = Topic.MANAGEMENT_PLANNING
        startingDateTime = LocalDateTime.now()
        durationMinutes = 10
        location = new LocationEntity(null, "Here")
        type = new MeetingTypeEntity(null, "Something", null)
        meetingEntity = new MeetingEntity(null, topic, startingDateTime, durationMinutes, location, type);
        String email = "irimia." + currentTimestamp + ".cosmin@gmail.com";
        personEntity = new PersonEntity(null, "Irimia Cosmin Test", email);

        entity = new MeetingPersonEntity(null, meetingEntity, personEntity);
    }

    def setup() {
        session = sessionFactory.openSession();
        System.out.println("Session created");
    }

    def cleanup() {
        if (session != null) {
            session.close();
        }
        System.out.println("Session closed\n");
    }

    def cleanupSpec() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
        System.out.println("SessionFactory destroyed");
    }

    def "create"() {
        System.out.println("Running create...");
        given:
            session.beginTransaction();
        when:
            Integer id = (Integer) session.save(entity);
            session.getTransaction().commit();
            entity.setId(id);
        then:
            id > 0
    }

    def "readBeforeUpdate"() {
        System.out.println("Running read...");
        given:
            Integer id = entity.getId();
        when:
            def returnedEntity = session.find(MeetingPersonEntity.class, id);
        then:
            returnedEntity.getMeeting().getId() == meetingEntity.getId()
            returnedEntity.getMeeting().getType() == meetingEntity.getType()
            returnedEntity.getMeeting().getTopic() == meetingEntity.getTopic()
            returnedEntity.getMeeting().getStartingDateTime() == meetingEntity.getStartingDateTime()
            returnedEntity.getMeeting().getLocation().getId() == meetingEntity.getLocation().getId()
            returnedEntity.getMeeting().getLocation().getName() == meetingEntity.getLocation().getName()
            returnedEntity.getMeeting().getDurationMinutes() == meetingEntity.getDurationMinutes()

            returnedEntity.getPerson().getId() == personEntity.getId()
            returnedEntity.getPerson().getName() == personEntity.getName()
            returnedEntity.getPerson().getEmail() == personEntity.getEmail()
    }

    def "update"() {
        System.out.println("Running update...");
        given:
            Integer id = entity.getId()
            def newPerson = personEntity
            newPerson.setName(changedName)
            def newYearOldMe = new MeetingPersonEntity(id, meetingEntity, personEntity)
        when:
            session.beginTransaction()
            session.update(newYearOldMe)
            session.getTransaction().commit()
            def returnedEntity = session.find(MeetingPersonEntity.class, entity.getId())
        then:
            returnedEntity.getMeeting().getId() == meetingEntity.getId()
            returnedEntity.getMeeting().getType() == meetingEntity.getType()
            returnedEntity.getMeeting().getTopic() == meetingEntity.getTopic()
            returnedEntity.getMeeting().getStartingDateTime() == meetingEntity.getStartingDateTime()
            returnedEntity.getMeeting().getLocation().getId() == meetingEntity.getLocation().getId()
            returnedEntity.getMeeting().getLocation().getName() == meetingEntity.getLocation().getName()
            returnedEntity.getMeeting().getDurationMinutes() == meetingEntity.getDurationMinutes()

            returnedEntity.getPerson().getId() == personEntity.getId()
            returnedEntity.getPerson().getName() == changedName
            returnedEntity.getPerson().getEmail() == personEntity.getEmail()
        where:
            changedName = "Altceva"
    }

    def "readAfterUpdate"() {
        System.out.println("Running readAfterUpdate...");
        given:
            Integer id = entity.getId()
        when:
            def returnedEntity = session.find(MeetingPersonEntity.class, id);
        then:
            returnedEntity.getMeeting().getId() == meetingEntity.getId()
            returnedEntity.getMeeting().getType() == meetingEntity.getType()
            returnedEntity.getMeeting().getTopic() == meetingEntity.getTopic()
            returnedEntity.getMeeting().getStartingDateTime() == meetingEntity.getStartingDateTime()
            returnedEntity.getMeeting().getLocation().getId() == meetingEntity.getLocation().getId()
            returnedEntity.getMeeting().getLocation().getName() == meetingEntity.getLocation().getName()
            returnedEntity.getMeeting().getDurationMinutes() == meetingEntity.getDurationMinutes()

            returnedEntity.getPerson().getId() == personEntity.getId()
            returnedEntity.getPerson().getName() == changedName
            returnedEntity.getPerson().getEmail() == personEntity.getEmail()
        where:
            changedName = "Altceva"
    }

    def "getAll"() {
        System.out.println("Running getAll...");
        when:
            def query = session.createQuery("from MeetingPersonEntity", MeetingPersonEntity.class);
            def resultList = query.getResultList();
        then:
            !resultList.isEmpty()
    }

    def "testDelete"() {
        System.out.println("Running testDelete...");
        given:
            Integer id = entity.getId()
            def entityToDelete = session.find(MeetingPersonEntity.class, id);
        when:
            session.beginTransaction()
            session.delete(entityToDelete)
            session.getTransaction().commit()
            def deletedEntity = session.find(MeetingPersonEntity.class, id);
        then:
            deletedEntity == null
    }
}
