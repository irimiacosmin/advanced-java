package integration.entities.spock

import lab4.entities.LocationEntity
import lab4.entities.MeetingEntity
import lab4.entities.MeetingTypeEntity
import lab4.entities.PersonEntity
import lab4.entities.Topic
import org.hibernate.Session
import org.hibernate.SessionFactory
import spock.lang.Specification
import spock.lang.Subject
import utils.HibernateUtil

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import java.time.LocalDateTime

class MeetingEntitySpec extends Specification {

    @Subject
    static MeetingEntity entity;

    static SessionFactory sessionFactory;
    static Session session;
    static long currentTimestamp = System.currentTimeMillis();

    static Topic topic
    static LocalDateTime startingDateTime
    static Integer durationMinutes
    static LocationEntity location
    static MeetingTypeEntity type

    def setupSpec() {
        sessionFactory = HibernateUtil.getSessionFactory();
        topic = Topic.MANAGEMENT_PLANNING
        startingDateTime = LocalDateTime.now()
        durationMinutes = 10
        location = new LocationEntity(null, "Here")
        type = new MeetingTypeEntity(null, "Something", null)
        entity = new MeetingEntity(null, topic, startingDateTime, durationMinutes, location, type);
    }

    def setup() {
        session = sessionFactory.openSession();
        System.out.println("Session created");
    }

    def cleanup() {
        if (session != null) {
            session.close();
        }
        System.out.println("Session closed\n");
    }

    def cleanupSpec() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
        System.out.println("SessionFactory destroyed");
    }

    def "create"() {
        System.out.println("Running create...");
        given:
            session.beginTransaction();
        when:
            Integer id = (Integer) session.save(entity);
            session.getTransaction().commit();
            entity.setId(id);
        then:
            id > 0
    }

    def "readBeforeUpdate"() {
        System.out.println("Running read...");
        given:
            Integer id = entity.getId();
        when:
            def returnedEntity = session.find(MeetingEntity.class, id);
        then:
            returnedEntity.getId() == id
            returnedEntity.getDurationMinutes() == durationMinutes
            returnedEntity.getLocation().getId() == location.getId()
            returnedEntity.getLocation().getName() == location.getName()
            returnedEntity.getStartingDateTime() == startingDateTime
            returnedEntity.getTopic() == topic
            returnedEntity.getType() == type
    }

    def "update"() {
        System.out.println("Running update...");
        given:
            Integer id = entity.getId()
            def newYearOldMe = new MeetingEntity(id, topic, startingDateTime, durationMinutes + 10, location, type)
        when:
            session.beginTransaction()
            session.update(newYearOldMe)
            session.getTransaction().commit()
            def updatedEntity = session.find(MeetingEntity.class, entity.getId())
        then:
            updatedEntity.getId() == id
            updatedEntity.getDurationMinutes() == durationMinutes + 10
            updatedEntity.getLocation().getId() == location.getId()
            updatedEntity.getLocation().getName() == location.getName()
            updatedEntity.getStartingDateTime() == startingDateTime
            updatedEntity.getTopic() == topic
            updatedEntity.getType() == type
    }

    def "readAfterUpdate"() {
        System.out.println("Running readAfterUpdate...");
        given:
            Integer id = entity.getId()
        when:
            def returnedEntity = session.find(MeetingEntity.class, id);
        then:
            returnedEntity.getId() == id
            returnedEntity.getDurationMinutes() == durationMinutes + 10
            returnedEntity.getLocation().getId() == location.getId()
            returnedEntity.getLocation().getName() == location.getName()
            returnedEntity.getStartingDateTime() == startingDateTime
            returnedEntity.getTopic() == topic
            returnedEntity.getType() == type
    }

    def "getAll"() {
        System.out.println("Running getAll...");
        when:
            def query = session.createQuery("from MeetingEntity", MeetingEntity.class);
            def resultList = query.getResultList();
        then:
            !resultList.isEmpty()
    }

    def "testDelete"() {
        System.out.println("Running testDelete...");
        given:
            Integer id = entity.getId()
            def entityToDelete = session.find(MeetingEntity.class, id);
        when:
            session.beginTransaction()
            session.delete(entityToDelete)
            session.getTransaction().commit()
            def deletedEntity = session.find(MeetingEntity.class, id);
        then:
            deletedEntity == null
    }
}
