package integration.entities.spock


import lab4.entities.DistanceEntity
import lab4.entities.LocationEntity
import org.hibernate.Session
import org.hibernate.SessionFactory
import spock.lang.Specification
import spock.lang.Subject
import utils.HibernateUtil

class DistanceEntitySpec extends Specification {

    @Subject
    static DistanceEntity entity;

    static LocationEntity firstLocation
    static LocationEntity secondLocation
    static Integer duration

    static SessionFactory sessionFactory;
    static Session session;
    static long currentTimestamp = System.currentTimeMillis();

    def setupSpec() {
        sessionFactory = HibernateUtil.getSessionFactory();
        String email = "irimia." + currentTimestamp + ".cosmin@gmail.com";
        firstLocation = new LocationEntity(null, "First location " + currentTimestamp)
        secondLocation = new LocationEntity(null, "Second location " + currentTimestamp)
        duration = 10
        entity = new DistanceEntity(null, firstLocation, secondLocation, duration);
    }

    def setup() {
        session = sessionFactory.openSession();
        System.out.println("Session created");
    }

    def cleanup() {
        if (session != null) {
            session.close();
        }
        System.out.println("Session closed\n");
    }

    def cleanupSpec() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
        System.out.println("SessionFactory destroyed");
    }

    def "create"() {
        System.out.println("Running create...");
        given:
            session.beginTransaction();
        when:
            Integer id = (Integer) session.save(entity);
            session.getTransaction().commit();
            entity.setId(id);
        then:
            id > 0
    }

    def "readBeforeUpdate"() {
        System.out.println("Running read...");
        given:
            Integer id = entity.getId();
        when:
            def returnedEntity = session.find(DistanceEntity.class, id);
        then:
            returnedEntity.getFirstLocation().getId() == firstLocation.getId()
            returnedEntity.getFirstLocation().getName() == firstLocation.getName()
            returnedEntity.getSecondLocation().getId() == secondLocation.getId()
            returnedEntity.getSecondLocation().getName() == secondLocation.getName()
            returnedEntity.getDurationMinutes() == duration
    }

    def "update"() {
        System.out.println("Running update...");
        given:
            Integer id = entity.getId()

            def newYearOldMe = new DistanceEntity(id, firstLocation, secondLocation, duration + 10)
        when:
            session.beginTransaction()
            session.update(newYearOldMe)
            session.getTransaction().commit()
            def updatedEntity = session.find(DistanceEntity.class, entity.getId())
        then:
            updatedEntity.getFirstLocation() == firstLocation
            updatedEntity.getSecondLocation() == secondLocation
            updatedEntity.getDurationMinutes() == duration + 10
    }

    def "readAfterUpdate"() {
        System.out.println("Running readAfterUpdate...");
        given:
            Integer id = entity.getId()
        when:
            def returnedEntity = session.find(DistanceEntity.class, id);
        then:
            returnedEntity.getFirstLocation().getId() == firstLocation.getId()
            returnedEntity.getFirstLocation().getName() == firstLocation.getName()
            returnedEntity.getSecondLocation().getId() == secondLocation.getId()
            returnedEntity.getSecondLocation().getName() == secondLocation.getName()
            returnedEntity.getDurationMinutes() == duration + 10
    }

    def "getAll"() {
        System.out.println("Running getAll...");
        when:
            def query = session.createQuery("from DistanceEntity", DistanceEntity.class);
            def resultList = query.getResultList();
        then:
            !resultList.isEmpty()
    }

    def "testDelete"() {
        System.out.println("Running testDelete...");
        given:
            Integer id = entity.getId()
            def entityToDelete = session.find(DistanceEntity.class, id);
        when:
            session.beginTransaction()
            session.delete(entityToDelete)
            session.getTransaction().commit()
            def deletedEntity = session.find(DistanceEntity.class, id);
        then:
            deletedEntity == null
    }
}
