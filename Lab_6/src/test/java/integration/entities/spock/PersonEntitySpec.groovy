package integration.entities.spock

import lab4.entities.AbstractEntity
import lab4.entities.PersonEntity
import org.hibernate.Session
import org.hibernate.SessionFactory
import spock.lang.Specification
import spock.lang.Subject
import utils.HibernateUtil

class PersonEntitySpec extends Specification {

    @Subject
    static PersonEntity entity;

    static SessionFactory sessionFactory;
    static Session session;
    static long currentTimestamp = System.currentTimeMillis();

    def setupSpec() {
        sessionFactory = HibernateUtil.getSessionFactory();
        String email = "irimia." + currentTimestamp + ".cosmin@gmail.com";
        entity = new PersonEntity(null, "Irimia Cosmin Test", email);
    }

    def setup() {
        session = sessionFactory.openSession();
        System.out.println("Session created");
    }

    def cleanup() {
        if (session != null) {
            session.close();
        }
        System.out.println("Session closed\n");
    }

    def cleanupSpec() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
        System.out.println("SessionFactory destroyed");
    }

    def "create"() {
        System.out.println("Running create...");
        given:
            session.beginTransaction();
        when:
            Integer id = (Integer) session.save(entity);
            session.getTransaction().commit();
            entity.setId(id);
        then:
            id > 0
    }

    def "readBeforeUpdate"() {
        System.out.println("Running read...");
        given:
            Integer id = entity.getId();
        when:
            def returnedEntity = session.find(PersonEntity.class, id);
        then:
            returnedEntity.getName() == expectedName
            returnedEntity.getEmail() == expectedEmail
        where:
            expectedName     | expectedEmail
            entity.getName() | entity.getEmail()
    }

    def "update"() {
        System.out.println("Running update...");
        given:
            Integer id = entity.getId()

            def newYearOldMe = new PersonEntity(id, expectedName, expectedEmail)
        when:
            session.beginTransaction()
            session.update(newYearOldMe)
            session.getTransaction().commit()
            def updatedEntity = session.find(PersonEntity.class, entity.getId())
        then:
            updatedEntity.getName() == expectedName
            updatedEntity.getEmail() == expectedEmail
        where:
            expectedName   | expectedEmail
            "Another name" | "another." + currentTimestamp + "@email.com"
    }

    def "readAfterUpdate"() {
        System.out.println("Running readAfterUpdate...");
        given:
            Integer id = entity.getId()
        when:
            def returnedEntity = session.find(PersonEntity.class, id);
        then:
            returnedEntity.getName() == expectedName
            returnedEntity.getEmail() == expectedEmail
        where:
            expectedName   | expectedEmail
            "Another name" | "another." + currentTimestamp + "@email.com"
    }

    def "getAll"() {
        System.out.println("Running getAll...");
        when:
            def query = session.createQuery("from PersonEntity", PersonEntity.class);
            def resultList = query.getResultList();
        then:
            !resultList.isEmpty()
    }

    def "testDelete"() {
        System.out.println("Running testDelete...");
        given:
            Integer id = entity.getId()
            def personToDelete = session.find(PersonEntity.class, id);
        when:
            session.beginTransaction()
            session.delete(personToDelete)
            session.getTransaction().commit()
            def deletedProduct = session.find(PersonEntity.class, id);
        then:
            deletedProduct == null
    }
}
