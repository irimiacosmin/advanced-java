package lab4.entities;

import lombok.*;

import javax.persistence.Entity;
import java.io.Serializable;

@Getter
@AllArgsConstructor
@ToString
public enum Topic implements Serializable {
    RELEASE_PLAN, MANAGEMENT_PLANNING, SPRINT_REVIEW;
}
