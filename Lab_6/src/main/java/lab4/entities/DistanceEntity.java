package lab4.entities;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Table(name = "distance", schema = "advanced_java")
public class DistanceEntity implements AbstractEntity<Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JoinColumn(name = "first_location_id", nullable = false)
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private LocationEntity firstLocation;

    @JoinColumn(name = "second_location_id", nullable = false)
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private LocationEntity secondLocation;

    @Column(name = "duration_in_minutes", nullable = false)
    private Integer durationMinutes;
}