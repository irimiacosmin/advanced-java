package lab4.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Table(name = "meeting", schema = "advanced_java")
public class MeetingEntity implements AbstractEntity<Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "topic", nullable = false)
    private Topic topic;

    @Column(name = "timestamp", nullable = false)
    private LocalDateTime startingDateTime;

    @Column(name = "duration_in_minutes", nullable = false)
    private Integer durationMinutes;

    @JoinColumn(name = "location_id", nullable = false)
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private LocationEntity location;

    @JoinColumn(name = "meeting_type_id", nullable = false)
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private MeetingTypeEntity type;
}
