package lab4.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class SpeakerEntity implements JsonbDataEntity {

    Integer id;
    String name;
    String expertise;
}
