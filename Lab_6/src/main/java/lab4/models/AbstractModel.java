package lab4.models;

public interface AbstractModel<T, E> {
    E toEntity();
}