package lab4.models;

import lab4.entities.LocationEntity;
import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Location implements AbstractModel<Location, LocationEntity> {
    public Integer id;

    @Size(min = 3)
    public String name;

    public Location(String name) {
        this.id = null;
        this.name = name;
    }

    public Location(LocationEntity entity) {
        this.id = entity.getId();
        this.name = entity.getName();
    }

    @Override
    public LocationEntity toEntity() {
        return LocationEntity.builder()
                .id(this.getId())
                .name(this.getName())
                .build();
    }
}
