package lab4.persistance;

import lab4.entities.MeetingEntity;
import lab4.entities.MeetingPersonEntity;
import lab4.entities.PersonEntity;
import lab4.exceptions.EntryNotFoundException;
import lab4.models.Meeting;
import lab4.models.Person;
import lab4.utils.JPQLConstants;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Query;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Named
@RequestScoped
public class MeetingRepository extends AbstractRepository implements SkeletonRepository<Meeting> {

    @Inject
    private PersonRepository personRepository;

    @Override
    public int add(Meeting entry) throws Exception {
        return create(entry.toEntity());
    }

    @Override
    public Meeting getById(int id) throws EntryNotFoundException {
        return getEntries(JPQLConstants.GET_BY_ID(MeetingEntity.class, id))
                .stream()
                .findFirst()
                .orElseThrow(EntryNotFoundException::new);
    }

    @Override
    public List<Meeting> getAll() {
        return getEntries(JPQLConstants.SELECT_ALL(MeetingEntity.class));
    }

    public List<Meeting> getAllFiltered(Query query) {
        return ((Collection<MeetingEntity>) query.getResultList())
                .stream()
                .map(meetingEntity -> new Meeting(meetingEntity, personRepository.getAllByMeetingId(meetingEntity.getId())))
                .collect(Collectors.toList());
    }


    private List<Meeting> getEntries(String query) {
        return ((Collection<MeetingEntity>) mspAppPU.createQuery(query).getResultList())
                .stream()
                .map(meetingEntity -> new Meeting(meetingEntity, personRepository.getAllByMeetingId(meetingEntity.getId())))
                .collect(Collectors.toList());
    }

    private void assignPersonToMeeting(Meeting meeting) throws Exception {
        for (Person person : meeting.getAttendees()) {
            MeetingPersonEntity mpe = MeetingPersonEntity.builder()
                    .meeting(meeting.toEntity())
                    .person(person.toEntity())
                    .build();
            create(mpe);
        }
    }

    private List<Person> getAttendees(Integer meetingId) {
        String attendees = JPQLConstants.getFilledQuery(JPQLConstants.GET_ALL_MEETING_ATTENDEES_QUERY, meetingId);
        return personRepository.getEntries(attendees);
    }
}
