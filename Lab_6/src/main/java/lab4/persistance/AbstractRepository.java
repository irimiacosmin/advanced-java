package lab4.persistance;

import lab4.entities.AbstractEntity;
import lombok.Getter;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;

@Getter
public abstract class AbstractRepository {

    @PersistenceContext(name = "MSPWebAppPU")
    protected EntityManager mspAppPU;

    int create(AbstractEntity entity) throws Exception {
        EntityTransaction transaction = mspAppPU.getTransaction();
        transaction.begin();
        mspAppPU.persist(entity);
        mspAppPU.flush();
        transaction.commit();
        return (int) entity.getId();
    }
}
