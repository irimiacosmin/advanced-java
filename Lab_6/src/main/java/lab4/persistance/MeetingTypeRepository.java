package lab4.persistance;

import lab4.entities.MeetingEntity;
import lab4.entities.MeetingPersonEntity;
import lab4.entities.MeetingTypeEntity;
import lab4.exceptions.EntryNotFoundException;
import lab4.models.Meeting;
import lab4.models.Person;
import lab4.utils.JPQLConstants;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Named
@RequestScoped
public class MeetingTypeRepository extends AbstractRepository implements SkeletonRepository<MeetingTypeEntity> {

    @Override
    public int add(MeetingTypeEntity entry) throws Exception {
        return create(entry);
    }

    @Override
    public MeetingTypeEntity getById(int id) throws EntryNotFoundException {
        return getEntries(JPQLConstants.GET_BY_ID(MeetingTypeEntity.class, id))
                .stream()
                .findFirst()
                .orElseThrow(EntryNotFoundException::new);
    }

    @Override
    public List<MeetingTypeEntity> getAll() {
        return getEntries(JPQLConstants.SELECT_ALL(MeetingTypeEntity.class));
    }

    private List<MeetingTypeEntity> getEntries(String query) {
        return new ArrayList<>(((Collection<MeetingTypeEntity>) mspAppPU.createQuery(query).getResultList()));
    }
}
