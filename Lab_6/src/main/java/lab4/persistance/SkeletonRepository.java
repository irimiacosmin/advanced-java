package lab4.persistance;

import lab4.exceptions.DuplicateEntryException;
import lab4.exceptions.EntryNotFoundException;
import lab4.utils.SQLConstants;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public interface SkeletonRepository<T> extends Serializable {

    int add(T entry) throws Exception;

    T getById(int id) throws SQLException, EntryNotFoundException;

    List<T> getAll() throws SQLException, EntryNotFoundException;

    default int genericAdd(Connection connection, String query) throws SQLException, DuplicateEntryException {
        int tId = -1;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet.next()) {
                tId = resultSet.getInt("id");
            }
            resultSet.close();
        } catch (SQLException sqlException) {
            if (sqlException.getSQLState().equals(SQLConstants.DUPLICATE_KEY_CODE)) {
                throw new DuplicateEntryException();
            }
            throw sqlException;
        } finally {
            connection.close();
        }
        return tId;
    }
}
