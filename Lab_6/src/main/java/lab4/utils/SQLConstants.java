package lab4.utils;

import lab4.models.Distance;
import lab4.models.Location;
import lab4.models.Meeting;
import lab4.models.Person;

import java.text.MessageFormat;

public class SQLConstants {
    public static final String JNDI_DATA_SOURCE = "jdbc/jndiDataSource";

    public static final String DATABASE_DRIVER = "org.postgresql.Driver";
    public static final String CONNECTION_URL = "jdbc:postgresql://localhost:5432/postgres";
    public static final String USER = "postgres";
    public static final String PASSWORD = "1234";

    public static final String DUPLICATE_KEY_CODE = "23505";
    public static final String GET_ALL_MEETING_ATTENDEES_QUERY = "SELECT p.* FROM advanced_java.meeting_person mp " +
            "JOIN advanced_java.person p ON mp.person_id = p.id " +
            "WHERE meeting_id = {0};";
    private static final String SELECT_ALL = "SELECT * FROM advanced_java.{0}";
    private static final String INSERT_PERSON_QUERY = "INSERT INTO advanced_java.person (name, email) VALUES (''{0}'', ''{1}'') RETURNING id;";
    private static final String INSERT_LOCATION_QUERY = "INSERT INTO advanced_java.location (name) VALUES (''{0}'') RETURNING id;";
    private static final String INSERT_DISTANCE_QUERY = "INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES ({0}, {1}, {2}) RETURNING id;";
    private static final String INSERT_MEETING_QUERY = "INSERT INTO advanced_java.meeting (topic, \"timestamp\", duration_in_minutes, location_id) VALUES (''{0}'', ''{1}'', {2}, {3}) RETURNING id;";
    private static final String INSERT_MEETING_PERSON_QUERY = "INSERT INTO advanced_java.meeting_person (meeting_id, person_id) VALUES ({0}, {1}) RETURNING id;";

    public static String SELECT_ALL(Class modelName) {
        return getFilledQuery(SELECT_ALL, modelName.getSimpleName().toLowerCase());
    }

    public static String GET_ALL_MEETING_ATTENDEES(int meetingId) {
        return getFilledQuery(GET_ALL_MEETING_ATTENDEES_QUERY, meetingId);
    }

    public static String GET_BY_ID(Class modelName, Integer id) {
        String whereClause = " where id = " + id;
        return SELECT_ALL(modelName) + whereClause;
    }

    public static String INSERT(Person entity) {
        return getFilledQuery(INSERT_PERSON_QUERY, entity.getName(), entity.getEmail());
    }

    public static String INSERT(Location entity) {
        return getFilledQuery(INSERT_LOCATION_QUERY, entity.getName());
    }

    public static String INSERT(Distance entity) {
        return getFilledQuery(INSERT_DISTANCE_QUERY, entity.getFirstLocation().getId(), entity.getSecondLocation().getId(), entity.getDurationMinutes());
    }

    public static String INSERT(Meeting entity) {
        return getFilledQuery(INSERT_MEETING_QUERY, entity.getTopic().toString(), entity.getStartingDateTime().toString(), entity.getDurationMinutes(), entity.getLocation().getId());
    }

    public static String INSERT(Meeting meeting, Person person) {
        return getFilledQuery(INSERT_MEETING_PERSON_QUERY, meeting.getId(), person.getId());
    }

    public static String getFilledQuery(String query, Object... params) {
        return MessageFormat.format(query, params);
    }
}
