package lab4.utils;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener()
public class SessionCounter implements HttpSessionListener {
    private static int users = 0;

    public static int getConcurrentUsers() {
        return users;
    }

    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        users++;
    }

    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        users--;
    }
}
