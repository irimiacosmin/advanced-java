package lab4.utils;

import java.text.MessageFormat;

public class JPQLConstants {
    public static final String GET_ALL_MEETING_ATTENDEES_QUERY = "SELECT p FROM MeetingPersonEntity mp " +
            "JOIN PersonEntity p ON mp.person.id = p.id " +
            "WHERE mp.meeting.id = {0}";
    private static final String SELECT_ALL = "SELECT e FROM {0} e";

    public static String SELECT_ALL(Class modelName) {
        return getFilledQuery(SELECT_ALL, modelName.getSimpleName());
    }

    public static String GET_ALL_MEETING_ATTENDEES(int meetingId) {
        return getFilledQuery(GET_ALL_MEETING_ATTENDEES_QUERY, meetingId);
    }

    public static String GET_BY_ID(Class modelName, Integer id) {
        String whereClause = " WHERE e.id = " + id;
        return SELECT_ALL(modelName) + whereClause;
    }

    public static String getFilledQuery(String query, Object... params) {
        return MessageFormat.format(query, params);
    }
}
