package lab4.utils;

public class HTMLConstants {

    public static final String ERROR_H6_TEMPLATE = "<h6>{0}</h6>";

    public static final String TABLE_ROW_TEMPLATE = "<tr>" + "<td>{0}</td>" + "<td>{1}</td>" + "<td>{2}</td>" + "</tr>";

    public static final String LANGUAGE_OPTION_TEMPLATE = "<option value=\"{0}\">{1}</option>";
}
