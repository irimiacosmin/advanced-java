--- SCHEMA
DROP SCHEMA IF EXISTS advanced_java CASCADE;
CREATE SCHEMA advanced_java;

--- ENUMS
DROP TYPE IF EXISTS topic_enum;
CREATE TYPE topic_enum AS ENUM ('RELEASE_PLAN', 'MANAGEMENT_PLANNING', 'SPRINT_REVIEW');

CREATE CAST (character varying AS advanced_java.topic_enum) WITH INOUT AS ASSIGNMENT;

--- TABLES
DROP TABLE IF EXISTS advanced_java.person;
CREATE TABLE advanced_java.person (
  id                        serial NOT NULL,
  name                varchar(255) NOT NULL,
  email      	      varchar(255) NOT NULL,
  CONSTRAINT person_id_pk PRIMARY KEY (id),
  CONSTRAINT person_email_un UNIQUE (email)
);

DROP TABLE IF EXISTS advanced_java.location;
CREATE TABLE advanced_java.location (
  id                        serial NOT NULL,
  name                varchar(255) NOT NULL,
  CONSTRAINT location_id_pk PRIMARY KEY (id)
);

DROP TABLE IF EXISTS advanced_java.distance;
CREATE TABLE advanced_java.distance (
  id                        serial NOT NULL,
  first_location_id           int4 NOT NULL,
  second_location_id          int4 NOT NULL,
  duration_in_minutes         int4 NOT NULL,
  CONSTRAINT distance_id_pk PRIMARY KEY (id),
  CONSTRAINT distance_to_first_location_fk FOREIGN KEY (first_location_id) REFERENCES advanced_java.location (id),
  CONSTRAINT distance_to_second_location_fk FOREIGN KEY (first_location_id) REFERENCES advanced_java.location (id),
  CONSTRAINT distance_un UNIQUE (first_location_id, second_location_id)
);


DROP TABLE IF EXISTS advanced_java.meeting_type;
CREATE TABLE advanced_java.meeting_type (
  id                        serial NOT NULL,
  type                varchar(255) NOT NULL,
  additional_data                     jsonb,
  CONSTRAINT meeting_type_id_pk PRIMARY KEY (id)
);

DROP TABLE IF EXISTS advanced_java.meeting;
CREATE TABLE advanced_java.meeting (
  id                        serial NOT NULL,
  topic                 topic_enum NOT NULL,
  timestamp              TIMESTAMP NOT NULL,
  duration_in_minutes         int4 NOT NULL,
  location_id                 int4 NOT NULL,
  meeting_type_id             int4 NOT NULL,
  CONSTRAINT meeting_id_pk PRIMARY KEY (id),
  CONSTRAINT meeting_to_location_fk FOREIGN KEY (location_id) REFERENCES advanced_java.location (id),
  CONSTRAINT meeting_to_meeting_type_fk FOREIGN KEY (meeting_type_id) REFERENCES advanced_java.meeting_type (id)
);

DROP TABLE IF EXISTS advanced_java.meeting_person;
CREATE TABLE advanced_java.meeting_person (
  id                        serial NOT NULL,
  meeting_id                 int4 NOT NULL,
  person_id                  int4 NOT NULL,
  CONSTRAINT meeting_person_id_pk PRIMARY KEY (id),
  CONSTRAINT meeting_person_to_person_fk FOREIGN KEY (person_id) REFERENCES advanced_java.person (id),
  CONSTRAINT meeting_person_to_meeting_fk FOREIGN KEY (meeting_id) REFERENCES advanced_java.meeting (id),
  CONSTRAINT meeting_person_un UNIQUE (person_id, meeting_id)
);

INSERT INTO advanced_java.person (name, email) VALUES ('Irimia Cosmin', 'irimia.cosmin@gmail.com');
INSERT INTO advanced_java.person (name, email) VALUES ('Romanescu Stefan', 'stefan.romanescu@gmail.com');
INSERT INTO advanced_java.person (name, email) VALUES ('Matei Madalin', 'madalin.matei@gmail.com');
INSERT INTO advanced_java.person (name, email) VALUES ('Alex Moruz', 'mmoruz@gmail.com');

INSERT INTO advanced_java.location (name) VALUES ('Palatul Culturii');
INSERT INTO advanced_java.location (name) VALUES ('Piata Unirii');
INSERT INTO advanced_java.location (name) VALUES ('Endava');
INSERT INTO advanced_java.location (name) VALUES ('Romsoft');
INSERT INTO advanced_java.location (name) VALUES ('Pacurari');

INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (1, 1, 0);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (1, 2, 10);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (1, 3, 3);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (1, 4, 20);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (1, 5, 30);

INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (2, 1, 10);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (2, 2, 0);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (2, 3, 13);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (2, 4, 30);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (2, 5, 20);

INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (3, 1, 3);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (3, 2, 13);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (3, 3, 0);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (3, 4, 17);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (3, 5, 25);

INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (4, 1, 20);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (4, 2, 30);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (4, 3, 17);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (4, 4, 0);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (4, 5, 40);

INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (5, 1, 30);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (5, 2, 20);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (5, 3, 25);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (5, 4, 40);
INSERT INTO advanced_java.distance (first_location_id, second_location_id, duration_in_minutes) VALUES (5, 5, 0);


INSERT INTO advanced_java.meeting_type (type, additional_data) VALUES ('conference', '{"speakers": [{"id": 1, "name": "Florin Olariu", "expertise": ".Net"}, {"id": 2, "name": "Victor Rentea", "expertise" : "Java"}], "resources" : [ {"id": 1, "name": "Pointer"}, {"id": 2, "name": "Whiteboard"},{"id": 3, "name": "Marker"}]}'::jsonb);
INSERT INTO advanced_java.meeting_type (type, additional_data) VALUES ('workshop', '{"speakers": [{"id": 3, "name": "Uncle Bob", "expertise": "Java"}, {"id": 4, "name": "Frasinariu Cristian", "expertise" : "Java"}], "resources" : [{"id": 2, "name": "Whiteboard"},{"id": 3, "name": "Marker"}]}'::jsonb);


INSERT INTO advanced_java.meeting (topic, "timestamp", duration_in_minutes, location_id, meeting_type_id) VALUES ('RELEASE_PLAN', CURRENT_TIMESTAMP, 80, 1, 1);
INSERT INTO advanced_java.meeting (topic, "timestamp", duration_in_minutes, location_id, meeting_type_id) VALUES ('MANAGEMENT_PLANNING', CURRENT_TIMESTAMP, 65, 2, 2);
INSERT INTO advanced_java.meeting (topic, "timestamp", duration_in_minutes, location_id, meeting_type_id) VALUES ('SPRINT_REVIEW', CURRENT_TIMESTAMP, 25, 3, 1);
INSERT INTO advanced_java.meeting (topic, "timestamp", duration_in_minutes, location_id, meeting_type_id) VALUES ('RELEASE_PLAN', CURRENT_TIMESTAMP, 55, 4, 2);
INSERT INTO advanced_java.meeting (topic, "timestamp", duration_in_minutes, location_id, meeting_type_id) VALUES ('MANAGEMENT_PLANNING', CURRENT_TIMESTAMP, 125, 5, 1);

INSERT INTO advanced_java.meeting_person (meeting_id, person_id) VALUES (1, 1);
INSERT INTO advanced_java.meeting_person (meeting_id, person_id) VALUES (1, 2);
INSERT INTO advanced_java.meeting_person (meeting_id, person_id) VALUES (1, 3);
INSERT INTO advanced_java.meeting_person (meeting_id, person_id) VALUES (2, 2);
INSERT INTO advanced_java.meeting_person (meeting_id, person_id) VALUES (2, 3);
INSERT INTO advanced_java.meeting_person (meeting_id, person_id) VALUES (3, 1);
INSERT INTO advanced_java.meeting_person (meeting_id, person_id) VALUES (3, 2);
INSERT INTO advanced_java.meeting_person (meeting_id, person_id) VALUES (3, 3);
INSERT INTO advanced_java.meeting_person (meeting_id, person_id) VALUES (3, 4);
INSERT INTO advanced_java.meeting_person (meeting_id, person_id) VALUES (4, 3);
INSERT INTO advanced_java.meeting_person (meeting_id, person_id) VALUES (4, 4);
INSERT INTO advanced_java.meeting_person (meeting_id, person_id) VALUES (5, 3);
INSERT INTO advanced_java.meeting_person (meeting_id, person_id) VALUES (5, 1);
INSERT INTO advanced_java.meeting_person (meeting_id, person_id) VALUES (5, 4);