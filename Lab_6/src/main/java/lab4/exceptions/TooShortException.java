package lab4.exceptions;

import javax.faces.application.FacesMessage;
import javax.faces.validator.ValidatorException;

public class TooShortException extends ValidatorException {
    public String message;

    public TooShortException(FacesMessage message) {
        super(message);
    }
}
