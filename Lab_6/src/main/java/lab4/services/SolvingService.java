package lab4.services;

import lab4.exceptions.EntryNotFoundException;
import lab4.models.*;
import lab4.persistance.DistanceRepository;
import lab4.persistance.LocationRepository;
import lab4.persistance.MeetingRepository;
import lab4.persistance.PersonRepository;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Named(value = "SolvingService")
@SessionScoped
public class SolvingService implements Serializable {

    @Inject
    private PersonRepository personRepository;

    @Inject
    private MeetingRepository meetingRepository;

    @Inject
    private DistanceRepository distanceRepository;

    @Inject
    private LocationRepository locationRepository;

    @Inject
    private AlgorithmSolverService algorithmSolverService;


    public List<String> getSolution() {
        List<Person> persons = personRepository.getAll();
        List<Meeting> meetings = meetingRepository.getAll();
        List<Distance> distances = distanceRepository.getAll();
        List<Location> locations = locationRepository.getAll();
        List<String> response = new ArrayList<>();
        List<MeetingDistribution> solution = algorithmSolverService.solveProblem(persons, meetings, distances, locations);

        if (solution == null) {
            response.add("We didnt find a solution.");
        } else {
            solution = solution.stream()
                    .sorted(Comparator.comparing(MeetingDistribution::getStartDateTime))
                    .collect(Collectors.toList());
            solution.forEach(meetingDistribution -> {
                response.add(meetingDistribution.getMeeting().getGeneralDetails() + " with "
                        + meetingDistribution.getMeeting().getAttendeesDetails() + " "
                        + meetingDistribution.getGeneralDetails());
            });
        }
        return response;
    }

}
