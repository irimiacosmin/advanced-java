package lab4.services;

import lab4.entities.MeetingTypeEntity;
import lab4.models.Location;
import lab4.models.Meeting;
import lab4.models.Person;
import lab4.persistance.LocationRepository;
import lab4.persistance.MeetingRepository;
import lab4.persistance.MeetingTypeRepository;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Named(value = "MeetingService")
@SessionScoped
public class MeetingService implements Serializable {

    @Inject
    private DistanceService distanceService;

    @Inject
    private PersonService personService;

    @Inject
    private MeetingRepository meetingRepository;

    @Inject
    private LocationRepository locationRepository;

    @Inject
    private MeetingTypeRepository meetingTypeRepository;

    public List<Meeting> getAll() {
        return meetingRepository.getAll();
    }

    public void save(Meeting meeting) throws Exception {
        distanceService.saveDistances(meeting.getLocation());
        List<Person> attendees = personService.getAttendeesChecked()
                .entrySet()
                .stream()
                .filter(Map.Entry::getValue)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        meeting.setAttendees(attendees);
        meetingRepository.add(meeting);
        meeting.reset();
    }

    public List<Location> getPossibleLocations() {
        List<Location> locations = new ArrayList<>();
        try {
            locations = locationRepository.getAll();
        } catch (Exception e) {
            System.err.println(Arrays.toString(e.getStackTrace()));
        }
        return locations;
    }

    public List<MeetingTypeEntity> getMeetingTypes() {
        return meetingTypeRepository.getAll();
    }

    public Location getLocationByToString(String toString) {
        return getPossibleLocations().stream()
                .filter(location -> location.toString().equals(toString))
                .findFirst()
                .orElse(null);
    }

}
