package lab4.services;

import lab4.models.Person;
import lab4.persistance.PersonRepository;
import lombok.Getter;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Named(value = "PersonService")
@SessionScoped
@Getter
public class PersonService implements Serializable {

    private final Map<Person, Boolean> attendeesChecked = new HashMap<>();
    @Inject
    private PersonRepository personRepository;

    public List<Person> getAll() {
        return personRepository.getAll();
    }

    public void save(Person person) throws Exception {
        personRepository.add(person);
        person.reset();
        attendeesChecked.clear();
    }
}
