package lab4.converters;

import lab4.exceptions.DuplicateEntryException;
import lab4.exceptions.TooShortException;
import lab4.models.Location;
import lab4.persistance.LocationRepository;
import lombok.SneakyThrows;

import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.sql.SQLException;


@FacesConverter("lab4.converters.LocationSearchConverter")
public class LocationSearchConverter implements Converter<Location> {

    private final LocationRepository locationRepository;

    public LocationSearchConverter() {
        this.locationRepository = CDI.current().select(LocationRepository.class).get();

    }
    @SneakyThrows
    @Override
    public Location getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return locationRepository.getAll()
                .stream()
                .filter(location -> location.toString().equals(s))
                .findFirst()
                .orElse(null);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Location location) {
        return location.toString();
    }
}
