package lab4.converters;

import lab4.entities.MeetingTypeEntity;
import lab4.exceptions.DuplicateEntryException;
import lab4.exceptions.TooShortException;
import lab4.models.Location;
import lab4.persistance.LocationRepository;
import lab4.persistance.MeetingTypeRepository;
import lombok.SneakyThrows;

import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.sql.SQLException;


@FacesConverter("lab4.converters.TypeConverter")
public class TypeConverter implements Converter<MeetingTypeEntity> {

    private final MeetingTypeRepository meetingTypeRepository;

    public TypeConverter() {
        this.meetingTypeRepository = CDI.current().select(MeetingTypeRepository.class).get();
    }

    @Override
    public MeetingTypeEntity getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return meetingTypeRepository.getAll().stream()
                .filter(meetingType -> meetingType.toString().equals(s))
                .findFirst()
                .orElse(null);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, MeetingTypeEntity meetingTypeEntity) {
        return meetingTypeEntity.toString();
    }
}
