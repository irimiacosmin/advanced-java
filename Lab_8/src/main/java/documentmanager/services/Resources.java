package documentmanager.services;

import documentmanager.utils.GlassfishConstants;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

@ApplicationScoped
public class Resources implements Serializable {

    @Inject
    AuthService authService;

    @PersistenceContext(name = GlassfishConstants.PERSISTENCE_UNIT_NAME)
    private EntityManager docsPu;

    @Produces
    public EntityManager getEntityManager() {
        return docsPu;
    }
}
