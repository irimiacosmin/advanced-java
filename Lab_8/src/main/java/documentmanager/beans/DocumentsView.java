package documentmanager.beans;

import documentmanager.models.Document;
import documentmanager.repositories.DocumentsRepository;
import documentmanager.services.alternatives.DocumentService;
import documentmanager.services.alternatives.FileService;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

@Named
@RequestScoped
public class DocumentsView implements Serializable {

    @EJB
    private DocumentsRepository documentsRepository;

    @Inject
    private FileService documentService;

    private List<Document> documents;

    private StreamedContent file;

    @PostConstruct
    public void init() {
        documents = documentsRepository.getAllDocuments();
    }

    public StreamedContent getFile(Integer registrationNumber, String name) {
        InputStream stream = new ByteArrayInputStream(documentService.getDocumentByRegistrationNumber(registrationNumber));
        file = new DefaultStreamedContent(stream, FacesContext.getCurrentInstance().getExternalContext().getMimeType(name), name);
        return file;
    }

    public void onDocumentsUpload(@Observes Document document) {
        documents.add(document);
        RequestContext.getCurrentInstance().update(":output-panel:docsTable");
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }
}
