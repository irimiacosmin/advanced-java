package documentmanager.webservices;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ViewDocumentServiceInterface {

    @WebMethod
    String businessMethod();
}
