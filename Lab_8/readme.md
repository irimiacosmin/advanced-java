# Java Technologies - Lab 8
#
#
#
#### Point 1 (2p)

- Create a simple JSF application for managing documents (or integrate the following functionalities into an existing application). The application will allow the following:
    * An authentication mechanism based on username and password.
    * Register new users and assign them a specific role, for example admin, guest, etc.
    * Specify a time frame, in which registration is open for users and submissions.
    * The possibility to upload a document (for guests) and to view all uploaded documents (for admin). Each uploaded document will have a uniquely generated registration number. All submissions will be logged in a text file.

    
#### Point 2 (3p)

- Use Contexts and Dependency Injection (CDI) for:
    * (0.5) the management of application's beans (@Inject);
    * (0.5) decoupling the components using dependency injection (for example, use a producer method to generate registration numbers) (@Produces);
    * (0.5) decoupling orthogonal concerns, such as logging; (@Interceptor)
    * (0.5) decoupling bussines concerns, such as verifying the date for operations like registration and submission (@Decorator);
    * (0.5) implementing at least one event-based comunication (for instance, whenever a new document is uploaded a message is produced and all observers of this type of event will be notified) (@Observes);
    * (0.5) data validation, using Bean Validation annotations.
- The original use of other CDI concepts such as qualifiers, alternatives, specializations, etc. will be appreciated.

#
#
#
#### Bibliography
- [Lab slides](https://profs.info.uaic.ro/~acf/tj/labs/pdf/lab8.pdf)
- [Java EE Tutorial: CDI](https://docs.oracle.com/javaee/7/tutorial/partcdi.htm#GJBNR)
- [Weld - Reference Implementation](https://docs.jboss.org/weld/reference/latest/en-US/html/)
- [Arquillian: An integration testing framework for Java EE](https://docs.jboss.org/arquillian/reference/1.0.0.Alpha1/en-US/html_single/)
- [Java EE Tutorial: Bean Validation](https://docs.oracle.com/javaee/7/tutorial/partbeanvalidation.htm#sthref1322)