package documentmanager.services;

import documentmanager.utils.GlassfishConstants;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

@ApplicationScoped
public class Resources implements Serializable {

    @Inject
    AuthService authService;

    private EntityManager docsPu;

    @PostConstruct
    private void init() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(GlassfishConstants.PERSISTENCE_UNIT_NAME);
        docsPu = emf.createEntityManager();
    }

    @Produces
    public EntityManager getEntityManager() {
        return docsPu;
    }
}
