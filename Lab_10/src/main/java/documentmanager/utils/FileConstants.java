package documentmanager.utils;

import javax.servlet.http.Part;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileConstants {

    public static final String LOG_PATH = ".\\logfile.txt";
    public static final String FILE_PATH = ".\\Lab_8_1";

    public static byte[] getByteArrayFromPartItem(Part part) {
        InputStream is = null;
        try {
            is = part.getInputStream();
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[16384];

            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            return buffer.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
