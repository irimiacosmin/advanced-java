package documentmanager.entities;

import documentmanager.models.Period;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "periods", schema = "docs_manager")
public class PeriodsEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column(name = "start_date", nullable = false)
    private Date startDate;

    @Basic
    @Column(name = "end_date", nullable = false)
    private Date endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "added_by")
    private AdminEntity addedBy;

    public Period toPeriod() {
        Period result = new Period();
        result.setStartDate(this.getStartDate());
        result.setEndDate(this.getEndDate());
        result.setUser(this.getAddedBy().toUser());
        return result;
    }
}
