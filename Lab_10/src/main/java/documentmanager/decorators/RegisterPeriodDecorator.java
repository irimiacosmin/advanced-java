package documentmanager.decorators;

import documentmanager.models.Period;
import documentmanager.repositories.PeriodRepository;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.ejb.EJB;
import javax.enterprise.inject.Any;
import javax.inject.Inject;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Decorator
public class RegisterPeriodDecorator implements RegisterPeriod {

    @Inject
    @Delegate
    @Any
    RegisterPeriod registerPeriod;

    @EJB
    private PeriodRepository periodRepository;

    @Override
    public String register() throws Exception {
        registerPeriod.register();
        List<Period> allPeriods = periodRepository.getAllPeriods();

        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);

        Date now = today.getTime();

        boolean validPeriod = allPeriods.stream().anyMatch(period -> period.getStartDate().compareTo(now) * now.compareTo(period.getEndDate()) > 0);

        if (!validPeriod) {
            throw new Exception("Invalid period to register.");
        }

        return "pages/index?faces-redirect=true";
    }
}
