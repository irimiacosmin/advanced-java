package documentmanager.decorators;

import java.security.NoSuchAlgorithmException;

public interface RegisterPeriod {
    String register() throws NoSuchAlgorithmException, Exception;
}
