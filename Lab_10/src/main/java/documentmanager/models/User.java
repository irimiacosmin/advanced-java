package documentmanager.models;

import documentmanager.entities.AdminEntity;
import documentmanager.entities.GuestEntity;
import documentmanager.entities.UsersEntity;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Arrays;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class User implements Serializable {
    private Integer id;

    @NotNull
    @Size(min=4, max=50)
    private String username;

    @NotNull
    @Size(min=4, max=50)
    private String password;

    private UserType type;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UsersEntity toEntity() {
        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setUsername(this.getUsername());
        usersEntity.setPassword(this.getPassword());
        return usersEntity;
    }

    public GuestEntity toGuestEntity() {
        GuestEntity usersEntity = new GuestEntity();
        usersEntity.setUsername(this.getUsername());
        usersEntity.setPassword(this.getPassword());
        return usersEntity;
    }

    public AdminEntity toAdminEntity() {
        AdminEntity adminEntity = new AdminEntity();
        adminEntity.setUsername(this.getUsername());
        adminEntity.setPassword(this.getPassword());
        return adminEntity;
    }

    @AllArgsConstructor
    @Getter
    public enum UserType {
        guest("guest"),
        admin("admin");

        private final String discriminator;

        public static UserType getByDiscriminator(String discriminator) {
            return Arrays.stream(UserType.values())
                    .filter(val -> val.getDiscriminator().equals(discriminator))
                    .findFirst()
                    .orElse(null);
        }
    }
}
