package documentmanager.webservices.JAXRS.tests;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ResultResponse implements Serializable {
    public Integer id;
    public Integer paramValue;
    public String response;
}
