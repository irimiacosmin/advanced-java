package documentmanager.webservices.JAXRS.tests;

import lombok.AllArgsConstructor;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@javax.ws.rs.Path("/test")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class TestController {

    public static int totalIterations;

    public static Map<String, Map<Integer, Map<Integer, String>>> pathsMap = new HashMap<>();

    public static void addLog(String key, Integer no, Integer value, String result) {
        totalIterations--;
        pathsMap.get(key).get(no).put(value, result);
    }

    @PostConstruct
    public void init() {
        totalIterations = Path.values().length * 20;
        Map<Integer, String> valuesMap = new HashMap<>();
        valuesMap.put(-1, null);
        valuesMap.put(0, null);

        Map<Integer, Map<Integer, String>> auxMap = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            auxMap.put(i, valuesMap);
        }

        Arrays.stream(Path.values()).forEach(path -> pathsMap.put(path.getDiscriminator(), auxMap));
    }

    @POST
    @javax.ws.rs.Path("/start")
    public void testMethods() {
        init();
        Arrays.stream(Path.values()).forEach(this::testTenTimesWithTenThreads);
    }

    @GET
    @javax.ws.rs.Path("/results")
    public Map<String, List<ResultResponse>> getResult() {
        Map<String, List<ResultResponse>> returnMap = new HashMap<>();
        pathsMap.forEach((key, value) -> returnMap.put(key.replace("/", ""), mapResponses(value)));
        return returnMap;
    }

    @GET
    @javax.ws.rs.Path("/is-ready")
    public Boolean isReady() {
        return totalIterations == 0;
    }

    public List<ResultResponse> mapResponses(Map<Integer, Map<Integer, String>> responseMap) {
        List<ResultResponse> resultResponses = new ArrayList<>();
        responseMap.forEach((iteration, paramAndResult) -> {
            paramAndResult.forEach((param, result) -> {
                resultResponses.add(new ResultResponse(iteration, param, result));
            });
        });
        return resultResponses;
    }

    public void testTenTimesWithTenThreads(Path path) {
        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        try {
            for (int i = 0; i < 10; i++) {
                executor.execute(new TestRunnable(i, path));
            }
        } catch (Exception err) {
            err.printStackTrace();
        }
        executor.shutdown();
    }
}

@AllArgsConstructor
class TestRunnable implements Runnable {
    int id;
    Path path;

    public void run() {
        try {
            System.out.println("Run: " + Thread.currentThread().getName() + " " + path.getDiscriminator());

            TestController.totalIterations--;
            String happyFlow = HttpService.get(path.getFullPath() + "0");
            TestController.pathsMap.get(path.getDiscriminator()).get(id).put(0, happyFlow);

            TestController.totalIterations--;
            String badFlow = HttpService.get(path.getFullPath() + "-1");
            TestController.pathsMap.get(path.getDiscriminator()).get(id).put(-1, badFlow);

            System.out.println("Runnable ended (" + path + " #" + id + ") \nHappy: (" + happyFlow + ") \nBad: ("+badFlow+")\n\n");
        } catch (Exception err) {
            err.printStackTrace();
        }
    }
}