package documentmanager.webservices.JAXRS;

import documentmanager.models.Document;
import documentmanager.repositories.DocumentsRepository;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/documents")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class AddDocumentService {

    @EJB
    private DocumentsRepository documentsRepository;

    @POST
    public void addDocument(Document document) {
        documentsRepository.addDocument(document);
    }
}