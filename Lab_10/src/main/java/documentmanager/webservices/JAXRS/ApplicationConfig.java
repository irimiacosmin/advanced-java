package documentmanager.webservices.JAXRS;

import documentmanager.webservices.JAXRS.tests.TestController;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;

@ApplicationPath("webresources")
@ApplicationScoped
public class ApplicationConfig extends Application {

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(AddDocumentService.class);
        resources.add(DeleteDocumentService.class);
        resources.add(GetDocumentService.class);
        resources.add(UpdateDocumentService.class);
        resources.add(HealthController.class);
        resources.add(TestController.class);
    }

    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }
}
