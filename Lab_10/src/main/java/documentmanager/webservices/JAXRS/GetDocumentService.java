package documentmanager.webservices.JAXRS;

import documentmanager.models.Document;
import documentmanager.repositories.DocumentsRepository;
import org.eclipse.microprofile.faulttolerance.*;
import org.eclipse.microprofile.metrics.Counter;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Gauge;
import org.eclipse.microprofile.metrics.annotation.Metric;
import org.eclipse.microprofile.metrics.annotation.Timed;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("/documents")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class GetDocumentService {

    @Inject
    @Metric(name = "counter_gauge")
    private Counter counter;

    @Gauge(name = "retry_counter_gauge", unit = MetricUnits.NONE)
    private long getRetryCounterGauge() {
        return counter.getCount();
    }

    @EJB
    private DocumentsRepository documentsRepository;

    @GET
    public List<Document> getAllDocuments() {
        return documentsRepository.getAllDocuments();
    }

    /*
        Fallback
    */
    @Timeout(500)
    @Fallback(fallbackMethod = "fallback")
    @Timed(name = "fallback-timed")
    @GET
    @Path("fallback/{id}")
    public List<Document> getAllDocumentsFallback(@PathParam("id") Integer id) throws SomeException {
        if (id == -1) {
            throw new SomeException();
        }
        return documentsRepository.getAllDocuments();
    }

    private List<Document> fallback(Integer id) throws SomeException {
        List<Document> documents = new ArrayList<>();
        documents.add(Document.builder().name("Fallback document").build());
        return documents;
    }

    /*
        Retry
    */
    @Retry(maxRetries = 2)
    @Fallback(fallbackMethod = "retry")
    @Timed(name = "retry-timed")
    @GET
    @Path("retry/{id}")
    public List<Document> getAllDocumentsRetry(@PathParam("id") Integer id) throws SomeException {
        if (id == -1) {
            throw new SomeException();
        }
        return documentsRepository.getAllDocuments();
    }

    private List<Document> retry(Integer id) {
        List<Document> documents = new ArrayList<>();
        documents.add(Document.builder().name("Retry document").build());
        return documents;
    }

    /*
        CircuitBreaker
    */
    @CircuitBreaker(successThreshold = 10, requestVolumeThreshold = 4, failureRatio = 0.75, delay = 1000)
    @Timed(name = "circuit-breaker-timed")
    @GET
    @Path("circuit-breaker/{id}")
    public List<Document> getAllDocumentsCircuitBreaker(@PathParam("id") Integer id) throws SomeException {
        if (id == -1) {
            throw new SomeException();
        }
        return documentsRepository.getAllDocuments();
    }

    private List<Document> circuitBreaker(Integer id) {
        List<Document> documents = new ArrayList<>();
        documents.add(Document.builder().name("CircuitBreaker document").build());
        return documents;
    }

    /*
        Bulkhead thread pool
    */
    @Bulkhead(5)
    @Timed(name = "thread-pool-timed")
    @GET
    @Path("thread-pool/{id}")
    public List<Document> getAllDocumentsBulkheadThreadPool(@PathParam("id") Integer id) throws SomeException {
        if (id == -1) {
            throw new SomeException();
        }
        return documentsRepository.getAllDocuments();
    }

    /*
        Bulkhead semaphore
    */
    @Bulkhead(value = 5, waitingTaskQueue = 8)
    @Timed(name = "semaphore-timed")
    @GET
    @Path("semaphore/{id}")
    public List<Document> getAllDocumentsBulkheadSemaphore(@PathParam("id") Integer id) throws SomeException {
        if (id == -1) {
            throw new SomeException();
        }
        return documentsRepository.getAllDocuments();
    }

    @GET
    @Path("{id}")
    public Document getDocumentById(@PathParam("id") Integer id) {
        return documentsRepository.getDocumentById(id);
    }

}
