package documentmanager.webservices.JAXWS.ws.ejbs;

import documentmanager.models.Document;
import documentmanager.repositories.DocumentsRepository;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.*;

@Stateless
public class CacheBean {

    @EJB
    private DocumentsRepository documentsRepository;

    private final Map<Integer, List<Document>> usersMappedByDocuments;

    public CacheBean() {
        usersMappedByDocuments = new HashMap<>();
    }

    public ArrayList<Document> getDocuments() {
        return new ArrayList<>(documentsRepository.getAllDocuments());
    }

    public void addUserToCache(Integer userId) {
        List<Document> documents = usersMappedByDocuments.get(userId);
        if (documents == null || documents.size() == 0) {
            usersMappedByDocuments.put(userId, documentsRepository.getDocumentsByUserId(userId));
        }
    }

    public ArrayList<Document> getDocumentsByUserId(Integer userId) {
        return new ArrayList<>(usersMappedByDocuments.get(userId));
    }

    public void reset() {
        Set<Integer> userIds = usersMappedByDocuments.keySet();
        userIds.forEach(userId -> usersMappedByDocuments.put(userId, new ArrayList<>()));
    }
}
