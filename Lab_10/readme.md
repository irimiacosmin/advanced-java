# Java Technologies - Lab 10
#
#
#
Continue the application created in the previous lab, exposing a CRUD REST resource as a microservice. Consider creating an additional microservice that will invoke the first one.
#
#### Point 1 (1p)

- Run the microservices using an Eclipse Microprofile server implementation (Payara Micro, Open Liberty, Quarkus, etc.)
- Create Docker containers for the microservices. Consider deploying the database also as a container.

    
#### Point 2 (2p)

- Implement simple test cases to highlight the support offered by MicroProfile for writing **resilient** microservices.
- Use the following: Fallback + Timeout and Retry(0.5p), CircuitBreaker(0.5p), Bulkhead thread-pool(0.5p) and semaphore(0.5p).
- You should be able to invoke the annotated methods and analyze their behaviour.

    
#### Point 3 (1p)

- Implement and test a **health check** procedure, in order to determine the readiness and the liveness of your service.

#### Point 4 (1p)

- Use MicroProfile Metrics API in order to **monitor** the behaviour of your service.
- Analyze the number of invocations and the response time for at least one method.

Note: Using Kubernetes in order to highlight the means of achieving auto-scalability would be nice.

#
#
#
#### Bibliography
- [Lab slides](https://profs.info.uaic.ro/~acf/tj/slides/micro_slide_en.pdf)
- [MicroProfile API Documentation](https://javadoc.io/doc/org.eclipse.microprofile/microprofile/latest/index.html)
- [MicroProfile: Optimizing Enterprise Java for a Microservices Architecture](https://microprofile.io/)
- [MicroProfile Starter](https://start.microprofile.io/)
- [Microservices for Java EE Developers](https://blog.payara.fish/microservices-for-java-ee-developers2)
- [Containerization](https://www.ibm.com/cloud/learn/containerization/)
- [Kubernetes Basics](https://kubernetes.io/docs/tutorials/kubernetes-basics/)
#
#
#

## Running the project

In order to run the app, we must generate the JAR file:
In classpath run `mvn install` or run the **Install** lifecycle option from the right **Maven** Menu

### Local

Using InteliJ is simple AF:

1. Create a new configurations of type **JAR Application**.
2. Select the **JAR Path** to `payara-micro-5.2020.7.jar` included in the classpath
3. Insert `--deploy target/Lab_8_1-1.0-SNAPSHOT.war --autoBindHttp --name DocumentService --contextRoot /rest` in  **Program Arguments**


### Docker

- In classpath run the following commands:
    *  Build
        * `docker build -f DockerfileDocumentService -t document-service .`
        * `docker build -f DockerfileTestService -t test-service .`
    * Run
	    * `docker run --name documents-container -d -p 8080:8080 -p 5432:5432 document-service`
	    * `docker run --link documents-container --name test-container -d -p 8081:8081 test-service`

### Kubernetes

- This deployment method uses [Minikube](https://minikube.sigs.k8s.io/docs/start/). After installation, follow the steps:
    * How to Start
        * `minikube start`
        * `eval $(minikube docker-env)`
        * `minikube ip` and use the first 3 ip groups to define a **start** and **end** IP 
(example: my minikube IP address is 192.168.49.5 and I used 192.168.49.1 as **start-IP** and 192.168.49.20 as **end-IP**)
        * `minikube addons enable metallb`
        * `minikube addons configure metallb` and use the defined **start-IP** and **end-IP**
        * `docker build -f DockerfileDocumentService -t document-service .`
        * `kubectl run document-foo --image=document-service --image-pull-policy=Never`
        * `kubectl expose deployment document-foo --type=LoadBalancer --port=8080`
        * `minikube dashboard` to check the health of your pods and services
    * How to Stop
        * `kubectl delete service document-foo`  
        * `kubectl delete deployment document-foo` 