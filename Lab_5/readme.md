# Java Technologies - Lab 5
#
#
#
#### Point 1 (2p)

- Continue the application created in the previous lab:
    * Each meeting has a location. There must be a table containing the locations (id and name) and a corresponding class.
    * Create the pages using **templates**:
        * page.xhtml: describing the general aspect of the application pages: header, content, footer. The header should display the title and might include a menu bar.
        * The footer will display a copyright notice and the current version of the aplication. The header, footer and the menu bar should all be in separate .xhtml files.
        * *dataView.xhtml*: a generic page for displaying data as a list, dataTable, etc.
        * *dataEdit.xhtml*: a generic page for editing data. This could be a dialog containing a generic form.
    * Create at least one **converter** (for the location) and at least one **validator** (for the meeting).
    
#### Point 2 (1p)

- Use additional JSF features:
    * Create at least one **composite** component. For example, create a custom component for displaying information about a meeting.
    * Use the components **ajax** and **poll** in order to continuously display how many active sessions are in progress, or information about the execution of your algorithm or something similar.
    * Internationalize the application, offering support for at least two locales.

#### Point 3 (1p)
- Implement an efficient way for obtaining connections to the database.
    * Configure a **connection pool** and a **JDBC resource** using an administrative tool (such as GlassFish/Payara Console or asadmin).
    * Create **DataSource** objects using either JNDI directly or resource injection.


#
#
#
#### Bibliography
- [Lab slides](https://profs.info.uaic.ro/~acf/tj/labs/pdf/lab5.pdf)
- [The Web Tier](https://docs.oracle.com/javaee/7/tutorial/partwebtier.htm#BNADP)
- [JavaServer Faces Technology](https://docs.oracle.com/javaee/7/tutorial/jsf-intro.htm)
- [Introduction to Facelets Library](https://docs.oracle.com/javaee/7/tutorial/jsf-facelets.htm#GIEPX)
- [JavaServer Faces 2.2 Facelets Tag Library Documentation](https://docs.oracle.com/javaee/7/javaserver-faces-2-2/vdldocs-facelets/toc.htm)
- [Java SE Tutorial: Java Naming and Directory Interface](https://docs.oracle.com/javase/tutorial/jndi/index.html)
- [Java EE Tutorial: Resources](https://docs.oracle.com/javaee/7/tutorial/resource-creation.htm)