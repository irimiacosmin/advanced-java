package lab4.utils;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Named(value = "DateTimeUtil")
@RequestScoped
public class DateTimeUtil {

    public static final String REGEX_MATCHES_INTL_DATE_TIME_FORMAT = "^(20[2-9][0-9])-((0[1-9])|(1[0-2]))-((0[1-9])|(1[0-9])|(2[1-9])|(3[01]))\\s((0[1-9]|1[0-9]|2[0-3])):([0-5][0-9])$";

    private static final String US_DATE_TIME_FORMAT = "MM/dd/yyyy HH:mm";
    private static final String INTL_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";


    private static String formatAs(LocalDateTime localDateTime, String pattern) {
        return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
    }

    private static LocalDateTime parseAs(String dateAsString, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDateTime.parse(dateAsString, formatter);
    }

    public static String toUSDateTime(LocalDateTime localDateTime) {
        return formatAs(localDateTime, US_DATE_TIME_FORMAT);
    }

    public static String toINTLDateTime(LocalDateTime localDateTime) {
        return formatAs(localDateTime, INTL_DATE_TIME_FORMAT);
    }

    public static LocalDateTime fromUSDateTime(String dateAsString) {
        return parseAs(dateAsString, US_DATE_TIME_FORMAT);
    }

    public static LocalDateTime fromINTLDateTime(String dateAsString) {
        return parseAs(dateAsString, INTL_DATE_TIME_FORMAT);
    }
}