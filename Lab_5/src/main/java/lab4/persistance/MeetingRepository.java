package lab4.persistance;

import lab4.exceptions.DuplicateEntryException;
import lab4.exceptions.EntryNotFoundException;
import lab4.models.Location;
import lab4.models.Meeting;
import lab4.models.Person;
import lab4.sql.DatabaseService;
import lab4.utils.SQLConstants;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static lab4.utils.SQLConstants.JNDI_DATA_SOURCE;

@Named
@RequestScoped
public class MeetingRepository implements SkeletonRepository<Meeting> {

    @Resource(lookup = JNDI_DATA_SOURCE)
    private DataSource dataSource;

    @Inject
    private LocationRepository locationRepository;

    @Inject
    private PersonRepository personRepository;

    @Override
    public int add(Meeting entry) throws SQLException, DuplicateEntryException {
        int savedMeetingId = genericAdd(dataSource.getConnection(), SQLConstants.INSERT(entry));
        entry.setId(savedMeetingId);
        assignPersonToMeeting(entry);
        return savedMeetingId;
    }

    @Override
    public Meeting getById(int id) throws SQLException, EntryNotFoundException {
        return getEntries(SQLConstants.GET_BY_ID(Meeting.class, id))
                .stream()
                .findFirst()
                .orElseThrow(EntryNotFoundException::new);
    }

    @Override
    public List<Meeting> getAll() throws SQLException, EntryNotFoundException {
        return getEntries(SQLConstants.SELECT_ALL(Meeting.class));
    }

    private List<Meeting> getEntries(String query) throws SQLException, EntryNotFoundException {
        List<Meeting> entries = new ArrayList<>();
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            int meetingId = resultSet.getInt("id");
            String topicString = resultSet.getString("topic");
            LocalDateTime timestamp = resultSet.getObject("timestamp", LocalDateTime.class);
            Integer durationInMinutes = resultSet.getInt("duration_in_minutes");
            int locationId = resultSet.getInt("location_id");
            Location location = locationRepository.getById(locationId);
            List<Person> personList = personRepository.getAllByMeetingId(meetingId);

            entries.add(new Meeting(meetingId, Meeting.Topic.valueOf(topicString), timestamp, durationInMinutes, location, personList));
        }
        resultSet.close();
        connection.close();
        return entries;
    }

    private void assignPersonToMeeting(Meeting meeting) throws SQLException, DuplicateEntryException {
        for (Person person : meeting.getAttendees()) {
            genericAdd(dataSource.getConnection(), SQLConstants.INSERT(meeting, person));
        }
    }

    private List<Person> getAttendees(Integer meetingId) throws SQLException {
        String attendees = SQLConstants.getFilledQuery(SQLConstants.GET_ALL_MEETING_ATTENDEES_QUERY, meetingId);
        return personRepository.getEntries(attendees);
    }
}
