package lab4.persistance;

import lab4.exceptions.DuplicateEntryException;
import lab4.exceptions.EntryNotFoundException;
import lab4.models.Location;
import lab4.sql.DatabaseService;
import lab4.utils.SQLConstants;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static lab4.utils.SQLConstants.JNDI_DATA_SOURCE;

@Named
@RequestScoped
public class LocationRepository implements SkeletonRepository<Location>, Serializable {

    @Resource(lookup = JNDI_DATA_SOURCE)
    private DataSource dataSource;

    @Override
    public int add(Location entry) throws SQLException, DuplicateEntryException {
        return genericAdd(dataSource.getConnection(), SQLConstants.INSERT(entry));
    }

    @Override
    public Location getById(int id) throws SQLException, EntryNotFoundException {
        return getEntries(SQLConstants.GET_BY_ID(Location.class, id))
                .stream()
                .findFirst()
                .orElseThrow(EntryNotFoundException::new);
    }

    @Override
    public List<Location> getAll() throws SQLException {
        return getEntries(SQLConstants.SELECT_ALL(Location.class));
    }

    private List<Location> getEntries(String query) throws SQLException {
        List<Location> entries = new ArrayList<>();
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            entries.add(new Location(id, name));
        }
        resultSet.close();
        connection.close();
        return entries;
    }
}
