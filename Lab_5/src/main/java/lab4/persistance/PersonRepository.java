package lab4.persistance;

import lab4.exceptions.DuplicateEntryException;
import lab4.models.Person;
import lab4.sql.DatabaseService;
import lab4.utils.SQLConstants;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static lab4.utils.SQLConstants.JNDI_DATA_SOURCE;

@Named
@RequestScoped
public class PersonRepository implements SkeletonRepository<Person> {

    @Resource(lookup = JNDI_DATA_SOURCE)
    private DataSource dataSource;

    @Override
    public int add(Person entry) throws SQLException, DuplicateEntryException {
        return genericAdd(dataSource.getConnection(), SQLConstants.INSERT(entry));
    }

    @Override
    public Person getById(int id) throws SQLException {
        return getEntries(SQLConstants.GET_BY_ID(Person.class, id))
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public List<Person> getAll() throws SQLException {
        return getEntries(SQLConstants.SELECT_ALL(Person.class));
    }

    public List<Person> getAllByMeetingId(int meetingId) throws SQLException {
        return getEntries(SQLConstants.GET_ALL_MEETING_ATTENDEES(meetingId));
    }

    public List<Person> getEntries(String query) throws SQLException {
        List<Person> entries = new ArrayList<>();
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String email = resultSet.getString("email");
            entries.add(new Person(id, name, email));
        }
        resultSet.close();
        connection.close();
        return entries;
    }
}
