package lab4.persistance;

import lab4.exceptions.DuplicateEntryException;
import lab4.exceptions.EntryNotFoundException;
import lab4.models.Distance;
import lab4.models.Location;
import lab4.sql.DatabaseService;
import lab4.utils.SQLConstants;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static lab4.utils.SQLConstants.JNDI_DATA_SOURCE;

@Named
@RequestScoped
public class DistanceRepository implements SkeletonRepository<Distance> {

    @Resource(lookup = JNDI_DATA_SOURCE)
    private DataSource dataSource;

    @Inject
    private LocationRepository locationRepository;

    @Override
    public int add(Distance entry) throws SQLException, DuplicateEntryException {
        return genericAdd(dataSource.getConnection(), SQLConstants.INSERT(entry));
    }

    @Override
    public Distance getById(int id) throws SQLException, EntryNotFoundException {
        return getEntries(SQLConstants.GET_BY_ID(Distance.class, id))
                .stream()
                .findFirst()
                .orElseThrow(EntryNotFoundException::new);
    }

    @Override
    public List<Distance> getAll() throws SQLException, EntryNotFoundException {
        return getEntries(SQLConstants.SELECT_ALL(Distance.class));
    }

    private List<Distance> getEntries(String query) throws SQLException, EntryNotFoundException {
        List<Distance> entries = new ArrayList<>();
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            Integer id = resultSet.getInt("id");
            int first_location_id = resultSet.getInt("first_location_id");
            int second_location_id = resultSet.getInt("second_location_id");
            Integer duration_in_minutes = resultSet.getInt("duration_in_minutes");

            Location firstLocation = locationRepository.getById(first_location_id);
            Location secondLocation = locationRepository.getById(second_location_id);
            entries.add(new Distance(id, firstLocation, secondLocation, duration_in_minutes));
        }
        resultSet.close();
        connection.close();
        return entries;
    }
}
