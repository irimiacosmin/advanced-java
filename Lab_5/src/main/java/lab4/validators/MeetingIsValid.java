package lab4.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MeetingValidator.class)
public @interface MeetingIsValid {

    String message() default "{person.name.too-short}";

    String fieldName() default "meeting";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
