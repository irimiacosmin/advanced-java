package lab4.models;

import lab4.validators.MeetingIsValid;
import lombok.*;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Named(value = "meetingBean")
@RequestScoped
@EqualsAndHashCode
@MeetingIsValid
public class Meeting {

    private Integer id;
    private Topic topic;
    private LocalDateTime startingDateTime;
    private Integer durationMinutes;
    private Location location;
    private List<Person> attendees;

    public Meeting(Integer id, Topic topic, LocalDateTime startingDateTime, Integer durationMinutes, Location location) {
        this.id = id;
        this.topic = topic;
        this.startingDateTime = startingDateTime;
        this.durationMinutes = durationMinutes;
        this.location = location;
    }

    public void addPerson(Person person) {
        if (attendees == null) {
            attendees = new ArrayList<>();
        }
        attendees.add(person);
    }

    public List<Topic> getTopics() {
        return Arrays.asList(Topic.values());
    }

    public void reset() {
        this.id = null;
        this.topic = null;
        this.startingDateTime = null;
        this.durationMinutes = null;
        this.location = null;
        this.attendees = null;
    }

    public LocalDateTime getStartingDateTime() {
        return startingDateTime == null ? LocalDateTime.now() : startingDateTime;
    }

    public String getGeneralDetails() {
        return location.getName() + ", " + topic + " (" + durationMinutes + " min)";
    }

    public String getAttendeesDetails() {
        String attendeesListAsString = attendees.stream().map(Person::getName).collect(Collectors.joining(", "));
        return "Attendees: [" + attendeesListAsString + "]";
    }

    public enum Topic {
        RELEASE_PLAN, MANAGEMENT_PLANNING, SPRINT_REVIEW
    }
}
