package lab4.models;

import lab4.exceptions.DuplicateEntryException;
import lab4.persistance.PersonRepository;
import lab4.services.PersonService;
import lombok.*;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.SQLException;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Named(value = "personBean")
@RequestScoped
@EqualsAndHashCode
public class Person implements Serializable {

    private Integer id;

    @Size(min = 3)
    private String name;

    @Email
    private String email;

    public void reset() {
        this.id = null;
        this.name = null;
        this.email = null;
    }

    public Person(Integer id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    @Inject
    private PersonService personService;

    public void submit() throws SQLException, DuplicateEntryException {
        personService.save(this);

        FacesContext facesContext = FacesContext.getCurrentInstance();
        NavigationHandler navigation = facesContext.getApplication().getNavigationHandler();
        navigation.handleNavigation(facesContext, null, "index");
    }
}
