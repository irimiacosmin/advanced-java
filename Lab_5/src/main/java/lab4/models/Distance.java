package lab4.models;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.Min;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Named(value = "distanceBean")
@RequestScoped
public class Distance {
    private Integer id;
    private Location firstLocation;
    private Location secondLocation;

    @Min(value = 0)
    private Integer durationMinutes;

    public Distance(Location firstLocation, Location secondLocation, Integer durationMinutes) {
        this.firstLocation = firstLocation;
        this.secondLocation = secondLocation;
        this.durationMinutes = durationMinutes;
    }

    public String getGeneralDetails() {
        return firstLocation.getName() + " -> " + secondLocation.getName() + " takes " + durationMinutes + " minutes.";
    }
}