package lab4.models;

import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Location {
    public Integer id;

    @Size(min = 3)
    public String name;

    public Location(String name) {
        this.id = null;
        this.name = name;
    }
}
