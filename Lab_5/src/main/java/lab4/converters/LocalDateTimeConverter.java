package lab4.converters;

import lab4.utils.DateTimeUtil;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.regex.Pattern;


@FacesConverter("lab4.converters.LocalDateTimeConverter")
@Valid
public class LocalDateTimeConverter implements Converter<LocalDateTime> {
    @Override
    public LocalDateTime getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        Pattern pattern = Pattern.compile(DateTimeUtil.REGEX_MATCHES_INTL_DATE_TIME_FORMAT);
        if (!pattern.matcher(s).matches()) {
            return null;
        }
        return DateTimeUtil.fromINTLDateTime(s);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, LocalDateTime localDateTime) {
        return localDateTime == null ? null : DateTimeUtil.toUSDateTime(localDateTime);
    }
}
