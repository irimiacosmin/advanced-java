--- SCHEMA
DROP SCHEMA IF EXISTS advanced_java CASCADE;
CREATE SCHEMA advanced_java;


--- TABLES
DROP TABLE IF EXISTS advanced_java.dictionary;
CREATE TABLE advanced_java.dictionary (
  id                        serial NOT NULL,
  language_short      varchar(255) NOT NULL,
  word      	      varchar(255) NOT NULL,
  definition      	  varchar(255) NOT NULL,
  CONSTRAINT dictionary_id_pk PRIMARY KEY (id),
  CONSTRAINT dictionary_un UNIQUE (language_short, word, definition)
);