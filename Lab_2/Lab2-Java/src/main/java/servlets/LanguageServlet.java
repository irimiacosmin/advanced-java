package servlets;

import exceptions.DictionarySQLException;
import exceptions.DuplicateEntryException;
import exceptions.InvalidCaptchaException;
import models.DictionaryEntry;
import services.CaptchaService;
import services.DictionaryService;
import utils.JSPConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet(name = "LanguageServlet", urlPatterns = "/LanguageServlet")
public class LanguageServlet extends HttpServlet {

    DictionaryService dictionaryService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        dictionaryService = new DictionaryService();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        try {
            CaptchaService.verifyCaptcha(request);
            DictionaryEntry dictionaryEntry = new DictionaryEntry(request);
            dictionaryService.createEntry(dictionaryEntry);

            Cookie userCookie = new Cookie(JSPConstants.COOKIE_PREFERRED_LANGUAGE_NAME, dictionaryEntry.getLanguage().getShortVersion());
            userCookie.setMaxAge(JSPConstants.COOKIE_MAX_TIME_AGE);
            response.addCookie(userCookie);

            respond(request, response, dictionaryService.getDictionaryAsTableRows());
        } catch (DuplicateEntryException | DictionarySQLException | InvalidCaptchaException duplicateEntryException) {
            respond(request, response, duplicateEntryException);
        }
    }

    private void respond(HttpServletRequest request, HttpServletResponse response, Exception exception) {
        request.setAttribute(JSPConstants.ERROR_OBJECT_KEY, exception.getMessage());
        redirectRequestToPage(request, response, JSPConstants.ERROR_OBJECT_PAGE);
    }

    private void respond(HttpServletRequest request, HttpServletResponse response, List<String> dictionaryTableRows) {
        request.setAttribute(JSPConstants.RESULT_OBJECT_KEY, dictionaryTableRows);
        redirectRequestToPage(request, response, JSPConstants.RESULT_OBJECT_PAGE);
    }

    private void redirectRequestToPage(HttpServletRequest request, HttpServletResponse response, String pageName) {
        RequestDispatcher dispatcher = request.getRequestDispatcher(pageName);
        if (dispatcher != null) {
            try {
                dispatcher.forward(request, response);
            } catch (ServletException | IOException e) {
                e.printStackTrace();
            }
        }
    }

}
