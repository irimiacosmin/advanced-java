package models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import utils.HTMLConstants;

import javax.servlet.http.HttpServletRequest;
import java.text.MessageFormat;

@Getter
@EqualsAndHashCode
public class DictionaryEntry {

    private Language language;
    private String word;
    private String definition;

    private Boolean safe;

    public Boolean isSafe() {
        return safe;
    }

    public DictionaryEntry(String languageShortVersion, String word, String definition) {
        try {
            this.language = Language.getValueByShortVersion(languageShortVersion);
            this.word = word;
            this.definition = definition;
            this.safe = true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            this.safe = false;
        }
    }

    public DictionaryEntry(HttpServletRequest req) {
        this(
                req.getParameter("language"),
                req.getParameter("word"),
                req.getParameter("definition")
        );
    }

    public String getAsTableRow() {
        if (!this.isSafe()) {
            return "";
        }
        Object[] params = new Object[]{this.getLanguage().getName(), this.getWord(), this.getDefinition()};
        return MessageFormat.format(HTMLConstants.TABLE_ROW_TEMPLATE, params);
    }
}