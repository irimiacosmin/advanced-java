package utils;

public class MessagesConstants {

    public static final String DUPLICATE_ENTRY_MESSAGE = "Duplicate entry in the dictionary";
    public static final String INVALID_CAPTCHA_MESSAGE = "Invalid captcha. Try again!";

}
