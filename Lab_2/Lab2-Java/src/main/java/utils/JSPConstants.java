package utils;

public class JSPConstants {
    public static final String ERROR_OBJECT_KEY = "error";
    public static final String ERROR_OBJECT_PAGE = "error.jsp";

    public static final String RESULT_OBJECT_KEY = "dictionaryTableRows";
    public static final String RESULT_OBJECT_PAGE = "result.jsp";

    public static final String CAPTCHA_OBJECT_KEY = "captcha";

    public static final String COOKIE_PREFERRED_LANGUAGE_NAME = "preferredLanguage";
    public static final Integer COOKIE_MAX_TIME_AGE = 10;
}
