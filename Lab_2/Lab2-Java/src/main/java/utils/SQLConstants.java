package utils;

import models.DictionaryEntry;

import java.text.MessageFormat;

public class SQLConstants {
    public static final String DATABASE_DRIVER = "org.postgresql.Driver";
    public static final String CONNECTION_URL = "jdbc:postgresql://localhost:5432/postgres";
    public static final String USER = "postgres";
    public static final String PASSWORD = "1234";

    public static final String DUPLICATE_KEY_CODE = "23505";

    public static final String SELECT_ALL_QUERY = "SELECT language_short, word, definition FROM advanced_java.DICTIONARY;";
    private static final String GET_BY_ID_QUERY = "SELECT language_short, word, definition FROM advanced_java.DICTIONARY WHERE id = {0};";
    private static final String INSERT_QUERY = "INSERT INTO advanced_java.DICTIONARY (language_short, word, definition) VALUES (''{0}'', ''{1}'', ''{2}'');";

    public static String INSERT(DictionaryEntry dictionaryEntry) {
        return getFilledQuery(INSERT_QUERY, dictionaryEntry);
    }

    public static String GET_BY_ID(Integer dictionaryId) {
        return getFilledQuery(GET_BY_ID_QUERY, dictionaryId);
    }

    public static String getFilledQuery(String query, Object ... params) {
        return MessageFormat.format(query, params);
    }

    public static String getFilledQuery(String query, DictionaryEntry dictionaryEntry) {
        Object[] params = new Object[]{dictionaryEntry.getLanguage().getShortVersion(),
                dictionaryEntry.getWord(), dictionaryEntry.getDefinition()};
        return MessageFormat.format(query, params);
    }
}
