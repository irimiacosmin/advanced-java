package exceptions;

import utils.MessagesConstants;

public class InvalidCaptchaException extends Exception {
    public String message;

    public InvalidCaptchaException(String message) {
        super(message);
    }

    public InvalidCaptchaException() {
        super(MessagesConstants.INVALID_CAPTCHA_MESSAGE);
    }
}
