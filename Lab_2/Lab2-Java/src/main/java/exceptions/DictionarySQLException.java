package exceptions;

import utils.MessagesConstants;

public class DictionarySQLException extends Exception {
    public String message;

    public DictionarySQLException(String message) {
        super(message);
    }
}
