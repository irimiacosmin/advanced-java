package services;

import exceptions.DictionarySQLException;
import exceptions.DuplicateEntryException;
import lombok.NoArgsConstructor;
import models.DictionaryEntry;
import persistance.DictionaryRepository;
import utils.SQLConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public class DictionaryService {
    List<DictionaryEntry> database = new ArrayList<>();

    public void createEntry(DictionaryEntry dictionaryEntry) throws DuplicateEntryException, DictionarySQLException {
        if (database.contains(dictionaryEntry)) {
            throw new DuplicateEntryException();
        }
        database.add(dictionaryEntry);
    }

    public List<String> getDictionaryAsTableRows() throws DictionarySQLException {
        return database.stream()
                .map(DictionaryEntry::getAsTableRow)
                .collect(Collectors.toList());
    }
}
