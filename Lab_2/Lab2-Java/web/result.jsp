<%--
  Created by IntelliJ IDEA.
  User: CosminIulianIrimia
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Result</title>
</head>
<body>
    <h1>Response:</h1>

    <table>
        <thead>
            <tr>
                <th>English</th>
                <th>Work</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <%=request.getAttribute("dictionaryTableRows")%>
        </tbody>
    </table>
</body>
</html>
