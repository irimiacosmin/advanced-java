<%@ page import="models.Language" %><%--
  Created by IntelliJ IDEA.
  User: CosminIulianIrimia
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Input</title>
    <style>
        .form-call {
            display: flex;
        }

        .form-call > * {
            margin: 10px;
        }
    </style>
</head>
<body>
    <form action="LanguageServlet" method="post" class="form-call">
        <div>
            <label for="language">Languages</label>
            <select id="language" name="language">
                <%=Language.getAllAsSelectOptions(request.getCookies())%>
            </select>
        </div>
        <div>
            <label for="word">Word</label>
            <input id="word" type="text" name="word">
        </div>
        <div>
            <label for="definition">Definition:</label>
            <input id="definition" type="text" name="definition">
        </div>
        <div>
            <img src="http://localhost:8081/CaptchaServlet" alt="Captcha">
        </div>
        <div>
            <label for="captcha">Captcha Response:</label>
            <input id="captcha" type="text" name="captcha">
        </div>
        <div>
            <input type="submit" value="Add word to dictionary">
        </div>
    </form>
</body>
</html>
