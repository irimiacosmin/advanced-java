# Java Technologies - Lab 2
#
#
#
#### Point 1 (2p)

-  Create a Web application containing the following components:
    * input.jsp: a page containing a form for introducing entries into a dictionary, i.e. a pair containing a language, a word and a definition. The available languages are not static, being read from a server-side component (an object);
    * result.jsp a page describing the response that will be delivered to the client, for example an HTML table containing the records stored on the server.
    * error.jsp a page for displaying various errors, in an "user-friendly" manner. For example, introducing duplicate words might generate a custom exception.
    * an object-oriented domain model;
    * a server-side component responsible with the bussines-logic of the application: writing the record to a server-side data structure, reading data from it, etc.
    * a server-side component responsible with controlling the web-flow.
- The purpose of the application is to integrate various components, such as JSP pages, servlets and JavaBeans.

#### Point 2 (0.5p)

- Use a "hand-made" [cookie](https://docs.oracle.com/javaee/7/api/javax/servlet/http/Cookie.html) to store the language selected by the client. When the user returns to the site (after the current session was invalidated) and presents this cookie, the language will be set automatically.
(In case you want to store sensitive data in a cookie, you may read [Improved Persistent Login Cookie Best Practice](https://gist.github.com/oleg-andreyev/9dcef18ca3687e12a071648c1abff782).)

#### Point 3 (1p)

- Add an original, math-inspired, [CAPTCHA](http://www.captcha.net/) facility to the input form.
#
#
#
#### Bibliography
- [Lab slides](https://profs.info.uaic.ro/~acf/tj/labs/pdf/lab2.pdf)
- [JavaServer Pages Technology](https://docs.oracle.com/javaee/5/tutorial/doc/bnagx.html)
- [JavaBeans Components](https://docs.oracle.com/javaee/5/tutorial/doc/bnair.html)