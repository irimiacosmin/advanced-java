package documentmanager.utils;

import java.text.MessageFormat;

public class JMSConstants {
    /*
            asadmin create-jms-resource --restype javax.jms.Queue --property Name=STOCKS jms/stocks
            asadmin create-jms-resource --restype javax.jms.Topic --property Name=DOCUMENTS Documents
            https://rieckpil.de/howto-messaging-with-jms-using-payara-with-embedded-openmq-broker/
     */

    public static final String MESSAGE_DRIVEN_SOURCE = "stockmdb";
    public static final String JMS_QUEUE = "jms/stocks";
    public static final String JMS_TOPIC = "Documents";
    public static final String JMS_FACTORY = "jms/__defaultConnectionFactory";

    public static String getFilledQuery(String query, Object... params) {
        return MessageFormat.format(query, params);
    }
}
