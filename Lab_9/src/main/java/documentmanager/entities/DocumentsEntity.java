package documentmanager.entities;

import documentmanager.models.Document;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "documents", schema = "docs_manager")
public class DocumentsEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column(name = "registration_number", nullable = false)
    private int registrationNumber;

    @Basic
    @Column(name = "name", nullable = false, length = 30)
    private String name;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UsersEntity user;

    public Document toDocument() {
        Document document = new Document();
        document.setId(this.getId());
        document.setRegistrationNumber(this.getRegistrationNumber());
        document.setName(this.getName());

        UsersEntity user = this.getUser();
        if (user != null) {
            document.setUser(user.toUser());
        }
        return document;
    }
}
