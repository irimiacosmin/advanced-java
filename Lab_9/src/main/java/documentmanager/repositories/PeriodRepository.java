package documentmanager.repositories;

import documentmanager.entities.AdminEntity;
import documentmanager.entities.PeriodsEntity;
import documentmanager.models.Period;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class PeriodRepository {

    @Inject
    private EntityManager entityManager;

    @EJB
    private UserRepository userRepository;

    public void addPeriod(Period period) {
        PeriodsEntity periodsEntity = period.toEntity();
        AdminEntity adminEntity = userRepository.getAdminEntityById(period.getUser().getId());
        periodsEntity.setAddedBy(adminEntity);
        entityManager.persist(periodsEntity);
    }

    public List<Period> getAllPeriods() {
        Query query = entityManager.createQuery("SELECT period FROM PeriodsEntity period");

        return ((Collection<PeriodsEntity>) query.getResultList()).stream().map(PeriodsEntity::toPeriod).collect(Collectors.toList());
    }
}
