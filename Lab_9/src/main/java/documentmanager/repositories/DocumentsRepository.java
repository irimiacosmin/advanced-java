package documentmanager.repositories;

import documentmanager.entities.DocumentsEntity;
import documentmanager.entities.UsersEntity;
import documentmanager.models.Document;
import documentmanager.models.User;
import documentmanager.repositories.publishers.PublisherBean;
import documentmanager.utils.GlassfishConstants;

import javax.annotation.Resource;
import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


//@Stateless(mappedName = "documents-ejb")
//@Remote(DocumentsRepositorySpecification.class)
@Stateless
@DeclareRoles({"admin", "guest"})
public class DocumentsRepository {

    @Resource
    SessionContext ctx;

    @PersistenceContext(name = GlassfishConstants.PERSISTENCE_UNIT_NAME)
    private EntityManager entityManager;

    @EJB
    private PublisherBean publisherBean;

    @EJB
    private UserRepository userRepository;

    public void addDocument(Document document) {
        if (ctx.isCallerInRole("admin")) {
            throw new SecurityException("Not allowed!");
        }

        DocumentsEntity documentsEntity = document.toEntity();
        UsersEntity usersEntity = userRepository.getGuestEntityById(document.getUser().getId());
        documentsEntity.setUser(usersEntity);
        entityManager.persist(documentsEntity);

        publisherBean.sendDocumentMessage(document);
    }

    public void updateDocument(Document document) {
        if (ctx.isCallerInRole("admin")) {
            throw new SecurityException("Not allowed!");
        }
        DocumentsEntity existingDocument = getDocumentEntityById(document.getId());
        existingDocument.setName(document.getName());
        existingDocument.setRegistrationNumber(document.getRegistrationNumber());

        entityManager.persist(existingDocument);
    }

    public List<Document> getDocumentsByUserId(Integer id) {
        Query query = entityManager.createQuery("SELECT document FROM DocumentsEntity document where document.user.id = :id");
        query.setParameter("id", id);
        List<Document> collect = ((Collection<DocumentsEntity>) query.getResultList()).stream().map(DocumentsEntity::toDocument).collect(Collectors.toList());
        return collect;
    }

    public List<Document> getAllDocuments() {
        Query query = entityManager.createQuery("SELECT document FROM DocumentsEntity document");

        List<Document> collect = ((Collection<DocumentsEntity>) query.getResultList()).stream().map(DocumentsEntity::toDocument).collect(Collectors.toList());
        return collect;
    }

    public Document getDocumentById(Integer id) {
        return getDocumentEntityById(id).toDocument();
    }

    public DocumentsEntity getDocumentEntityById(Integer id) {
        Query query = entityManager.createQuery("SELECT document FROM DocumentsEntity document WHERE document.id=:id");
        query.setParameter("id", id);
        return ((DocumentsEntity) query.getSingleResult());
    }

    public void deleteDocumentById(Integer id) {
        Query query = entityManager.createQuery("SELECT document FROM DocumentsEntity document WHERE document.id=:id");
        query.setParameter("id", id);
        DocumentsEntity documentsEntity = ((DocumentsEntity) query.getSingleResult());
        entityManager.remove(documentsEntity);
    }
}
