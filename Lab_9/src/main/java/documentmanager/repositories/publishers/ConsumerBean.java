package documentmanager.repositories.publishers;

import documentmanager.messaging.JMSMessage;
import documentmanager.models.Document;
import documentmanager.utils.JMSConstants;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.*;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.io.StringReader;

import static documentmanager.utils.GlassfishConstants.JMS_TOPIC_DOCUMENTS;

@Stateless
public class ConsumerBean {

    @Resource
    private SessionContext sc;

    @Resource(lookup = JMS_TOPIC_DOCUMENTS)
    private Topic topic;

    @Inject
    private JMSContext context;

    public Document getDocumentMessage() {
        JMSConsumer consumer = context.createConsumer(topic);
        consumer.setMessageListener( new MessageListener() {
            public void onMessage(Message message) {
                System.out.println(message);
            }});
        return null;
    }
}
