package documentmanager.webservices.JAXWS.repositories.specifications;

import documentmanager.models.Document;

import java.util.List;

public interface DocumentsRepositorySpecification {
    List<Document> getDocumentsByUserId(Integer id);

    List<Document> getAllDocuments();
}
