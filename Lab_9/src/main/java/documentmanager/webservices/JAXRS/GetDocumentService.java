package documentmanager.webservices.JAXRS;

import documentmanager.models.Document;
import documentmanager.repositories.DocumentsRepository;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/documents")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class GetDocumentService {

    @EJB
    private DocumentsRepository documentsRepository;

    @GET
    public List<Document> getAllDocuments() {
        return documentsRepository.getAllDocuments();
    }

    @GET
    @Path("{id}")
    public Document getDocumentById(@PathParam("id") Integer id) {
        return documentsRepository.getDocumentById(id);
    }

}
