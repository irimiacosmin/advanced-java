package documentmanager.models;

import documentmanager.entities.DocumentsEntity;
import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Document implements Serializable {
    private Integer id;
    private Integer registrationNumber;
    private String name;
    private User user;

    public Document(Integer registrationNumber, String name) {
        this.registrationNumber = registrationNumber;
        this.name = name;
    }

    public DocumentsEntity toEntity() {
        DocumentsEntity documentsEntity = new DocumentsEntity();
        if (this.getId() != null) {
            documentsEntity.setId(this.getId());
        }
        documentsEntity.setRegistrationNumber(this.getRegistrationNumber());
        documentsEntity.setName(this.getName());

        documentsEntity.setUser(this.getUser().toEntity());
        return documentsEntity;
    }
}
