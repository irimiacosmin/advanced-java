package documentmanager.messaging;

import documentmanager.utils.JMSConstants;

import javax.annotation.PostConstruct;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.StringReader;

@MessageDriven(name = JMSConstants.MESSAGE_DRIVEN_SOURCE,
        activationConfig = {
                @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = JMSConstants.JMS_QUEUE),
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")})
public class JMSMessageListener implements MessageListener {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void onMessage(Message message) {

        TextMessage textMessage = (TextMessage) message;

        try {
            System.out.println("A new message information arrived: " + textMessage.getText());

            JsonReader jsonReader = Json.createReader(new StringReader(textMessage.getText()));
            JsonObject stockInformation = jsonReader.readObject();

            em.persist(new JMSMessage(stockInformation));
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

}