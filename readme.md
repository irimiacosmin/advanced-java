# Java-Technologies Labs
#
#### Implementation of Java Technologies Master Course Laboratories
#
#
## [Laboratory 1](./Lab_1)
### Java Servlet Technology
#
## [Laboratory 2](./Lab_2)
### JavaServerPages, Servlets, Beans
#
## [Laboratory 3](./Lab_3)
### Web Filters, Custom JSP Tags, JSTL
#
## [Laboratory 4](./Lab_4)
### JavaServer Faces
#
## [Laboratory 5](./Lab_5)
### JavaServer Faces, Facelets. JNDI and Resources.
#
## [Laboratory 6](./Lab_6)
### Java Persistence API (JPA)
#
## [Laboratory 7](./Lab_7)
### Enterprise Java Beans (EJB)
#
## [Laboratory 8](./Lab_8)
### Contexts and Dependency Injection (CDI)
#
## [Laboratory 9](./Lab_9)
### Web Services
#
## [Laboratory 10](./Lab_10)
### MicroProfile - Enterprise Java for Microservices
#
## [Laboratory 11](./Lab_11)
### Introduction to Security in the Java EE Platform