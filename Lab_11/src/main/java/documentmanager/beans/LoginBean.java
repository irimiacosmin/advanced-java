package documentmanager.beans;

import documentmanager.decorators.RegisterPeriod;
import documentmanager.interceptors.ValidPeriod;
import documentmanager.models.User;
import documentmanager.repositories.UserRepository;
import documentmanager.services.AuthService;
import lombok.Getter;
import lombok.Setter;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Named
@RequestScoped
@Setter
@Getter
public class LoginBean implements Serializable, RegisterPeriod {

    private User user = new User();

    @Inject
    private AuthService authService;

    @EJB
    private UserRepository userRepository;

    private HttpServletRequest getRequest() {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        return (HttpServletRequest) externalContext.getRequest();
    }

    public String login() throws ServletException {
        HttpServletRequest request = getRequest();
        request.login(user.getUsername(), user.getPassword());
        authService.login(user);
        return "/pages/home?faces-redirect=true";
    }

    public String logout() throws ServletException {
        HttpServletRequest request = getRequest();
        request.logout();
        authService.logout();
        return "/all/index?faces-redirect=true";
    }

    public List<User.UserType> getUserTypes() {
        return Arrays.stream(User.UserType.values()).collect(Collectors.toList());
    }

    @ValidPeriod
    public String register() throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("md5");
        String password = user.getPassword();
        md.update(password.getBytes(), 0, password.length());
        byte[] passwordDigest = md.digest();
        user.setPassword(new BigInteger(1, passwordDigest).toString(16));
        userRepository.addUser(user);
        return "pages/index?faces-redirect=true";
    }
}
