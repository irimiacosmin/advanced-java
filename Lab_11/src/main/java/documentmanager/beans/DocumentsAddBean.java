package documentmanager.beans;

import documentmanager.interceptors.ValidPeriod;
import documentmanager.models.Document;
import documentmanager.producers.DocumentRegistrationNumber;
import documentmanager.repositories.DocumentsRepository;
import documentmanager.services.AuthService;
import documentmanager.services.alternatives.CloudService;
import documentmanager.services.alternatives.DocumentService;
import documentmanager.services.alternatives.FileService;
import documentmanager.utils.FileConstants;
import documentmanager.webservices.JAXWS.ws.ejbs.CacheBean;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.ejb.EJB;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Any;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;
import java.io.Serializable;

@Named
@ViewScoped
@Getter
@Setter
public class DocumentsAddBean implements Serializable {

    private UploadedFile file;

    private Part uploadedFile;

    @Inject
    AuthService authService;

    @Inject
    @DocumentRegistrationNumber
    private int registrationNumber;

    @EJB
    private DocumentsRepository documentsRepository;

    @EJB
    private CacheBean cacheBean;

    @Inject
    @Any
    private Event<Document> documentEvent;

    @Inject
    @CloudService
    private FileService documentService;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    @ValidPeriod
    public void upload() {
        if (uploadedFile == null) {
            return;
        }

        byte[] fileContent = FileConstants.getByteArrayFromPartItem(uploadedFile);

        if (fileContent == null) {
            return;
        }

        String fileName = uploadedFile.getSubmittedFileName();
        documentService.uploadDocument(fileContent, registrationNumber);
        Document newDocument = new Document(registrationNumber, fileName);
        newDocument.setUser(authService.getCurrentUser());
        documentsRepository.addDocument(newDocument);
        cacheBean.reset();
        documentEvent.fire(newDocument);
        FacesMessage message = new FacesMessage("Successful", fileName + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void handleFileUpload(FileUploadEvent event) {
        FacesMessage msg = new FacesMessage("Successful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
