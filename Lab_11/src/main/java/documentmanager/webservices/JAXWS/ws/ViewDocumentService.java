package documentmanager.webservices.JAXWS.ws;

import documentmanager.models.Document;
import documentmanager.repositories.DocumentsRepository;
import documentmanager.webservices.JAXWS.ws.ejbs.CacheBean;

import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(name="ViewDocumentService")
@HandlerChain(file = "simple-chain.xml")
public class ViewDocumentService implements ViewDocumentServiceInterface {

    @EJB
    private CacheBean cacheBean;

    @WebMethod
    public ArrayList<Document> getAllDocumentsByUserId(Integer userId) {
        if (userId == null) {
            return cacheBean.getDocuments();
        }
        return cacheBean.getDocumentsByUserId(userId);
    }

    @WebMethod
    public String getTestString(String param) {
        return "Hello MR. " + param;
    }
}
