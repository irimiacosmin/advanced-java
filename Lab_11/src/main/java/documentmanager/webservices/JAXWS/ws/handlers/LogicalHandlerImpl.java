package documentmanager.webservices.JAXWS.ws.handlers;

import documentmanager.webservices.JAXWS.ws.ejbs.CacheBean;
import org.xml.sax.InputSource;

import javax.ejb.EJB;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.LogicalMessage;
import javax.xml.ws.handler.LogicalHandler;
import javax.xml.ws.handler.LogicalMessageContext;
import javax.xml.ws.handler.MessageContext;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Objects;

public class LogicalHandlerImpl implements LogicalHandler<LogicalMessageContext> {

    @EJB
    private CacheBean cacheBean;

    @Override
    public boolean handleMessage(LogicalMessageContext context) {
        Boolean outboundProperty = (Boolean)
                context.get (MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (outboundProperty) {
            return true;
        }

        LogicalMessage message = context.getMessage();
        Source payload = message.getPayload();

        try {
            StringWriter sw = new StringWriter();
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult( sw );
            transformer.transform( payload, output );
            org.w3c.dom.Document xmlParsedDoc = convertStringToXMLDocument(sw.toString());
            if (xmlParsedDoc == null || !xmlParsedDoc.getFirstChild().getNodeName().equals("ns2:getAllDocumentsByUserId")) {
                return true;
            }
            String param = xmlParsedDoc.getElementsByTagName("arg0").item(0).getFirstChild().getNodeValue();
            Integer userId = Integer.parseInt(param);
            cacheBean.addUserToCache(userId);
            System.out.println("Response : "+sw.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(payload);
        return true;
    }

    @Override
    public boolean handleFault(LogicalMessageContext context) {
        return false;
    }

    @Override
    public void close(MessageContext context) {

    }

    private static org.w3c.dom.Document convertStringToXMLDocument(String xmlString)
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
            org.w3c.dom.Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
