package documentmanager.webservices.JAXWS.ws;

import documentmanager.models.Document;
import documentmanager.webservices.JAXWS.ws.ejbs.CacheBean;

import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.List;

@WebService(name="ViewDocumentService")
public interface ViewDocumentServiceInterface {

    @WebMethod
    ArrayList<Document> getAllDocumentsByUserId(Integer userId);

    @WebMethod String getTestString(String param);
}
