package documentmanager.webservices.JAXRS;

import documentmanager.models.Document;
import documentmanager.models.User;
import documentmanager.repositories.DocumentsRepository;
import org.eclipse.microprofile.faulttolerance.*;
import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.ClaimValue;
import org.eclipse.microprofile.metrics.annotation.Timed;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Path("/documents")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class GetDocumentService {

    @EJB
    private DocumentsRepository documentsRepository;

    @Inject
    @Claim("custom-value")
    private ClaimValue<String> custom;

    @GET
    @Path("/classified")
    @RolesAllowed("admin")
    public List<Document> getProtectedDocuments() {
        User unknownUser = new User(0,  custom.getValue(), "4e25bd04abf1a14d65b11d7c71f72af7", User.UserType.admin);

        return documentsRepository.getAllDocuments().stream()
                        .peek(document -> document.setUser(unknownUser)).collect(Collectors.toList());
    }

    @GET
    public List<Document> getAllDocuments() {
        return documentsRepository.getAllDocuments();
    }

    /*
        Fallback
    */
    @Timeout(500)
    @Fallback(fallbackMethod = "fallback")
    @Timed(name = "fallback-timed")
    @GET
    @Path("fallback/{id}")
    public List<Document> getAllDocumentsFallback(@PathParam("id") Integer id) throws SomeException {
        if (id == -1) {
            throw new SomeException();
        }
        return documentsRepository.getAllDocuments();
    }

    private List<Document> fallback(Integer id) throws SomeException {
        List<Document> documents = new ArrayList<>();
        documents.add(Document.builder().name("Fallback document").build());
        return documents;
    }

    /*
        Retry
    */
    @Retry(maxRetries = 2)
    @Fallback(fallbackMethod = "retry")
    @Timed(name = "retry-timed")
    @GET
    @Path("retry/{id}")
    public List<Document> getAllDocumentsRetry(@PathParam("id") Integer id) throws SomeException {
        if (id == -1) {
            throw new SomeException();
        }
        return documentsRepository.getAllDocuments();
    }

    private List<Document> retry(Integer id) {
        List<Document> documents = new ArrayList<>();
        documents.add(Document.builder().name("Retry document").build());
        return documents;
    }

    /*
        CircuitBreaker
    */
    @CircuitBreaker(successThreshold = 10, requestVolumeThreshold = 4, failureRatio = 0.75, delay = 1000)
    @Timed(name = "circuit-breaker-timed")
    @GET
    @Path("circuit-breaker/{id}")
    public List<Document> getAllDocumentsCircuitBreaker(@PathParam("id") Integer id) throws SomeException {
        if (id == -1) {
            throw new SomeException();
        }
        return documentsRepository.getAllDocuments();
    }

    private List<Document> circuitBreaker(Integer id) {
        List<Document> documents = new ArrayList<>();
        documents.add(Document.builder().name("CircuitBreaker document").build());
        return documents;
    }

    /*
        Bulkhead thread pool
    */
    @Bulkhead(5)
    @Timed(name = "thread-pool-timed")
    @GET
    @Path("thread-pool/{id}")
    public List<Document> getAllDocumentsBulkheadThreadPool(@PathParam("id") Integer id) throws SomeException {
        if (id == -1) {
            throw new SomeException();
        }
        return documentsRepository.getAllDocuments();
    }

    /*
        Bulkhead semaphore
    */
    @Bulkhead(value = 5, waitingTaskQueue = 8)
    @Timed(name = "semaphore-timed")
    @GET
    @Path("semaphore/{id}")
    public List<Document> getAllDocumentsBulkheadSemaphore(@PathParam("id") Integer id) throws SomeException {
        if (id == -1) {
            throw new SomeException();
        }
        return documentsRepository.getAllDocuments();
    }

    @GET
    @Path("{id}")
    public Document getDocumentById(@PathParam("id") Integer id) {
        return documentsRepository.getDocumentById(id);
    }

}
