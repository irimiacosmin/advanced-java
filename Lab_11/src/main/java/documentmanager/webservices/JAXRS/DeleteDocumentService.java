package documentmanager.webservices.JAXRS;

import documentmanager.models.Document;
import documentmanager.repositories.DocumentsRepository;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/documents")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class DeleteDocumentService {

    @EJB
    private DocumentsRepository documentsRepository;

    @DELETE
    @Path("{id}")
    public void deleteDocument(@PathParam("id") Integer id) {
        documentsRepository.deleteDocumentById(id);
    }
}
