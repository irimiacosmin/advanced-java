package documentmanager.webservices.JAXRS;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Liveness;
import org.eclipse.microprofile.health.Readiness;

import javax.ejb.Singleton;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Produces;

@Singleton
public class ServiceReadyHealthCheck {

    @Produces
    @Liveness
    public HealthCheck live() {
        return () -> HealthCheckResponse.named("live-check").up().build();
    }

    @Produces
    @Readiness
    public HealthCheck ready() {
        return () -> HealthCheckResponse.named("ready-check").up().build();
    }
}
