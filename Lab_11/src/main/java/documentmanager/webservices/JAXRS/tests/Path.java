package documentmanager.webservices.JAXRS.tests;

import lombok.Getter;

@Getter
public enum Path {

    CIRCUIT_BREAKER("/circuit-breaker/"),
    FALLBACK("/fallback/"),
    RETRY("/retry/"),
    SEMAPHORE("/semaphore/"),
    THREAD_POOL("/thread-pool/");

    String discriminator;

    Path(String discriminator) {
        this.discriminator = discriminator;
    }

    public String getFullPath() {
        String machineSpecificBasePath = OS.isRunningInDocker() ? "documents-container" : "localhost";
        return "http://" + machineSpecificBasePath + ":8080/rest/webresources/documents" + this.discriminator;
    }
}
