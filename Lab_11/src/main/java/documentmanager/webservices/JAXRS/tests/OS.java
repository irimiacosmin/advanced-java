package documentmanager.webservices.JAXRS.tests;

public class OS {

    public static String getOSName() {
        return System.getProperty("os.name");
    }

    public static boolean isRunningInDocker() {
        return getOSName().toLowerCase().contains("linux");
    }
}
