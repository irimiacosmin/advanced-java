package documentmanager.webservices.JAXRS;

import documentmanager.models.Document;
import documentmanager.repositories.DocumentsRepository;
import org.eclipse.microprofile.health.HealthCheckResponse;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/health")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class HealthController {

    @EJB
    private ServiceReadyHealthCheck serviceReadyHealthCheck;

    @GET
    @Path("live")
    public HealthCheckResponse live() {
        return serviceReadyHealthCheck.live().call();
    }

    @GET
    @Path("ready")
    public HealthCheckResponse ready() {
        return serviceReadyHealthCheck.ready().call();
    }

}
