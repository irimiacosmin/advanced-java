package documentmanager.services;

import documentmanager.models.Document;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.Date;

import static documentmanager.utils.FileConstants.LOG_PATH;

@ApplicationScoped
public class TxtLoggingService implements Serializable {

    private Path logFile;

    @PostConstruct
    public void init() {
        logFile = Paths.get(LOG_PATH);
    }

    public void onDocumentsUpload(@Observes Document document) {
        try {
            Files.write(logFile,
                    Collections.singleton("New document added: " + document.toString() + " at " + new Date() + "\n"),
                    StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
