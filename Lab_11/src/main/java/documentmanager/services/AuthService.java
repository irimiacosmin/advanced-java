package documentmanager.services;

import documentmanager.models.User;
import documentmanager.repositories.UserRepository;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import java.io.Serializable;

@Named
@ApplicationScoped
public class AuthService implements Serializable {

    @EJB
    UserRepository userRepository;

    private User currentUser;

    public void login(User user) {
        currentUser = userRepository.getUserByName(user);
    }

    public void logout() {
        currentUser = null;
    }

    public User getCurrentUser() {
        return currentUser;
    }
}
