package documentmanager.repositories;

import documentmanager.models.Document;
import documentmanager.repositories.specifications.DocumentsRepositorySpecification;

import java.util.List;

public class DocumentsRepositoryNew implements DocumentsRepositorySpecification {

    //    @EJB(mappedName = "documents-ejb")
    private DocumentsRepositorySpecification documentsRepository;

    public List<Document> getDocumentsByUserId(Integer id) {
        return documentsRepository.getDocumentsByUserId(id);
    }

    public List<Document> getAllDocuments() {
        return documentsRepository.getAllDocuments();
    }

    public void addDocument(Document document) {
        documentsRepository.addDocument(document);
    }

    public Document getDocumentById(Integer id) {
        return documentsRepository.getDocumentById(id);
    }

    public void deleteDocumentById(Integer id) {
        documentsRepository.deleteDocumentById(id);
    }
}
