package documentmanager.repositories;

import documentmanager.entities.AdminEntity;
import documentmanager.entities.GuestEntity;
import documentmanager.entities.UsersEntity;
import documentmanager.models.User;
import documentmanager.utils.GlassfishConstants;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.*;

@Stateless
public class UserRepository {

    EntityManager entityManager;

    @PostConstruct
    private void init() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(GlassfishConstants.PERSISTENCE_UNIT_NAME);
        entityManager = emf.createEntityManager();
    }

    public User getUserByName(User user) {
        Query query = entityManager.createQuery("SELECT ue FROM UsersEntity ue WHERE ue.username=:username");
        query.setParameter("username", user.getUsername());

        Object singleResult = query.getSingleResult();
        return ((UsersEntity) singleResult).toUser();
    }

    public AdminEntity getAdminEntityById(Integer id) {
        Query query = entityManager.createQuery("SELECT ue FROM AdminEntity ue WHERE ue.id=:id");
        query.setParameter("id", id);

        Object singleResult = query.getSingleResult();
        return (AdminEntity) singleResult;
    }

    public GuestEntity getGuestEntityById(Integer id) {
        Query query = entityManager.createQuery("SELECT ue FROM GuestEntity ue WHERE ue.id=:id");
        query.setParameter("id", id);

        Object singleResult = query.getSingleResult();
        return (GuestEntity) singleResult;
    }

    public void addUser(User user) {
        try {
            getUserByName(user);
            return;
        } catch (NoResultException e) {
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        UsersEntity usersEntity = null;
        switch (user.getType()) {
            case admin: {
                usersEntity = user.toAdminEntity();
                break;
            }
            case guest: {
                usersEntity = user.toGuestEntity();
                break;
            }
            default: {
                return;
            }
        }
        entityManager.persist(usersEntity);
    }
}
