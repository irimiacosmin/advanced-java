package clientapp;

import documentmanager.webservices.JAXWS.ws.ViewDocumentServiceInterface;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

public class Main {
    public static final String HOST_URL = "http://localhost:8080/Lab_8_1-1.0-SNAPSHOT/ViewDocumentServiceService?wsdl";
    public static final String NAMESPACE = "http://ws.JAXWS.webservices.documentmanager/";
    public static final String LOCAL_PART = "ViewDocumentServiceService";

    public static void main(String[] args) throws Exception {
        QName serviceName = new QName(NAMESPACE, LOCAL_PART);
        Service service = Service.create(new URL(HOST_URL), serviceName);
        ViewDocumentServiceInterface viewDocumentServiceInterface = service.getPort(ViewDocumentServiceInterface.class);
        System.out.println(viewDocumentServiceInterface.getTestString("Cosmin"));
        System.out.println(viewDocumentServiceInterface.getAllDocumentsByUserId(1));
        System.out.println(viewDocumentServiceInterface.getAllDocumentsByUserId(2));
    }
}
