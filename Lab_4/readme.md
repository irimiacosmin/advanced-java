# Java Technologies - Lab 4
#
##### Create a Web application using JavaServer Faces technology, dedicated to the [Meeting Scheduling Problem](http://www.csplib.org/Problems/prob046/).
#
#
#### Point 1 (2p)

-  Create the following components:
    * A Web page for defining the persons that must attend the meetings. Each person has an id and a name.
    * A Web page for defining the meetings. Each meeting has an id, a topic, a starting time and a duration. Each meeting is associated with a set of persons that must attend it.
- Use a relational database and JDBC in order to store and retrieve data. (PostgreSQL is recommended).
- Use at least one non trivial JSF component, for example a data table, a dialog, etc.

#### Point 2 (2p)

- Solve the problem.
    * Create an algorithm for solving the problem. The scheduled time-slots must enable the participating to attend their meetings.
    * You may use a constraint programming solver, such as [Choco](https://choco-solver.org/) or [OptaPlanner](https://www.optaplanner.org/), etc.
    * Create a page for displaying the results.
    * Create an "import page", for reading data from [external files](http://www.csplib.org/Problems/prob046/data/instances.md.html) (choose your own format).
    * (Bonus) Consider the case where each meeting has also a location and the the time required to cover the distances between locations is known. The schedule must enable the participating to travel among their meetings.
- Notes:
    * It is recommended to use an Ajax-based JSF implementation: PrimeFaces, BootsFaces, ICEFaces (ACE components), etc.
    * Additional points will be given for using JSF technology beyond the "beginner" level (use custom converters, validators, "rich" components, i18n, etc.).
#
#
#
#### Bibliography
- [Lab slides](https://profs.info.uaic.ro/~acf/tj/labs/pdf/lab4.pdf)
- [The Web Tier](https://docs.oracle.com/javaee/7/tutorial/partwebtier.htm#BNADP)
- [JavaServer Faces Technology](https://docs.oracle.com/javaee/7/tutorial/jsf-intro.htm)
- [Introduction to Facelets Library](https://docs.oracle.com/javaee/7/tutorial/jsf-facelets.htm#GIEPX)
- [JavaServer Faces 2.2 Facelets Tag Library Documentation](https://docs.oracle.com/javaee/7/javaserver-faces-2-2/vdldocs-facelets/toc.htm)