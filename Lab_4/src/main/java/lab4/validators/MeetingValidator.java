package lab4.validators;


import lab4.models.Meeting;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MeetingValidator implements ConstraintValidator<MeetingIsValid, Meeting> {

    private String message;
    private String fieldName;

    @Override
    public void initialize(MeetingIsValid constraintAnnotation) {
        message = constraintAnnotation.message();
        fieldName = constraintAnnotation.fieldName();
    }

    @Override
    public boolean isValid(Meeting meeting, ConstraintValidatorContext constraintValidatorContext) {
        constraintValidatorContext.disableDefaultConstraintViolation();
        constraintValidatorContext
                .buildConstraintViolationWithTemplate(message)
                .addPropertyNode(fieldName)
                .addConstraintViolation();



        return false;
    }
}
