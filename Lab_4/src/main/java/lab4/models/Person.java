package lab4.models;

import lombok.*;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Named(value = "personBean")
@RequestScoped
@EqualsAndHashCode
public class Person implements Serializable {

    private Integer id;

    @Size(min = 3)
    private String name;

    @Email
    private String email;

    public void reset() {
        this.id = null;
        this.name = null;
        this.email = null;
    }
}
