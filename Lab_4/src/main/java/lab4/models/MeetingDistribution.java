package lab4.models;

import lab4.utils.DateTimeUtil;
import lombok.*;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Named(value = "meetingDistributionBean")
@RequestScoped
@EqualsAndHashCode
public class MeetingDistribution implements Serializable {

    public static final LocalTime todayMorningTime = LocalTime.of(9, 0);
    private static final LocalDateTime currentDateTime = LocalDateTime.now();
    public static final LocalDateTime todayMorningDateTime = LocalDateTime.of(currentDateTime.getYear(), currentDateTime.getMonth(), currentDateTime.getDayOfMonth(), 9, 0);
    private Meeting meeting;
    private LocalDateTime startDateTime;
    private LocalTime meetingEndTime;
    private LocalTime nextMeetingArrivalTime;

    public MeetingDistribution(Meeting meeting, Integer startingMinutes, Integer minutesToTravel) {
        this.meeting = meeting;
        this.startDateTime = todayMorningDateTime.plusMinutes(startingMinutes);
        this.meetingEndTime = todayMorningTime.plusMinutes(startingMinutes).plusMinutes(meeting.getDurationMinutes());
        this.nextMeetingArrivalTime = this.meetingEndTime.plusMinutes(minutesToTravel);
    }

    public String getGeneralDetails() {
        return "Started at: " + DateTimeUtil.toINTLDateTime(this.startDateTime) + " | Ending Time: " + meetingEndTime.toString() + " | Ariving time to next meeting: " + nextMeetingArrivalTime.toString();
    }
}
