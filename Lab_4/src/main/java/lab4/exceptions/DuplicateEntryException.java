package lab4.exceptions;

import lab4.utils.MessagesConstants;

public class DuplicateEntryException extends Exception {
    public String message;

    public DuplicateEntryException(String message) {
        super(message);
    }

    public DuplicateEntryException() {
        super(MessagesConstants.DUPLICATE_ENTRY_MESSAGE);
    }
}
