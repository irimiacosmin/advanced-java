package lab4.services;

import com.google.gson.Gson;
import lab4.exceptions.DuplicateEntryException;
import lab4.models.Location;
import lab4.models.Meeting;
import lab4.models.Person;
import lab4.persistance.LocationRepository;
import lab4.persistance.MeetingRepository;
import lab4.persistance.PersonRepository;
import lab4.utils.DateTimeUtil;
import lombok.Getter;
import lombok.Setter;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Named(value = "FileUpload")
@RequestScoped
@Getter
@Setter
public class FileUpload {

    @Inject
    private PersonRepository personRepository;

    @Inject
    private MeetingRepository meetingRepository;

    @Inject
    private LocationRepository locationRepository;

    private Part uploadedFile;

    public void importFile() throws SQLException, DuplicateEntryException {
        String fileContent = getFileContent();
        Gson gson = new Gson();
        DataInputModel dataInputModel = gson.fromJson(fileContent, DataInputModel.class);
        createEntities(dataInputModel);
    }

    private String getFileContent() {
        StringBuilder textBuilder = new StringBuilder();
        try (InputStream input = uploadedFile.getInputStream()) {

            try (Reader reader = new BufferedReader(
                    new InputStreamReader(input, Charset.forName(StandardCharsets.UTF_8.name())))) {
                int c = 0;
                while ((c = reader.read()) != -1) {
                    textBuilder.append((char) c);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return textBuilder.toString().replaceAll("(\\r|\\n|\\t)", "");
    }

    private void createEntities(DataInputModel dataInputModel) throws SQLException, DuplicateEntryException {
        Map<Integer, Person> personsMappedByIdentity = dataInputModel.getPersons()
                .stream()
                .collect(Collectors.toMap(PersonInputModel::getIdentifier, transformToPersonModel()));

        for (Map.Entry<Integer, Person> entry : personsMappedByIdentity.entrySet()) {
            Person currentPerson = entry.getValue();
            int personId = personRepository.add(currentPerson);
            currentPerson.setId(personId);
            personsMappedByIdentity.put(entry.getKey(), currentPerson);
        }

        Map<Integer, Location> locationsMappedByIdentity = dataInputModel.getLocations()
                .stream()
                .collect(Collectors.toMap(LocationInputModel::getIdentifier, transformToLocationModel()));

        for (Map.Entry<Integer, Location> entry : locationsMappedByIdentity.entrySet()) {
            Location currentLocation = entry.getValue();
            int locationId = locationRepository.add(currentLocation);
            currentLocation.setId(locationId);
            locationsMappedByIdentity.put(entry.getKey(), currentLocation);
        }

        for (MeetingInputModel meetingInputModel : dataInputModel.getMeetings()) {
            Meeting.Topic topic = Meeting.Topic.valueOf(meetingInputModel.getTopic());
            LocalDateTime startingDateTime = DateTimeUtil.fromINTLDateTime(meetingInputModel.getStartingDateTime());
            Integer durationMinutes = meetingInputModel.getDurationMinutes();
            Location location = locationsMappedByIdentity.get(meetingInputModel.getLocationIdentifier());
            List<Person> personList = meetingInputModel.getAttendeesIdentifiers()
                    .stream()
                    .map(personsMappedByIdentity::get)
                    .collect(Collectors.toList());
            Meeting currentMeeting = new Meeting(null, topic, startingDateTime, durationMinutes, location, personList);
            meetingRepository.add(currentMeeting);
        }
    }

    private Function<PersonInputModel, Person> transformToPersonModel() {
        return personInputModel -> new Person(null, personInputModel.getName(), personInputModel.getEmail());
    }

    private Function<LocationInputModel, Location> transformToLocationModel() {
        return locationInputModel -> new Location(null, locationInputModel.getName());
    }
}