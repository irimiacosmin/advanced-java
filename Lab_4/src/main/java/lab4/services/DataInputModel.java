package lab4.services;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
class PersonInputModel {
    private Integer identifier;
    private String name;
    private String email;
}

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
class LocationInputModel {
    private Integer identifier;
    private String name;
}

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
class MeetingInputModel {
    private Integer identifier;
    private String topic;
    private String startingDateTime;
    private Integer durationMinutes;
    private Integer locationIdentifier;
    private List<Integer> attendeesIdentifiers;
}

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DataInputModel {
    List<PersonInputModel> persons;
    List<LocationInputModel> locations;
    List<MeetingInputModel> meetings;
}