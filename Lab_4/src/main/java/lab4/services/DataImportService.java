package lab4.services;

import com.google.gson.Gson;
import lombok.Getter;
import lombok.Setter;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.Part;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@Named(value = "Data1Import")
@ViewScoped
@Getter
@Setter
public class DataImportService implements Serializable {

    private final Gson gson = new Gson();
    private Part uploadedFile;

    public Part getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(Part uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public Gson getGson() {
        return gson;
    }

    private String getFileContent() {
        StringBuilder textBuilder = new StringBuilder();
        try (InputStream input = uploadedFile.getInputStream()) {

            try (Reader reader = new BufferedReader(
                    new InputStreamReader(input, Charset.forName(StandardCharsets.UTF_8.name())))) {
                int c = 0;
                while ((c = reader.read()) != -1) {
                    textBuilder.append((char) c);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return textBuilder.toString();
    }

    public void importFile() {
        String fileContent = getFileContent();
        DataInputModel dataInputModel = gson.fromJson(fileContent, DataInputModel.class);
        System.out.println(1);
    }
}
