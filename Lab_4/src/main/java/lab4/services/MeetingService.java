package lab4.services;

import lab4.exceptions.DuplicateEntryException;
import lab4.exceptions.EntryNotFoundException;
import lab4.models.Location;
import lab4.models.Meeting;
import lab4.models.Person;
import lab4.persistance.LocationRepository;
import lab4.persistance.MeetingRepository;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Named(value = "MeetingService")
@SessionScoped
public class MeetingService implements Serializable {

    @Inject
    private DistanceService distanceService;

    @Inject
    private PersonService personService;

    @Inject
    private MeetingRepository meetingRepository;

    @Inject
    private LocationRepository locationRepository;

    public List<Meeting> getAll() throws SQLException, EntryNotFoundException {
        return meetingRepository.getAll();
    }

    public void save(Meeting meeting) throws SQLException, DuplicateEntryException {
        distanceService.saveDistances(meeting.getLocation());
        List<Person> attendees = personService.getAttendeesChecked()
                .entrySet()
                .stream()
                .filter(Map.Entry::getValue)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        meeting.setAttendees(attendees);
        meetingRepository.add(meeting);
        meeting.reset();
    }

    public List<Location> getPossibleLocations() {
        List<Location> locations = new ArrayList<>();
        try {
            locations = locationRepository.getAll();
        } catch (Exception e) {
            System.err.println(Arrays.toString(e.getStackTrace()));
        }
        return locations;
    }

    public Location getLocationByToString(String toString) {
        return getPossibleLocations().stream()
                .filter(location -> location.toString().equals(toString))
                .findFirst()
                .orElse(null);
    }

}
