package lab4.persistance;

import lab4.exceptions.DuplicateEntryException;
import lab4.exceptions.EntryNotFoundException;
import lab4.models.Location;
import lab4.sql.DatabaseService;
import lab4.utils.SQLConstants;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Named
@ApplicationScoped
public class LocationRepository implements SkeletonRepository<Location>, Serializable {

    @Override
    public int add(Location entry) throws SQLException, DuplicateEntryException {
        return genericAdd(SQLConstants.INSERT(entry));
    }

    @Override
    public Location getById(int id) throws SQLException, EntryNotFoundException {
        return getEntries(SQLConstants.GET_BY_ID(Location.class, id))
                .stream()
                .findFirst()
                .orElseThrow(EntryNotFoundException::new);
    }

    @Override
    public List<Location> getAll() throws SQLException {
        return getEntries(SQLConstants.SELECT_ALL(Location.class));
    }

    private List<Location> getEntries(String query) throws SQLException {
        List<Location> entries = new ArrayList<>();
        Statement statement = DatabaseService.getConnectionInstance().createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            entries.add(new Location(id, name));
        }
        resultSet.close();
        return entries;
    }
}
