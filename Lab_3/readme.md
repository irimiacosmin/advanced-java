# Java Technologies - Lab 3
#
#
#
#### Point 1 (2p)

-  Continue the application created in the previous lab, adding the following components:
    * A web filter that will log all requests received by input.jsp or result.jsp pages.
    * A web filter that will validate the requests addressed to the InputController. If either word or definition is missing, the request will be redirected back to input.jsp.
    If language parameter is missing, the request will be decorated such that the returned language will have a default value, specified as a [context init parameter](https://javatutorial.net/java-init-parameters).
    * A web filter that will decorate the response by manipulating the HTML document (elements, attributes or text). You may use an [HTML parser](https://jsoup.org/).
- Important: we assume that the pages are already created and the functionalities describead above cannot be implemented by modifying them directly.

#### Point 2 (0.5p)

- Create a custom tag with the name definition that allows the inclusion in the response page of the definition of a word. Example: <definition word='scalability'/>. The language attribute is optional.

#### Point 3 (0.5p)

- Create a custom tag using JSTL to implement a view of the data.
- The execution of this tag will create an HTML table containing the words that were saved on the server. Example: <viewDictionary language='en'/>
- To create the view custom tag use a tag file and not a class file handler. The goal of this exercise is to use JSTL - do not write any Java code.

#### Point 4 (0.5p)

- Internationalize the application using at least two localizations (en and ro). Translate the static text and display the dates when words were added into the dictionary, according to the selected locale.
#
#
#
#### Bibliography
- [Lab slides](https://profs.info.uaic.ro/~acf/tj/labs/pdf/lab3.pdf)
- [The Essentials of Filters](https://www.oracle.com/java/technologies/filters.html)
- [Custom Tags in JSP Pages](https://docs.oracle.com/javaee/5/tutorial/doc/bnalj.html)
- [JavaServer Pages Standard Tag Library](https://docs.oracle.com/javaee/5/tutorial/doc/bnakc.html)