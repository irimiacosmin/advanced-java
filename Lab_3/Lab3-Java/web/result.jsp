<%@ page import="models.DictionaryEntry" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: CosminIulianIrimia
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tellme" %>

<html>
<head>
    <title>Result</title>
</head>
<body>
    <tellme:definition word="bug"/>
    <%
        List<DictionaryEntry> dictionary = (List<DictionaryEntry>) request.getAttribute("dictionary");
        String locale = (String) request.getAttribute("locale");
    %>
    <tellme:viewDictionary dictionary="<%=dictionary%>" language="<%=locale%>"/>
</body>
</html>
