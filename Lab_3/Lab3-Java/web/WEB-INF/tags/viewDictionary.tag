<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="dictionary" required="true" type="java.util.List" %>
<%@ attribute name="language" required="true" type="java.lang.String" %>

<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="lab3package.Messages" var="localeBundle" scope="page"/>

<table>
    <thead>
    <tr>
        <th><fmt:message key="language" bundle="${localeBundle}"/></th>
        <th><fmt:message key="word" bundle="${localeBundle}"/></th>
        <th><fmt:message key="description" bundle="${localeBundle}"/></th>
        <th><fmt:message key="creation" bundle="${localeBundle}"/></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${dictionary}" var="dictionaryEntry">
        <tr>
            <td><c:out value="${dictionaryEntry.getLanguage().getName()}"/></td>
            <td><c:out value="${dictionaryEntry.getWord()}"/></td>
            <td><c:out value="${dictionaryEntry.getDefinition()}"/></td>
            <td><c:out value="${dictionaryEntry.getCreationTime(language)}"/></td>
        </tr>
    </c:forEach>
    </tbody>
</table>