<%@ tag import="services.DictionaryService" %>
<%@ attribute name="word" required="true" %>
<%@ attribute name="language" required="false" %>
<h5>
    Definition of <%= word %> is <i><%= DictionaryService.getDefinitionFor(word, language) %></i>
</h5>