package utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.ResourceBundle;

public class DateTimeUtil {

    private static final String US_DATE_TIME_FORMAT = "MM/dd/yyyy HH:mm";
    private static final String INTL_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private static String formatAs(LocalDateTime localDateTime, String pattern) {
        return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
    }

    public static String toUSDateTime(LocalDateTime localDateTime) {
        return formatAs(localDateTime, US_DATE_TIME_FORMAT);
    }

    public static String toINTLDateTime(LocalDateTime localDateTime) {
        return formatAs(localDateTime, INTL_DATE_TIME_FORMAT);
    }

    public static String formatDateTimeByLocale(LocalDateTime localDateTime, String locale) {
        ResourceBundle labels = ResourceBundle.getBundle("lab3package.Messages", Locale.forLanguageTag(locale));

        return formatAs(localDateTime, labels.getString("date_time_format"));
    }
}