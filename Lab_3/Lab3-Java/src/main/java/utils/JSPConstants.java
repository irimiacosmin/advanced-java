package utils;

public class JSPConstants {
    public static final String INPUT_OBJECT_PAGE = "input.jsp";
    public static final String INPUT_OBJECT_ERROR_LIST_KEY = "error_list";

    public static final String ERROR_OBJECT_KEY = "error";
    public static final String ERROR_OBJECT_PAGE = "error.jsp";

    public static final String RESULT_OBJECT_KEY = "dictionary";
    public static final String RESULT_OBJECT_PAGE = "result.jsp";

    public static final String CAPTCHA_OBJECT_KEY = "captcha";
    public static final String LOCALE_OBJECT_KEY = "locale";

    public static final String COOKIE_PREFERRED_LANGUAGE_NAME = "preferredLanguage";
    public static final Integer COOKIE_MAX_TIME_AGE = 10;

    public static final String CONTEXT_INIT_PARAM_CAPTCHA_DEFAULT_VALUE_KEY = "23";
}
