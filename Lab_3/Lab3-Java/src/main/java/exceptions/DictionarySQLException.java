package exceptions;

public class DictionarySQLException extends Exception {
    public String message;

    public DictionarySQLException(String message) {
        super(message);
    }
}
