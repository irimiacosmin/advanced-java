package filters;

import decorators.LanguageServletResponseWrapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

@WebFilter(urlPatterns = {"/LanguageServlet"})
public class ResponseDecoratorFilter extends HttpFilter {


    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        LanguageServletResponseWrapper languageServletRequestWrapper = new LanguageServletResponseWrapper(response);
        chain.doFilter(request, languageServletRequestWrapper);

        String enhancedContent = changeResponseColor(languageServletRequestWrapper.toString());
        PrintWriter printWriter = response.getWriter();
        printWriter.write(enhancedContent);
    }

    private String changeResponseColor(String responseContent) {
        String footer = "<h3 style=\"color: blue\">This is the best footer ever.</h3>";
        Document document = Jsoup.parse(responseContent);
        document.getElementsByTag("body").first().append(footer);
        return document.html();
    }
}
