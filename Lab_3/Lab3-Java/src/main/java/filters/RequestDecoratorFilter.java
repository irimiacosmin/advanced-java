package filters;

import decorators.LanguageServletRequestWrapper;
import utils.JSPConstants;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = {"/LanguageServlet"})
public class RequestDecoratorFilter extends HttpFilter {


    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String field = request.getParameter(JSPConstants.CAPTCHA_OBJECT_KEY);
        if (field == null || field.isEmpty() || field.isBlank()) {
            LanguageServletRequestWrapper languageServletRequestWrapper = new LanguageServletRequestWrapper(request);
            chain.doFilter(languageServletRequestWrapper, response);
        } else {
            chain.doFilter(request, response);
        }
    }
}