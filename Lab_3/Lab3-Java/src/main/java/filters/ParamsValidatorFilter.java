package filters;

import utils.HTMLConstants;
import utils.JSPConstants;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@WebFilter(urlPatterns = {"/LanguageServlet"})
public class ParamsValidatorFilter extends HttpFilter {


    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        List<String> errorMessages = languageServletRequestParamsAreInvalid(request);
        if (!errorMessages.isEmpty()) {
            String errorMessagesMappedForHTML = errorMessages.stream().map(this::mapErrorToHTML).collect(Collectors.joining("<br>"));
            request.setAttribute(JSPConstants.INPUT_OBJECT_ERROR_LIST_KEY, errorMessagesMappedForHTML);
            redirectRequestToPage(request, response, JSPConstants.INPUT_OBJECT_PAGE);
        } else {
            chain.doFilter(request, response);
        }
    }

    private String mapErrorToHTML(String error) {
        Object[] params = new Object[]{error};
        return MessageFormat.format(HTMLConstants.ERROR_H6_TEMPLATE, params);
    }


    private String getErrorFromField(String field, String fieldName) {
        return field == null || field.isEmpty() || field.isBlank() ? fieldName + " is invalid." : null;
    }

    private List<String> languageServletRequestParamsAreInvalid(HttpServletRequest request) {
        String language = request.getParameter("language");
        String word = request.getParameter("word");
        String definition = request.getParameter("definition");
        List<String> errorList = Arrays.asList(
                getErrorFromField(language, "Language"),
                getErrorFromField(word, "Word"),
                getErrorFromField(definition, "Definition")
        );
        return errorList.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    private void redirectRequestToPage(HttpServletRequest request, HttpServletResponse response, String pageName) {
        RequestDispatcher dispatcher = request.getRequestDispatcher(pageName);
        if (dispatcher != null) {
            try {
                dispatcher.forward(request, response);
            } catch (ServletException | IOException e) {
                e.printStackTrace();
            }
        }
    }

}
