package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.stream.Collectors;

@WebFilter(urlPatterns = {"/LanguageServlet"})
public class LoggingFilter implements Filter {

    System.Logger LOGGER = System.getLogger("LoggingFilter");

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (servletRequest instanceof HttpServletRequest) {
            logRequest((HttpServletRequest) servletRequest);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void logRequest(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        String params = request.getParameterMap()
                .entrySet()
                .stream()
                .map((entry) -> "[" + entry.getKey() + ":" + String.join(" ", entry.getValue()) + "]")
                .collect(Collectors.joining(", "));

        StringBuilder loggingBuilder = new StringBuilder();
        loggingBuilder.append(System.lineSeparator()).append("=".repeat(10)).append(" New request ").append("=".repeat(10));
        loggingBuilder.append(System.lineSeparator()).append("Method: ").append(request.getMethod());
        loggingBuilder.append(System.lineSeparator()).append("User-agent: ").append(request.getHeader("user-agent"));
        loggingBuilder.append(System.lineSeparator()).append("IP Address: ").append(ipAddress);
        loggingBuilder.append(System.lineSeparator()).append("Language: ").append(request.getHeader("Accept-Language"));
        loggingBuilder.append(System.lineSeparator()).append("Parameters: ").append(params);
        loggingBuilder.append(System.lineSeparator()).append("=".repeat(33));

        LOGGER.log(System.Logger.Level.INFO, loggingBuilder.toString());
    }
}
