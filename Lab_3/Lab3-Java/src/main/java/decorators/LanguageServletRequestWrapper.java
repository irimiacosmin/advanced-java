package decorators;

import utils.JSPConstants;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class LanguageServletRequestWrapper extends HttpServletRequestWrapper {

    public LanguageServletRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getParameter(String name) {
        if (name != null && name.equals(JSPConstants.CAPTCHA_OBJECT_KEY)) {
            return this.getServletContext().getInitParameter(JSPConstants.CONTEXT_INIT_PARAM_CAPTCHA_DEFAULT_VALUE_KEY);
        }
        return super.getParameter(name);
    }
}
