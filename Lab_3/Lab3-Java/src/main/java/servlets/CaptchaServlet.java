package servlets;

import services.CaptchaService;
import utils.JSPConstants;

import javax.imageio.ImageIO;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

@WebServlet(name = "CaptchaServlet", urlPatterns = "/CaptchaServlet")
public class CaptchaServlet extends HttpServlet {

    private static final Integer CAPTCHA_WINDOW_WIDTH = 200;
    private static final Integer CAPTCHA_WINDOW_HEIGHT = 50;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String captcha = CaptchaService.generateMathProblem(CaptchaService.getRandomBetween(2, 4));
        request.getSession().setAttribute(JSPConstants.CAPTCHA_OBJECT_KEY, captcha);

        BufferedImage bufferedImage = new BufferedImage(CAPTCHA_WINDOW_WIDTH, CAPTCHA_WINDOW_HEIGHT, BufferedImage.TYPE_INT_RGB);
        Font font = new Font(Font.SANS_SERIF, Font.BOLD, 18);

        RenderingHints renderingHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        renderingHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

        GradientPaint gradientPaint = new GradientPaint(0, 0, Color.BLUE, CAPTCHA_WINDOW_WIDTH / 16, 0, Color.DARK_GRAY, true);

        Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setFont(font);
        graphics2D.setRenderingHints(renderingHints);
        graphics2D.setPaint(gradientPaint);
        graphics2D.fillRect(0, 0, CAPTCHA_WINDOW_WIDTH, CAPTCHA_WINDOW_HEIGHT);
        graphics2D.setColor(Color.WHITE);

        int x = 0;
        int y = 0;

        char[] captchaToCharArray = captcha.toCharArray();

        for (int i = 0; i < captcha.length(); ++i) {
            x += 10 + CaptchaService.getRandomBetween(0, 15);
            y = 20 + CaptchaService.getRandomBetween(0, 20);
            graphics2D.drawChars(captchaToCharArray, i, 1, x, y);
        }

        graphics2D.dispose();

        response.setContentType("image/png");

        OutputStream outputStream = response.getOutputStream();
        ImageIO.write(bufferedImage, "png", outputStream);
        outputStream.close();
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        processRequest(request, response);
    }
}
