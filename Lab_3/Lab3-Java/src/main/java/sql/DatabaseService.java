package sql;


import utils.SQLConstants;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseService {


    private static Connection connection;

    private static Connection getConnection() {
        try {
            Class.forName(SQLConstants.DATABASE_DRIVER);
            return DriverManager.getConnection(SQLConstants.CONNECTION_URL, SQLConstants.USER, SQLConstants.PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            return null;
        }
    }

    public static Connection getConnectionInstance() {
        if (connection == null) {
            connection = getConnection();
        }
        return connection;
    }
}
