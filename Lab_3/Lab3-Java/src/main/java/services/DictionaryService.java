package services;

import exceptions.DictionarySQLException;
import exceptions.DuplicateEntryException;
import lombok.NoArgsConstructor;
import models.DictionaryEntry;
import persistance.DictionaryRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@NoArgsConstructor
public class DictionaryService {

    public void createEntry(DictionaryEntry dictionaryEntry) throws DuplicateEntryException, DictionarySQLException {
        DictionaryRepository.add(dictionaryEntry);
    }

    public List<String> getDictionaryAsTableRows() throws DictionarySQLException {
        return getDictionary().stream()
                .map(DictionaryEntry::getAsTableRow)
                .collect(Collectors.toList());
    }

    public static List<DictionaryEntry> getDictionary() throws DictionarySQLException {
        return DictionaryRepository.getAll();
    }

    public static String getDefinitionFor(String word, String language) throws DictionarySQLException {
        Optional<DictionaryEntry> entry = DictionaryRepository.getAll().stream()
                .filter((dictionaryEntry) -> entryMatches(dictionaryEntry, word, language))
                .findFirst();
        return entry.map(DictionaryEntry::getDefinition).orElse(null);
    }

    private static boolean entryMatches(DictionaryEntry dictionaryEntry, String word, String language) {
        boolean wordMatches = dictionaryEntry.getWord().toLowerCase().equals(word.toLowerCase());
        if (wordMatches && language != null) {
            return dictionaryEntry.getLanguage().getShortVersion().toLowerCase().equals(language.toLowerCase());
        }
        return wordMatches;
    }
}