package services;

import exceptions.InvalidCaptchaException;
import lombok.NoArgsConstructor;
import utils.JSPConstants;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@NoArgsConstructor
public class CaptchaService {

    private static final List<String> basicOperations = new ArrayList<>() {
        {
            add("+");
            add("-");
        }
    };

    public static void verifyCaptcha(HttpServletRequest request) throws InvalidCaptchaException {
        try {
            String captcha = (String) request.getSession().getAttribute(JSPConstants.CAPTCHA_OBJECT_KEY);
            String typedCaptcha = request.getParameter(JSPConstants.CAPTCHA_OBJECT_KEY);
            if (!computeCaptcha(captcha).equals(Integer.parseInt(typedCaptcha))) {
                throw new InvalidCaptchaException();
            }
        } catch (Exception e) {
            throw new InvalidCaptchaException();
        }
    }

    public static Integer getRandomBetween(Integer min, Integer max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    public static List<String> splitStringIntoLowercaseChars(String word) {
        if (word == null) {
            return new ArrayList<>();
        }
        return new ArrayList<>(Arrays.asList(word.toLowerCase().split("(?!^)")));
    }

    public static String generateMathProblem(int length) {
        List<Integer> randomNumbers = IntStream.rangeClosed(0, length)
                .boxed()
                .map((number) -> getRandomBetween(1, 10))
                .collect(Collectors.toList());

        List<String> randomOperations = IntStream.rangeClosed(0, length - 1)
                .boxed()
                .map((number) -> basicOperations.get(getRandomBetween(0, basicOperations.size())))
                .collect(Collectors.toList());

        StringBuilder randomProblem = new StringBuilder();

        for (int i = 0; i < randomOperations.size(); i++) {
            randomProblem.append(randomNumbers.get(i).toString());
            randomProblem.append(randomOperations.get(i));
        }

        randomProblem.append(randomNumbers.get(randomNumbers.size() - 1).toString());
        return randomProblem.toString();
    }

    public static Integer computeCaptcha(String captcha) throws InvalidCaptchaException {
        try {
            List<String> captchaCharacters = splitStringIntoLowercaseChars(captcha);
            int result = Integer.parseInt(captchaCharacters.get(0));
            for (int i = 2; i < captchaCharacters.size(); i++) {
                if (i % 2 == 0) {
                    int currentNumber = Integer.parseInt(captchaCharacters.get(i));
                    switch (captchaCharacters.get(i - 1)) {
                        case "+" -> result += currentNumber;
                        case "-" -> result -= currentNumber;
                    }
                }
            }
            return result;
        } catch (Exception e) {
            throw new InvalidCaptchaException();
        }
    }
}
