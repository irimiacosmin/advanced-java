package persistance;

import exceptions.DictionarySQLException;
import exceptions.DuplicateEntryException;
import models.DictionaryEntry;
import sql.DatabaseService;
import utils.SQLConstants;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class DictionaryRepository {

    public static void add(DictionaryEntry dictionaryEntry) throws DuplicateEntryException, DictionarySQLException {
        try {
            Statement statement = DatabaseService.getConnectionInstance().createStatement();
            String insertStatement = SQLConstants.INSERT(dictionaryEntry);
            statement.executeUpdate(insertStatement);
        } catch (SQLException sqlException) {
            if (sqlException.getSQLState().equals(SQLConstants.DUPLICATE_KEY_CODE)) {
                throw new DuplicateEntryException();
            }
            throw new DictionarySQLException(sqlException.getMessage());
        }
    }

    public static List<DictionaryEntry> getAll() throws DictionarySQLException {
        List<DictionaryEntry> entries = new ArrayList<>();
        try {
            Statement statement = DatabaseService.getConnectionInstance().createStatement();
            ResultSet resultSet = statement.executeQuery(SQLConstants.SELECT_ALL_QUERY);
            while (resultSet.next()) {
                String language_short = resultSet.getString("language_short");
                String word = resultSet.getString("word");
                String definition = resultSet.getString("definition");
                LocalDateTime creationTime = resultSet.getObject("timestamp", LocalDateTime.class);
                entries.add(new DictionaryEntry(language_short, word, definition, creationTime));
            }
            resultSet.close();
        } catch (Exception exception) {
            throw new DictionarySQLException(exception.getMessage());
        }
        return entries;
    }
}
