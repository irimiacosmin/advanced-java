package models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import utils.HTMLConstants;
import utils.JSPConstants;

import javax.servlet.http.Cookie;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
public enum Language {
    EN("en", "English"),
    RO("ro", "Romanian");

    private final String shortVersion;
    private final String name;

    public static Language getValueByShortVersion(String shortVersion) {
        return Arrays.stream(Language.values())
                .filter(value -> value.getShortVersion().equals(shortVersion))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Language <" + shortVersion + "> does not exist."));
    }

    public static List<String> getAllAsSelectOptions() {
        return Arrays.stream(Language.values())
                .map(Language::getLanguageAsOptionFormatted)
                .collect(Collectors.toList());
    }

    public static List<String> getAllAsSelectOptions(Cookie[] cookies) {
        List<Language> languages = Arrays.stream(Language.values())
                .collect(Collectors.toList());

        Optional<Cookie> preferredLanguageCookie = Arrays.stream(cookies)
                .filter(cookie -> cookie.getName().equals(JSPConstants.COOKIE_PREFERRED_LANGUAGE_NAME))
                .findAny();

        if (preferredLanguageCookie.isPresent()) {
            Language priorityLanguage = Language.getValueByShortVersion(preferredLanguageCookie.get().getValue());
            Collections.swap(languages, 0, languages.indexOf(priorityLanguage));
        }

        return languages.stream()
                .map(Language::getLanguageAsOptionFormatted)
                .collect(Collectors.toList());
    }

    private static String getLanguageAsOptionFormatted(Language language) {
        Object[] params = new Object[]{language.getShortVersion(), language.getName()};
        return MessageFormat.format(HTMLConstants.LANGUAGE_OPTION_TEMPLATE, params);
    }
}
