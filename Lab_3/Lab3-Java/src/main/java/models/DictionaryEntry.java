package models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import utils.DateTimeUtil;
import utils.HTMLConstants;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.time.LocalDateTime;

@Getter
@EqualsAndHashCode
public class DictionaryEntry {

    private Language language;
    private String word;
    private String definition;

    private LocalDateTime creationTime;
    private Boolean safe;

    public DictionaryEntry(String languageShortVersion, String word, String definition, LocalDateTime creationTime) {
        try {
            this.language = Language.getValueByShortVersion(languageShortVersion);
            this.word = word;
            this.definition = definition;
            this.creationTime = creationTime;
            this.safe = true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            this.safe = false;
        }
    }

    public DictionaryEntry(HttpServletRequest req) {
        this(
                req.getParameter("language"),
                req.getParameter("word"),
                req.getParameter("definition"),
                null
        );
    }

    public String getCreationTime(String locale) {
        return DateTimeUtil.formatDateTimeByLocale(this.creationTime, locale);
    }

    public Boolean isSafe() {
        return safe;
    }

    public String getAsTableRow() {
        if (!this.isSafe()) {
            return "";
        }
        Object[] params = new Object[]{this.getLanguage().getName(), this.getWord(), this.getDefinition()};
        return MessageFormat.format(HTMLConstants.TABLE_ROW_TEMPLATE, params);
    }
}