# Java Technologies - Lab 7
#
#
#
#### Point 1 (2p)

- Rewrite the data access layer of the application created in the previous laboratories, implementing the repository classes as Enterprise Java Beans.
    * Use the support offered by the EJB technology for implementing transactions.
    * Suppose that each meeting has a limit regarding the number of persons who can participate. Create a "reservation" page, allowing a group of persons to subscribe to a meeting, either all of them or none.
    * The following enterprise beans must be implemented:
        * A **stateless session bean** that offers methods for checking the availability of a meeting.
        * A **stateful session bean** responsible with the assignment of one or more persons to a specific meeting. The assignment should be atomic, either all persons are successfully assigned, or the transaction will be rolled back.
        * A **singleton session bean** that keeps an in-memory map of the current assignments. The map will be instantiated at application startup and updated whenever the assignments change.
    
#### Point 2 (1p)

- Create a test case that highlights the performance gain of using the JPA second level cache (or other forms of optimizations).
- Use an EJB interceptor in order to monitor the running time of a business method.
- Create a timer that will trigger the invocation of the business method, using a specified schedule.

#
#
#
#### Bibliography
- [Lab slides](https://profs.info.uaic.ro/~acf/tj/labs/pdf/lab7.pdf)
- [Java EE Tutorial: Enterprise Beans](https://docs.oracle.com/javaee/7/tutorial/partentbeans.htm#BNBLR)
- [Mastering Enterprise JavaBeans](http://media.techtarget.com/tss/static/books/wiley/masteringEJB/downloads/MasteringEJB3rdEd.pdf)
- [GlassFish Server High Availability Administration Guide](https://docs.oracle.com/cd/E26576_01/doc.312/e24934/overview.htm#GSHAG00002)
- [Java Persistence Performance](http://java-persistence-performance.blogspot.com/2011/06/how-to-improve-jpa-performance-by-1825.html)