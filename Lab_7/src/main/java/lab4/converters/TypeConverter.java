package lab4.converters;

import lab4.entities.MeetingTypeEntity;
import lab4.exceptions.DuplicateEntryException;
import lab4.exceptions.TooShortException;
import lab4.models.Location;
import lab4.persistance.LocationRepository;
import lab4.persistance.MeetingTypeRepository;
import lab4.services.MeetingService;
import lombok.SneakyThrows;

import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.sql.SQLException;


@FacesConverter("lab4.converters.TypeConverter")
public class TypeConverter implements Converter<MeetingTypeEntity> {

    private final MeetingService meetingService;

    public TypeConverter() throws NamingException {
        this.meetingService = CDI.current().select(MeetingService.class).get();
    }

    @Override
    public MeetingTypeEntity getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return meetingService.getAllMeetingTypes().stream()
                .filter(meetingType -> meetingType.toString().equals(s))
                .findFirst()
                .orElse(null);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, MeetingTypeEntity meetingTypeEntity) {
        return meetingTypeEntity.toString();
    }
}
