package lab4.converters;

import lab4.exceptions.DuplicateEntryException;
import lab4.exceptions.TooShortException;
import lab4.models.Location;
import lab4.persistance.LocationRepository;
import lab4.services.LocationService;
import lombok.SneakyThrows;

import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.sql.SQLException;


@FacesConverter("lab4.converters.LocationConverter")
public class LocationConverter implements Converter<Location> {

    private final LocationService locationService;

    public LocationConverter() {
        this.locationService = CDI.current().select(LocationService.class).get();
    }

    @SneakyThrows
    @Override
    public Location getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if (s == null || s.isEmpty()) {
            throw new TooShortException(new FacesMessage("Location name is too short."));
        }
        Location newLocation = new Location(s);
        int newLocationId = -1;
        try {
            newLocationId = locationService.save(newLocation);
        } catch (SQLException | DuplicateEntryException e) {
            e.printStackTrace();
        }
        newLocation.setId(newLocationId);
        return newLocation;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Location location) {
        return location.toString();
    }
}
