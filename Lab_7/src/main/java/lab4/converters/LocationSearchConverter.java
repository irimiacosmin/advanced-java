package lab4.converters;

import lab4.exceptions.DuplicateEntryException;
import lab4.exceptions.TooShortException;
import lab4.models.Location;
import lab4.persistance.LocationRepository;
import lab4.services.LocationService;
import lombok.SneakyThrows;

import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.sql.SQLException;


@FacesConverter("lab4.converters.LocationSearchConverter")
public class LocationSearchConverter implements Converter<Location> {

    private final LocationService locationService;

    public LocationSearchConverter() {
        this.locationService = CDI.current().select(LocationService.class).get();

    }
    @SneakyThrows
    @Override
    public Location getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return locationService.getAll()
                .stream()
                .filter(location -> location.toString().equals(s))
                .findFirst()
                .orElse(null);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Location location) {
        return location.toString();
    }
}
