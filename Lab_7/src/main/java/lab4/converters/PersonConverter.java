package lab4.converters;

import lab4.exceptions.DuplicateEntryException;
import lab4.exceptions.TooShortException;
import lab4.models.Location;
import lab4.models.Person;
import lab4.services.LocationService;
import lab4.services.PersonService;
import lombok.SneakyThrows;

import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.sql.SQLException;


@FacesConverter("lab4.converters.PersonConverter")
public class PersonConverter implements Converter<Person> {

    private final PersonService personService;

    public PersonConverter() {
        this.personService = CDI.current().select(PersonService.class).get();
    }


    @Override
    public Person getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return personService.getAll().stream()
                .filter(person -> person.toString().equals(s))
                .findFirst()
                .orElse(null);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Person person) {
        return person.toString();
    }
}
