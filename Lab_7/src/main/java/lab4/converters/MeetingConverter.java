package lab4.converters;

import lab4.models.Meeting;
import lab4.services.MeetingService;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;


@FacesConverter("lab4.converters.MeetingConverter")
public class MeetingConverter implements Converter<Meeting> {

    private final MeetingService MeetingService;

    public MeetingConverter() {
        this.MeetingService = CDI.current().select(MeetingService.class).get();
    }

    @Override
    public Meeting getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return MeetingService.getAll().stream()
                .filter(meeting -> meeting.toString().equals(s))
                .findFirst()
                .orElse(null);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Meeting Meeting) {
        return Meeting.toString();
    }
}
