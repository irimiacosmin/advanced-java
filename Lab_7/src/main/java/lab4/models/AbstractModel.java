package lab4.models;

import javax.persistence.Entity;

public interface AbstractModel<T, E> {
    E toEntity();
}