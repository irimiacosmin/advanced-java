package lab4.models;

import lab4.entities.PersonEntity;
import lab4.services.PersonService;
import lombok.*;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Named(value = "personBean")
@RequestScoped
@EqualsAndHashCode
public class Person implements Serializable, AbstractModel<Person, PersonEntity> {

    private Integer id;

    @Size(min = 3)
    private String name;

    @Email
    private String email;

    @Inject
    private PersonService personService;

    public Person(Integer id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public Person(PersonEntity personEntity) {
        this.id = personEntity.getId();
        this.name = personEntity.getName();
        this.email = personEntity.getEmail();
    }

    public void reset() {
        this.id = null;
        this.name = null;
        this.email = null;
    }

    public void submit() throws Exception {
        personService.save(this);

        FacesContext facesContext = FacesContext.getCurrentInstance();
        NavigationHandler navigation = facesContext.getApplication().getNavigationHandler();
        navigation.handleNavigation(facesContext, null, "index");
    }

    @Override
    public PersonEntity toEntity() {
        return PersonEntity.builder()
                .id(this.id)
                .name(this.name)
                .email(this.email)
                .build();
    }
}
