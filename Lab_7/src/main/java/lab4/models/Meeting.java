package lab4.models;

import lab4.entities.*;
import lab4.validators.MeetingIsValid;
import lombok.*;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Named(value = "meetingBean")
@RequestScoped
@EqualsAndHashCode
@MeetingIsValid
public class Meeting implements AbstractModel<Meeting, MeetingEntity> {

    private Integer id;
    private Topic topic;
    private LocalDateTime startingDateTime;
    private Integer durationMinutes;
    private Location location;
    private MeetingTypeEntity type;
    private Integer meetingLimit;
    private List<Person> attendees;

    public Meeting(Integer id, Topic topic, LocalDateTime startingDateTime, Integer durationMinutes, Location location,
                   MeetingTypeEntity type, Integer meetingLimit) {
        this.id = id;
        this.topic = topic;
        this.startingDateTime = startingDateTime;
        this.durationMinutes = durationMinutes;
        this.location = location;
        this.type = type;
        this.meetingLimit = meetingLimit;
    }

    public Meeting(MeetingEntity meetingEntity, List<Person> allByMeetingId) {
        this.id = meetingEntity.getId();
        this.topic = meetingEntity.getTopic();
        this.startingDateTime = meetingEntity.getStartingDateTime();
        this.durationMinutes = meetingEntity.getDurationMinutes();
        this.location = new Location(meetingEntity.getLocation());
        this.type = meetingEntity.getType();
        this.meetingLimit = meetingEntity.getMeetingLimit();
        this.attendees = allByMeetingId;
    }

    public void addPerson(Person person) {
        if (attendees == null) {
            attendees = new ArrayList<>();
        }
        attendees.add(person);
    }

    public List<Topic> getTopics() {
        return Arrays.asList(Topic.values());
    }

    public void reset() {
        this.id = null;
        this.topic = null;
        this.startingDateTime = null;
        this.durationMinutes = null;
        this.location = null;
        this.attendees = null;
    }

    public LocalDateTime getStartingDateTime() {
        return startingDateTime == null ? LocalDateTime.now() : startingDateTime;
    }

    public String getGeneralDetails() {
        return location.getName() + ", " + topic + " (" + durationMinutes + " min)";
    }

    public String getAttendeesDetails() {
        String attendeesListAsString = attendees.stream().map(Person::getName).collect(Collectors.joining(", "));
        return "Attendees: [" + attendeesListAsString + "]";
    }

    public String getTypeDetails() {
        MeetingTypeEntity type = this.getType();
        MeetingTypeAdditionalData additionalData = type.getAdditionalData();

        String toBeReturned = "Type " + type.getType().toUpperCase() + " Resources: [";
        toBeReturned += additionalData.getResources()
                .stream()
                .map(ResourceEntity::getName)
                .collect(Collectors.joining(", "));
        toBeReturned += "] Speakers: [";
        toBeReturned += additionalData.getSpeakers()
                .stream()
                .map(speaker -> "{" + speaker.getName() + " | " + speaker.getExpertise() + "}")
                .collect(Collectors.joining(", "));
        toBeReturned += "]";
        return toBeReturned;
    }

    @Override
    public MeetingEntity toEntity() {
        return MeetingEntity.builder()
                .id(this.getId())
                .topic(this.getTopic())
                .startingDateTime(this.getStartingDateTime())
                .durationMinutes(this.getDurationMinutes())
                .location(this.location.toEntity())
                .type(this.type)
                .meetingLimit(this.meetingLimit)
                .build();
    }
}
