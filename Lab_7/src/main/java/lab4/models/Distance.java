package lab4.models;

import lab4.entities.DistanceEntity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.Min;
import java.io.Serializable;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Named(value = "distanceBean")
@RequestScoped
public class Distance implements Serializable, AbstractModel<Distance, DistanceEntity> {
    private Integer id;
    private Location firstLocation;
    private Location secondLocation;

    @Min(value = 0)
    private Integer durationMinutes;

    public Distance(Location firstLocation, Location secondLocation, Integer durationMinutes) {
        this.firstLocation = firstLocation;
        this.secondLocation = secondLocation;
        this.durationMinutes = durationMinutes;
    }

    public Distance(DistanceEntity entity) {
        this.id = entity.getId();
        this.firstLocation = new Location(entity.getFirstLocation());
        this.secondLocation = new Location(entity.getSecondLocation());
        this.durationMinutes = entity.getDurationMinutes();
    }

    public String getGeneralDetails() {
        return firstLocation.getName() + " -> " + secondLocation.getName() + " takes " + durationMinutes + " minutes.";
    }

    @Override
    public DistanceEntity toEntity() {
        return DistanceEntity.builder()
                .id(this.getId())
                .firstLocation(this.getFirstLocation().toEntity())
                .secondLocation(this.getSecondLocation().toEntity())
                .durationMinutes(this.getDurationMinutes())
                .build();
    }
}