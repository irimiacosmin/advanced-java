package lab4.utils;

import lab4.models.Distance;
import lab4.models.Location;
import lab4.models.Meeting;
import lab4.models.Person;

import java.text.MessageFormat;

public class JMSConstants {
    /*
            asadmin create-jms-resource --restype javax.jms.Topic --property Name=DOCUMENTS Documents
            https://rieckpil.de/howto-messaging-with-jms-using-payara-with-embedded-openmq-broker/
     */

    public static final String MESSAGE_DRIVEN_SOURCE = "stockmdb";
    public static final String JMS_QUEUE = "jms/stocks";
    public static final String JMS_FACTORY = "jms/__defaultConnectionFactory";

    public static final String JMS_PERSON_REGISTER_CODE = "PERSON_REGISTER";
    public static final String JMS_PERSON_REGISTER_MESSAGE = "Person named {0} with email {1} wants to participate to meeting {2}.";

    public static String register(Integer meetingId, Person person) {
        return getFilledQuery(JMS_PERSON_REGISTER_MESSAGE, person.getName(), person.getEmail(), meetingId);
    }

    public static String getFilledQuery(String query, Object... params) {
        return MessageFormat.format(query, params);
    }
}
