package lab4.utils;

public class MessagesConstants {

    public static final String DUPLICATE_ENTRY_MESSAGE = "Duplicate entry!";
    public static final String ENTRY_NOT_FOUND_MESSAGE = "Entry not found.";
    public static final String TOO_SHORT_MESSAGE = "Too short.";
}
