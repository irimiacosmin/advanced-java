package lab4.entities;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class ResourceEntity implements JsonbDataEntity {

    Integer id;
    String name;
}
