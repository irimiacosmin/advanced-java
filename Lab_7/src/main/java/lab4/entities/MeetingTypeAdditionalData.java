package lab4.entities;

import lombok.*;

import javax.persistence.Entity;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class MeetingTypeAdditionalData implements JsonbDataEntity {
    List<SpeakerEntity> speakers;
    List<ResourceEntity> resources;
}
