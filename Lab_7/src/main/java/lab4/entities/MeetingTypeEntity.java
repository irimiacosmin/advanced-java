package lab4.entities;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
@TypeDefs({
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
@Table(name = "meeting_type", schema = "advanced_java")
public class MeetingTypeEntity implements AbstractEntity<Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "type")
    private String type;

    @Type(type = "jsonb")
    @Column(name = "additional_data", columnDefinition = "jsonb")
    @Basic(fetch = FetchType.EAGER)
    private MeetingTypeAdditionalData additionalData;

    @Override
    public String toString() {
        return "MeetingTypeEntity{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }
}
