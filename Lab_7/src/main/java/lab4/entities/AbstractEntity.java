package lab4.entities;

public interface AbstractEntity<ID> {
    ID getId();
}