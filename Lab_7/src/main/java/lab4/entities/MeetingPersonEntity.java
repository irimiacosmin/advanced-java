package lab4.entities;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Table(name = "meeting_person", schema = "advanced_java")
public class MeetingPersonEntity implements AbstractEntity<Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JoinColumn(name = "meeting_id", nullable = false)
    @OneToOne(fetch = FetchType.EAGER)
    private MeetingEntity meeting;

    @JoinColumn(name = "person_id", nullable = false)
    @OneToOne(fetch = FetchType.EAGER)
    private PersonEntity person;
}
