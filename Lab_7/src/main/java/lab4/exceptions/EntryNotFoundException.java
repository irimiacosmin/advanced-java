package lab4.exceptions;

import lab4.utils.MessagesConstants;

public class EntryNotFoundException extends Exception {
    public String message;

    public EntryNotFoundException(String message) {
        super(message);
    }

    public EntryNotFoundException() {
        super(MessagesConstants.ENTRY_NOT_FOUND_MESSAGE);
    }
}
