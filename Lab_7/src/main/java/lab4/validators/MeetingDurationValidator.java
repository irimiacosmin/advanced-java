package lab4.validators;

import lab4.exceptions.TooShortException;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator(value = "MeetingDurationValidator")
public class MeetingDurationValidator implements Validator<Integer> {

    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Integer value) throws ValidatorException {
        if (value < 5) {
            throw new TooShortException(new FacesMessage("Meeting time is too short."));
        }
    }
}
