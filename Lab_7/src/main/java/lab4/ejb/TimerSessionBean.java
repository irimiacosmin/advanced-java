package lab4.ejb;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

import javax.ejb.Schedule;
import javax.ejb.Stateless;

@Stateless
public class TimerSessionBean {

    @Schedule(second = "*/1", minute = "*", hour = "*", persistent = false)
    public void doWork() {
//        if (!PerformanceTests.CACHE_MODE) {
//            CacheManager.ALL_CACHE_MANAGERS.stream()
//                    .map(cacheManager -> cacheManager.getCache("lab4.entities.PersonEntity"))
//                    .forEach(Cache::removeAll);
//        }
    }
}
