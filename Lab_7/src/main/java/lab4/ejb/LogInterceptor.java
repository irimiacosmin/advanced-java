package lab4.ejb;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import java.util.ArrayList;
import java.util.List;

public class LogInterceptor {

    public static List<String> logs = new ArrayList<>();

    @AroundInvoke
    public Object log(InvocationContext invocationContext) throws Exception {
        String className = invocationContext.getTarget().getClass().getName();
        String methodName = invocationContext.getMethod().getName();
        String target = className + "." + methodName + "()";
        long startTime = System.currentTimeMillis();
        try {
            return invocationContext.proceed();
        } catch (Exception e ) {
            throw e;
        } finally {
            long endTime = System.currentTimeMillis();
            logs.add(target + " took " + (endTime - startTime) + "ms to execute");
        }
    }
}