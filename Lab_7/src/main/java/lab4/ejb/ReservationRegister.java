package lab4.ejb;

import lab4.ejb.messaging.ReservationMessagePublisher;
import lab4.exceptions.EntryNotFoundException;
import lab4.models.Meeting;
import lab4.models.Person;
import lab4.persistance.MeetingRepository;
import lab4.utils.JMSConstants;
import lombok.Getter;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
@Startup
@Getter
public class ReservationRegister {

    @EJB
    private MeetingRepository meetingRepository;

    @EJB
    private ReservationMessagePublisher reservationMessagePublisher;

    private Map<Meeting, List<Person>> personsMappedByMeeting;

    @PostConstruct
    private void init() {
        personsMappedByMeeting = new HashMap<>();
    }

    public void registerPerson(Meeting meeting, Person person) throws EntryNotFoundException {
        if (meeting == null || meeting.getId() == null || person == null) {
            return;
        }
        registerPerson(meeting.getId(), person);
    }

    public void registerPerson(Integer meetingId, Person person) throws EntryNotFoundException {
        if (meetingId == null || person == null) {
            return;
        }
        Meeting meeting = meetingRepository.getById(meetingId);
        List<Person> personList = personsMappedByMeeting.get(meeting);
        if (personList == null) {
            personList = new ArrayList<>();
        }
        Integer meetingLimit = meeting.getMeetingLimit();
        if (meetingLimit != null && meetingLimit > personList.size()) {
            personList.add(person);
            personsMappedByMeeting.put(meeting, personList);
        }
        reservationMessagePublisher.sendMessageReservation(JMSConstants.JMS_PERSON_REGISTER_CODE, JMSConstants.register(meetingId, person));
    }


    public void reset() {
        personsMappedByMeeting = new HashMap<>();
    }
}
