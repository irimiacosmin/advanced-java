package lab4.ejb;

import lab4.entities.PersonEntity;
import lab4.persistance.PersonRepository;
import lab4.utils.SQLConstants;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

@Named(value = "PerformanceTestBean")
@RequestScoped
public class PerformanceTests {

    @PersistenceContext(name = SQLConstants.PERSISTENCE_UNIT_NAME)
    private EntityManager manager;

    private List<String> logs;

    public static boolean CACHE_MODE = true;

    @EJB
    private PersonRepository personRepository;

    public List<String> getCachePerformance() throws Exception {
        logs = new ArrayList<>();
        LogInterceptor.logs = new ArrayList<>();
        logs.add("\n\n => Analyze CACHE Performance");
        CACHE_MODE = true;
        analyzePerformance();
        return logs;
    }

    public List<String> getNoCachePerformance() throws Exception {
        logs = new ArrayList<>();
        LogInterceptor.logs = new ArrayList<>();
        logs.add("\n\n => Analyze NO_CACHE Performance");
        CACHE_MODE = false;
        analyzePerformance();
        return logs;
    }

    private void analyzePerformance() throws Exception {
        int existingPersonsSize = personRepository.getAll().size();
        Map<Integer, PersonEntity> personEntityMap = new HashMap<>();
        int howMany = 10;

        for (int i = existingPersonsSize; i < existingPersonsSize + howMany; i++) {
            PersonEntity personEntity = new PersonEntity(null, "Irimia Test#" + i, "irimia.cosmin" + i + "@yahoo.com");
            int id = personRepository.add(personEntity);
            personEntityMap.put(id, personEntity);
            logs.add("Created entity #" + i);
        }
        logCacheSize();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            logs.add("\nStarting getting entity #" + i + " for 100 times in a row and the get all: ");
            if (!CACHE_MODE) {
                CacheManager.ALL_CACHE_MANAGERS.stream()
                        .map(cacheManager -> cacheManager.getCache("lab4.entities.PersonEntity"))
                        .filter(Objects::nonNull)
                        .forEach(Cache::removeAll);
            }
            personEntityMap.keySet().forEach(id -> {
                personRepository.getById(id);
            });
            if (!CACHE_MODE) {
                CacheManager.ALL_CACHE_MANAGERS.stream()
                        .map(cacheManager -> cacheManager.getCache("lab4.entities.PersonEntity"))
                        .filter(Objects::nonNull)
                        .forEach(Cache::removeAll);
            }
            personRepository.getAll();
        }
        long endTime = System.currentTimeMillis();
        logs.addAll(LogInterceptor.logs);
        logs.add("Performance test took " + (endTime - startTime) + "ms to execute");

        personEntityMap.values().forEach(personEntity -> personRepository.delete(personEntity));

        CacheManager.getInstance().clearAll();
    }

    private void logCacheSize() {
        if (!CACHE_MODE) {
            CacheManager.ALL_CACHE_MANAGERS.stream()
                    .map(cacheManager -> cacheManager.getCache("lab4.entities.PersonEntity"))
                    .filter(Objects::nonNull)
                    .forEach(Cache::removeAll);
        }
        try {
            logs.add("Cache size: " + CacheManager.ALL_CACHE_MANAGERS.get(0).getCache("lab4.entities.PersonEntity").getSize());
        } catch (NullPointerException nullPointerException) {
            logs.add("Cache does not exist. ");
        }
    }
}
