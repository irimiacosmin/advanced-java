package lab4.ejb;


import lab4.exceptions.EntryNotFoundException;
import lab4.models.Meeting;
import lab4.models.Person;
import lab4.persistance.MeetingRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Named(value = "reservationBean")
@RequestScoped
public class ReservationBean {

    private Person person;
    private Meeting meeting;

    @EJB
    private ReservationAssigner reservationAssigner;

    @EJB
    private ReservationChecker reservationChecker;

    @EJB
    private ReservationRegister reservationRegister;

    public String getMeetingAvailability(Object meeting) throws EntryNotFoundException {
        if (meeting instanceof Meeting) {
            Meeting toWorkMeeting = (Meeting) meeting;
            return getMeetingAvailability(toWorkMeeting);
        }
        return " is fully booked.";
    }

    public String getMeetingAvailability(Meeting meeting) throws EntryNotFoundException {
        return reservationChecker.meetingIsAvailable(meeting) ? " still has empty spaces." : " is fully booked.";
    }

    public String getMeetingAvailability() throws EntryNotFoundException {
        return getMeetingAvailability(this.meeting);
    }

    public void register() throws EntryNotFoundException {
        reservationRegister.registerPerson(meeting, person);
    }

    public void finish() throws Exception {
        reservationAssigner.save();
    }
}
