package lab4.ejb.messaging;

import lombok.*;

import javax.json.JsonObject;
import javax.persistence.*;
import java.time.Instant;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Table(name = "meeting_reservation_message", schema = "advanced_java")
public class MeetingReservationMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String code;

    @Column(nullable = false)
    private String message;

    @Column(nullable = false)
    private Instant timestamp;

    public MeetingReservationMessage(JsonObject json) {
        this.code = json.getString("code");
        this.message = json.getString("message");
        this.timestamp = Instant.ofEpochMilli(json.getJsonNumber("timestamp").longValue());
    }
}
