package lab4.ejb;

import lab4.ejb.messaging.ReservationMessagePublisher;
import lab4.entities.MeetingPersonEntity;
import lab4.models.Meeting;
import lab4.models.Person;
import lab4.persistance.MeetingRepository;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Stateful
@LocalBean
public class ReservationAssigner {

    @EJB
    private ReservationRegister reservationRegister;

    @EJB
    private MeetingRepository meetingRepository;

    @Remove
    public void save() throws Exception {
        Map<Meeting, List<Person>> personsMappedByMeeting = reservationRegister.getPersonsMappedByMeeting();
        List<Meeting> meetingsToBeSaved = new ArrayList<>(personsMappedByMeeting.keySet());
        meetingsToBeSaved.forEach(meeting -> personsMappedByMeeting.get(meeting).forEach(meeting::addPerson));
        for (Meeting meeting: meetingsToBeSaved) {
            meetingRepository.assignPersonToMeeting(meeting);
        }
        reservationRegister.reset();
    }

}
