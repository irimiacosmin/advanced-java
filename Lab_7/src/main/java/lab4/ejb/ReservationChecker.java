package lab4.ejb;

import lab4.exceptions.EntryNotFoundException;
import lab4.models.Meeting;
import lab4.models.Person;
import lab4.persistance.MeetingRepository;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
@LocalBean
public class ReservationChecker {

    @EJB
    private ReservationRegister reservationRegister;

    @EJB
    private MeetingRepository meetingRepository;

    public Boolean meetingIsAvailable(Meeting meeting) throws EntryNotFoundException {
        if (meeting == null || meeting.getId() == null) {
            return false;
        }
        return meetingIsAvailable(meeting.getId());
    }

    public Boolean meetingIsAvailable(Integer meetingId) throws EntryNotFoundException {
        if (meetingId == null) {
            return false;
        }
        Meeting meeting = meetingRepository.getById(meetingId);
        List<Person> personList = reservationRegister.getPersonsMappedByMeeting().get(meeting);
        if (personList == null) {
            return true;
        }
        return meeting.getMeetingLimit() > personList.size();
    }
}
