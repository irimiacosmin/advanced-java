package lab4.services;

import lab4.exceptions.EntryNotFoundException;
import lab4.models.Distance;
import lab4.models.Location;
import lab4.persistance.DistanceRepository;
import lombok.Getter;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Named(value = "DistanceService")
@SessionScoped
@Getter
public class DistanceService implements Serializable {

    private final Map<Location, Integer> distancesMap = new HashMap<>();

    @EJB
    private DistanceRepository distanceRepository;

    public void saveDistances(Location firstLocation) {
        for (Map.Entry<Location, Integer> entry : distancesMap.entrySet()) {
            try {
                Integer dist = entry.getValue();
                Distance distance = new Distance(firstLocation, entry.getKey(), dist);
                Distance reverseDistance = new Distance(entry.getKey(), firstLocation, dist);
                distanceRepository.add(distance);
                distanceRepository.add(reverseDistance);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        distancesMap.clear();
    }

    public List<Distance> getAllFiltered() {
        return this.distanceRepository.getAll()
                .stream()
                .filter(distance -> distance.getDurationMinutes() > 0)
                .collect(Collectors.toList());
    }
}
