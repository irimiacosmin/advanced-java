package lab4.services;

import lab4.entities.MeetingEntity;
import lab4.entities.MeetingPersonEntity;
import lab4.entities.MeetingTypeEntity;
import lab4.entities.Topic;
import lab4.models.Location;
import lab4.models.Meeting;
import lab4.models.Person;
import lab4.persistance.LocationRepository;
import lab4.persistance.MeetingRepository;
import lab4.persistance.MeetingTypeRepository;
import lab4.persistance.PersonRepository;
import lab4.utils.SQLConstants;
import lombok.*;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.convert.IntegerConverter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Named(value = "MeetingSearchService")
@SessionScoped
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class MeetingSearchService implements Serializable {

    private final Map<String, Boolean> filterCriteria = new HashMap<>();
    private List<Meeting> toBeShownMeetings = new ArrayList<>();

    private Integer durationMinutesMin;
    private Integer durationMinutesMax;
    private Location location;
    private MeetingTypeEntity type;
    private String personName;

    public List<Topic> getTopics() {
        return Arrays.asList(Topic.values());
    }

    @PersistenceContext(name = SQLConstants.PERSISTENCE_UNIT_NAME)
    protected EntityManager mspAppPU;

    @Inject
    private MeetingRepository meetingRepository;

    @Inject
    private PersonRepository personRepository;

    public List<Meeting> getAllFiltered() {
        return toBeShownMeetings;
    }

    public void updateMeetingsWithCriteriaAPI() {
        CriteriaBuilder criteriaBuilder = mspAppPU.getCriteriaBuilder();
        CriteriaQuery<MeetingEntity> criteriaQuery = criteriaBuilder.createQuery(MeetingEntity.class);
        Root<MeetingEntity> meetingEntityRoot = criteriaQuery.from(MeetingEntity.class);

        List<Predicate> predicates = new ArrayList<>();

        if (filterCriteria.get("DURATION")) {
            Integer min = durationMinutesMin == null ? 0 : durationMinutesMin;
            Integer max = durationMinutesMax == null ? 99999 : durationMinutesMax;
            predicates.add(criteriaBuilder.between(meetingEntityRoot.get("durationMinutes"), min, max));
        }

        if (filterCriteria.get("LOCATION")) {
            predicates.add(criteriaBuilder.equal(meetingEntityRoot.get("location").get("name"), location.name));
        }

        if (filterCriteria.get("TYPE")) {
            predicates.add(criteriaBuilder.equal(meetingEntityRoot.get("type"), type));
        }

        if (filterCriteria.get("PERSON")) {
            CriteriaQuery<Integer> personsWithNameQuery = criteriaBuilder.createQuery(Integer.class);
            Root<MeetingPersonEntity> subQueryMeetingPersons = personsWithNameQuery.from(MeetingPersonEntity.class);
            personsWithNameQuery
                    .select(subQueryMeetingPersons.get("meeting").get("id"))
                    .where(criteriaBuilder.like(subQueryMeetingPersons.get("person").get("name"), "%" + personName + "%"));
            TypedQuery<Integer> meetingIdsOfPersonsWithNameLikeQuery = mspAppPU.createQuery(personsWithNameQuery);
            List<Integer> meetingIdsOfPersonsWithNameLike = meetingIdsOfPersonsWithNameLikeQuery.getResultList();
            predicates.add(meetingEntityRoot.get("id").in(meetingIdsOfPersonsWithNameLike));
        }

        Predicate[] predicateArray = new Predicate[predicates.size()];
        predicateArray = predicates.toArray(predicateArray);

        criteriaQuery.select(meetingEntityRoot)
                .where(filterCriteria.get("SEARCH_TYPE") ? criteriaBuilder.or(predicateArray) : criteriaBuilder.and(predicateArray));

        TypedQuery<MeetingEntity> query = mspAppPU.createQuery(criteriaQuery);
        List<MeetingEntity> results = query.getResultList();
        toBeShownMeetings = results.stream()
                .map(meetingEntity -> new Meeting(meetingEntity, personRepository.getAllByMeetingId(meetingEntity.getId())))
                .collect(Collectors.toList());

    }

    public void updateMeetingsWithJPQL() {
        String aggregatorType = filterCriteria.get("SEARCH_TYPE") ? "OR" : "AND";
        Query query = mspAppPU.createQuery("SELECT e FROM MeetingEntity e " +
                "WHERE (e.durationMinutes >= :durationMinutesMin OR :durationMinutesMin IS NULL) " +
                aggregatorType + " (e.durationMinutes <= :durationMinutesMax OR :durationMinutesMax IS NULL) " +
                aggregatorType + " (e.location.name = :location OR :location IS NULL) " +
                aggregatorType + " (e.type = :type OR :type IS NULL) " +
                aggregatorType + " (SELECT COUNT(mp) FROM MeetingPersonEntity mp " +
                "     WHERE mp.meeting = e " +
                "     AND (mp.person.name LIKE :personName OR :personName IS NULL)) > 0");
        query.setParameter("durationMinutesMin", filterCriteria.get("DURATION") ? durationMinutesMin : null);
        query.setParameter("durationMinutesMax", filterCriteria.get("DURATION") ? durationMinutesMax : null);
        query.setParameter("location", filterCriteria.get("LOCATION") ? location.name : null);
        query.setParameter("type", filterCriteria.get("TYPE") ? type : null);
        query.setParameter("personName", filterCriteria.get("PERSON") ? "%" + personName + "%": null);

        toBeShownMeetings = meetingRepository.getAllFiltered(query);
    }

    public void updateMeetings() {
        updateMeetingsWithCriteriaAPI();
        //updateMeetingsWithJPQL();
    }
}
