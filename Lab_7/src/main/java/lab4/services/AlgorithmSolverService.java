package lab4.services;

import lab4.models.*;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.constraints.LCF;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.VF;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named(value = "SolveDumb")
@RequestScoped
public class AlgorithmSolverService implements Serializable {

    // process input
    Solver solver;
    int nMeetings; // number of meetings to be scheduled
    int mAgents; // the number of agents
    int timeslots; // timeslots available
    int[][] attendance; // container for the first matrix of the input file (each agent and his meetings attendance)
    int[][] distance; // container for the second matrix of the input file (distance between meetings)

    /*
     * IntVar that contains all meetings (of length nMeetings). The value of meeting[i]
     * corresponds to the timeslot in which meeting i occurs. The value of meeting[i]
     * for 0 <= i < nMeetings is [0, timeslots).
     */
    IntVar[] meeting;

    private void init(List<Person> persons, List<Meeting> meetings, List<Distance> distances, List<Location> locations) {
        nMeetings = meetings.size();
        mAgents = persons.size();
        timeslots = 840;

        attendance = new int[mAgents][nMeetings]; // this is only needed to compute which meetings can be in parallel
        distance = new int[nMeetings][nMeetings]; // this is used to apply the travel contraints

        for (int i = 0; i < mAgents; i++) {
            for (int j = 0; j < nMeetings; j++) {
                attendance[i][j] = 0;
            }
        }

        for (int i = 0; i < nMeetings; i++) {
            for (int j = 0; j < nMeetings; j++) {
                distance[i][j] = 0;
            }
        }

        for (Meeting meet : meetings) {
            for (Person person : meet.getAttendees()) {
                attendance[persons.indexOf(person)][meetings.indexOf(meet)] = 1;
            }
        }

        for (Distance dist : distances) {
            int firstLocationIndex = locations.indexOf(dist.getFirstLocation());
            int secondLocationIndex = locations.indexOf(dist.getSecondLocation());
            Meeting meetingThatIsHappeningAtFirstLocation = meetings.stream()
                    .filter(meet -> meet.getLocation().equals(dist.getFirstLocation()))
                    .findFirst()
                    .orElse(null);
            int meetingDuration = meetingThatIsHappeningAtFirstLocation == null ? 0 :
                    meetingThatIsHappeningAtFirstLocation.getDurationMinutes();

            distance[firstLocationIndex][secondLocationIndex] = dist.getDurationMinutes() + meetingDuration;
        }

        solver = new Solver("Meeting Scheduling Problem"); // create an instance of Solver

        // value of meeting[i] is the timeslot in which meeting i occurs
        meeting = VF.enumeratedArray("Meetings", nMeetings, 0, timeslots - 1, solver);

        // constraint that some meetings cannot be in parallel
        for (int m1 = 0; m1 < nMeetings; m1++) {
            for (int m2 = m1 + 1; m2 < nMeetings; m2++) {
                boolean canBeParallel = canBeParallel(m1, m2);
                if (!canBeParallel) {
                    /*
                     * for each two meetings that cannot occur in parallel, make sure that their
                     * distance is less than the difference of their timeslots
                     * i.e.: |meeting[m1] - meeting[m2]| > distance[m1][m2]
                     * By doing some calculations, this is equivalent to:
                     *     meeting[m1] - meeting[m2] > distance[m1][m2]
                     *                      OR
                     *     meeting[m2] - meeting[m1] > distance[m1][m2]
                     */
                    Constraint eq1 = ICF.arithm(meeting[m1], "-", meeting[m2], ">", distance[m1][m2]);
                    Constraint eq2 = ICF.arithm(meeting[m2], "-", meeting[m1], ">", distance[m1][m2]);

                    solver.post(LCF.or(eq1, eq2));
                }
            }
        }
    }

    /* Checks whether meeting m1 and meeting m2 can occur in parallel
     *     if yes: returns true
     *     if no: returns false
     * meetings m1 and m2 can occur in parallel iff there does not exist any agent that has to
     * attend both of them.
     */

    private boolean canBeParallel(int m1, int m2) {
        for (int i = 0; i < mAgents; i++) {
            if (attendance[i][m1] == 1 && attendance[i][m2] == 1) {
                return false;
            }
        }
        return true;
    }

    private boolean solve() {
        return solver.findSolution();
    }

    private List<MeetingDistribution> computeResult(List<Distance> distances, List<Meeting> meetings) {
        List<MeetingDistribution> meetingDistributions = new ArrayList<>();

        for (int i = 0; i < nMeetings; i++) {
            Meeting meet = meetings.get(i);
            int timeTillNextMeeting = 0;
            if (i < nMeetings - 1) {
                Meeting nextMeet = meetings.get(i + 1);
                Distance distance = distances.stream()
                        .filter(dist -> dist.getFirstLocation().equals(meet.getLocation()))
                        .filter(dist -> dist.getSecondLocation().equals(nextMeet.getLocation()))
                        .findFirst()
                        .orElse(null);
                if (distance != null) {
                    timeTillNextMeeting = distance.getDurationMinutes();
                }
            }
            Integer startingMinute = meeting[i].getLB();
            MeetingDistribution meetingDistribution = new MeetingDistribution(meet, startingMinute, timeTillNextMeeting);
            meetingDistributions.add(meetingDistribution);
        }
        return meetingDistributions;
    }

    public List<MeetingDistribution> solveProblem(List<Person> persons, List<Meeting> meetings, List<Distance> distances, List<Location> locations) {
        init(persons, meetings, distances, locations);
        if (solver.findSolution()) {
            return computeResult(distances, meetings);
        }
        return null;
    }
}
