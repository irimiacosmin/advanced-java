package lab4.services;

import lab4.models.Location;
import lab4.persistance.LocationRepository;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named(value = "LocationService")
@SessionScoped
public class LocationService implements Serializable {

    @EJB
    private LocationRepository locationRepository;

    public List<Location> getAll() {
        return locationRepository.getAll();
    }

    public int save(Location location) throws Exception {
        return locationRepository.add(location);
    }
}
