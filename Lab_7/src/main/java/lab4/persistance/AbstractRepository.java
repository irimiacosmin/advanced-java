package lab4.persistance;

import lab4.entities.AbstractEntity;
import lab4.utils.SQLConstants;
import lombok.Getter;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;

@Getter
public abstract class AbstractRepository {

    @PersistenceContext(name = SQLConstants.PERSISTENCE_UNIT_NAME)
    public EntityManager mspAppPU;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    int create(AbstractEntity entity) {
        mspAppPU.persist(entity);
        mspAppPU.flush();
        return (int) entity.getId();
    }
}
