package lab4.persistance;

import lab4.entities.MeetingTypeEntity;
import lab4.exceptions.EntryNotFoundException;
import lab4.utils.JPQLConstants;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Stateless
@LocalBean
public class MeetingTypeRepository extends AbstractRepository implements SkeletonRepository<MeetingTypeEntity> {

    @Override
    public int add(MeetingTypeEntity entry) throws Exception {
        return create(entry);
    }

    @Override
    public MeetingTypeEntity getById(int id) throws EntryNotFoundException {
        return getEntries(JPQLConstants.GET_BY_ID(MeetingTypeEntity.class, id))
                .stream()
                .findFirst()
                .orElseThrow(EntryNotFoundException::new);
    }

    @Override
    public List<MeetingTypeEntity> getAll() {
        return getEntries(JPQLConstants.SELECT_ALL(MeetingTypeEntity.class));
    }

    private List<MeetingTypeEntity> getEntries(String query) {
        return new ArrayList<>(((Collection<MeetingTypeEntity>) mspAppPU.createQuery(query).getResultList()));
    }
}
