package lab4.persistance;

import lab4.entities.DistanceEntity;
import lab4.exceptions.EntryNotFoundException;
import lab4.models.Distance;
import lab4.utils.JPQLConstants;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class DistanceRepository extends AbstractRepository implements SkeletonRepository<Distance> {

    @Override
    public int add(Distance entry) throws Exception {
        return create(entry.toEntity());
    }

    @Override
    public Distance getById(int id) throws EntryNotFoundException {
        return getEntries(JPQLConstants.GET_BY_ID(DistanceEntity.class, id))
                .stream()
                .findFirst()
                .orElseThrow(EntryNotFoundException::new);
    }

    @Override
    public List<Distance> getAll() {
        return getEntries(JPQLConstants.SELECT_ALL(DistanceEntity.class));
    }

    private List<Distance> getEntries(String query) {
        return ((Collection<DistanceEntity>) mspAppPU.createQuery(query).getResultList())
                .stream()
                .map(Distance::new)
                .collect(Collectors.toList());
    }
}
