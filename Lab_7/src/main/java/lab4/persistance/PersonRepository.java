package lab4.persistance;

import lab4.ejb.LogInterceptor;
import lab4.entities.PersonEntity;
import lab4.models.Person;
import lab4.utils.JPQLConstants;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class PersonRepository extends AbstractRepository implements SkeletonRepository<Person> {

    @Override
    public int add(Person entry) throws Exception {
        return create(entry.toEntity());
    }

    public int add(PersonEntity entry) throws Exception {
        return create(entry);
    }

    @Override
    @Interceptors(LogInterceptor.class)
    public Person getById(int id) {
        return getEntries(JPQLConstants.GET_BY_ID(PersonEntity.class, id))
                .stream()
                .findFirst()
                .orElse(null);
    }

    public Person update(Person person) {
        return new Person(mspAppPU.merge(person.toEntity()));
    }

    public boolean delete(Person person) {
        try {
            mspAppPU.detach(person.toEntity());
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean delete(PersonEntity person) {
        try {
            mspAppPU.detach(person);
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    @Override
    @Interceptors(LogInterceptor.class)
    public List<Person> getAll() {
        return getEntries(JPQLConstants.SELECT_ALL(PersonEntity.class));
    }

    public List<Person> getAllByMeetingId(int meetingId) {
        return getEntries(JPQLConstants.GET_ALL_MEETING_ATTENDEES(meetingId));
    }

    public List<Person> getEntries(String query) {
        return ((Collection<PersonEntity>) mspAppPU.createQuery(query).getResultList())
                .stream()
                .map(Person::new)
                .collect(Collectors.toList());
    }
}
