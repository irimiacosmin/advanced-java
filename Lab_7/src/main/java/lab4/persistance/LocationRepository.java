package lab4.persistance;

import lab4.entities.LocationEntity;
import lab4.exceptions.EntryNotFoundException;
import lab4.models.Location;
import lab4.utils.JPQLConstants;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class LocationRepository extends AbstractRepository implements SkeletonRepository<Location>, Serializable {

    @Override
    public int add(Location entry) throws Exception {
        return create(entry.toEntity());
    }

    @Override
    public Location getById(int id) throws EntryNotFoundException {
        return getEntries(JPQLConstants.GET_BY_ID(LocationEntity.class, id))
                .stream()
                .findFirst()
                .orElseThrow(EntryNotFoundException::new);
    }

    @Override
    public List<Location> getAll() {
        return getEntries(JPQLConstants.SELECT_ALL(LocationEntity.class));
    }

    private List<Location> getEntries(String query) {
        return ((Collection<LocationEntity>) mspAppPU.createQuery(query).getResultList())
                .stream()
                .map(Location::new)
                .collect(Collectors.toList());
    }
}
